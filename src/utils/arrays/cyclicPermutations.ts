import { initialPermutationReduction } from './initialPermutationReduction';
import { permutationReducer } from './permutationReducer';
import { LikePairs } from './LikePairs';

export const cyclicPermutations =
    <T>(items:Array<T>):LikePairs<T> => {
        const [first] = items
        const loopedItems = [...items, first]
        return loopedItems.reduce(
            permutationReducer,
            initialPermutationReduction<T>()
        ).permutations
    }