import * as React from 'react';
import { IconButton, Tooltip } from '@material-ui/core';
import TodayIcon from '@material-ui/icons/Today';

export const FilterByDateButton = ({buttonClickHandler}) => <Tooltip title="Filter By Date" placeholder="right">
    <IconButton onClick={buttonClickHandler}>
        <TodayIcon />
    </IconButton>
</Tooltip>