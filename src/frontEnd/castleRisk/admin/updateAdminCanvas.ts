import * as React from 'react';
import { CastleRiskState } from '../types/CastleRiskState';
import { clearCanvas } from '../canvas/clearCanvas';
import { context2dForRef } from '../canvas/context2dForRef';
import { drawTerritoryNames } from '../territories/general/drawTerritoryNames';
import { drawAdminStuff } from './drawAdminStuff';
import { drawTerritoryShapes } from "../territories/general/drawTerritoryShapes";
import { drawImage } from '../canvas/drawImage';
import { multiplex } from '../../../utils/composition/multiplex';

export const updateAdminCanvas = 
    (canvasRef: React.MutableRefObject<null>) =>
    (state: CastleRiskState) =>
    (background: HTMLImageElement) =>
    multiplex([
        clearCanvas,
        drawImage(background)(0)(0),
        drawTerritoryShapes(state.mousePoint),
        drawTerritoryNames,
        drawAdminStuff(state),
    ])(context2dForRef(canvasRef))
