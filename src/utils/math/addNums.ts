import { zip } from "../list/zip"

export const addNums = 
    (nums1: number[]) => 
    (nums2: number[]) => 
    zip(nums1, nums2).value.map(([n1, n2]) => n1 + n2)
