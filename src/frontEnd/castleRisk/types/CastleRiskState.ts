import { Phase } from "./Phase";
import { Players } from "./Players";
import { Player } from "./Player";
import { AdminState } from "./AdminState";
import { Point } from "./Point";

export type CastleRiskState = {
    phase: Phase
    players: Players
    newPlayer: Player | undefined
    admin: AdminState,
    currentPlayer?: Player,
    mousePoint: Point,
};
