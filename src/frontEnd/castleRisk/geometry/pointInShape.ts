import { Shape } from "../types/Shape"
import { Point } from "../types/Point"
import classifyPoint from "robust-point-in-polygon"
import { pointToArrayPoint } from "./pointToArrayPoint"

export const pointInShape = 
    (point: Point) => 
    (shape: Shape) => 
    classifyPoint(
        shape.map(pointToArrayPoint), 
        pointToArrayPoint(point)
    ) < 1
