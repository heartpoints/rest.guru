import { originPoint } from './originPoint';
import { Shape } from '../types/Shape';

export const rectangle =
    (width: number) => 
    (height: number):Shape => 
    [
        originPoint, 
        { x: width, y: 0 }, 
        { x: width, y: height }, 
        { x: 0, y: height }
    ];
