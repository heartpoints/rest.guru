export const urlWithGuaranteedProtocol = (url: string) => {
    const urlWithHttp = url
        ? !url.match(/^[a-zA-Z]+:\/\//)
            ? 'https://' + url
            : url
        : ""

    return urlWithHttp;
}