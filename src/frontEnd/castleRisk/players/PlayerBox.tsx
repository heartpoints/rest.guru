import * as React from 'react'
import { Typography } from '@material-ui/core'
import { colorStyle } from '../colors/colorStyle'
import { Player } from '../types/Player'
import { lightenColor } from '../colors/lightenColor'
import { highlightedPlayerBoxStyle } from './highlightedPlayerBoxStyle';

export const PlayerBox = 
    (p: Player) => 
    (isCurrent:boolean) => {
        const color = isCurrent ? lightenColor(0.5)(p.color) : p.color
        const divStyle = {
            ...isCurrent ? highlightedPlayerBoxStyle : {},
            ...colorStyle(color),
            padding: "1em"
        }
        return <div style={divStyle}>
            <Typography variant="body1">
                <strong>
                    {isCurrent && ">>> "}
                    {p.name}
                    {p.castles.length > 0 && ` - Castle(s): ${p.castles.map(c => c.empire.name).join(",")}`}
                </strong>
            </Typography>
        </div>
    }
    
