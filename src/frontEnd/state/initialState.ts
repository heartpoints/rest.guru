import { initialOrgsState } from "../organizations/data/initialOrgsState";
import { initialDevelopersState } from "../developers/initialDevelopersState";
import { initialNavState } from "../nav/initialNavState";
import { initialModalsState } from "../modals/initialModalsState";
import { initialSearchBarState } from "../search/initialSearchBarState";
import { initialCastleRiskState } from "../castleRisk/general/initialCastleRiskState";
import { combineStateProviders } from "./combineStateProviders";
import { initialUserState } from '../user/initialUserState';

export const initialState = combineStateProviders([
    initialOrgsState,
    initialDevelopersState,
    initialNavState,
    initialModalsState,
    initialSearchBarState,
    initialCastleRiskState,
    initialUserState
])
