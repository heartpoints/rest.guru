import * as React from 'react'
import { PhaseTitle } from "../general/PhaseTitle";
import { PlayerSection } from './PlayerSection';
import { Players } from '../types/Players';
import { Player } from '../types/Player';

export const rightSection = (players: Players) => (currentPlayer: Player) => <div>
    <PhaseTitle>Place Castles</PhaseTitle>
    <p>
        In turn, players place their castle, along with 1 army piece,
        on any territory within the empire that matches their castle
        </p>
    {PlayerSection(players)(currentPlayer)}
</div>;
