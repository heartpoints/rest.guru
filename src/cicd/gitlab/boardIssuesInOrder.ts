import { envVarGetOrThrow } from '../../utils/envVar/envVarGetOrThrow'
import { JSONObject } from '../../utils/json/JSONObject'
import { ellipsis } from '../../utils/strings/ellipsis'
import { services } from './services'
import { projectId } from "./projectId"

export const boardIssuesInOrder = async () => {
    const issuesInOrderUrl = "https://gitlab.com/-/boards/1417855/lists/4105657/issues?id=4105657&page=1"
    const headers = { "PRIVATE-TOKEN": envVarGetOrThrow("GITLAB_ENV_TOKEN") }
    const result = await fetch(issuesInOrderUrl, { headers })
    const body = await result.json()
    const issues = await services.Issues.all({ projectId }) as JSONObject[]
    const niceIssue = ({ iid, title, real_path }) => ({
        iid, title, description: ellipsis(issues.find(i => i.iid == iid)!.description as string || "", 75), url: `https://gitlab.com${real_path}`
    })
    console.log(JSON.stringify(body.issues.map(niceIssue), null, 3))
}
