import { history } from "./history"
import { navTo } from "./navTo"
import { UnregisterCallback } from "history"
import { doNothing } from "../../utils/axioms/doNothing"
import { udpateStateAndRerender } from "./udpateStateAndRerender"
import { heartpointsMutableState } from "../state/heartpointsMutableState"

export let previousHistoryListener:UnregisterCallback = doNothing
export const updateStateToUseOnBackAndForwardNav = () => {
    previousHistoryListener()
    previousHistoryListener = history().listen(
        (location, action) =>
        action == "POP" 
        && udpateStateAndRerender
            (navTo(heartpointsMutableState.read(), location.pathname, true))
    )
}
