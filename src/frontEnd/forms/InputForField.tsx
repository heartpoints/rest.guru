import * as React from 'react';
import { textChangeHandler } from "./textChangeHandler";
import { HPTextField } from './HPTextField';

export const InputForField =
  (props) => {
    const { value, setValue } = props
    return <HPTextField value={value === undefined || value === null ? "" : value} onChange={textChangeHandler(setValue)} {...props} />
  }
    
