import { OnCanvasMouseEventHandler } from '../canvas/OnCanvasMouseEventHandler';
import { UpdateStateForCastleRisk } from '../general/UpdateStateForCastleRisk';
import { pointFromMouseEvent } from '../canvas/pointFromMouseEvent';

export const onMouseMove = 
    (updateState: UpdateStateForCastleRisk): OnCanvasMouseEventHandler => 
    (e) => {
        const mousePoint = pointFromMouseEvent(e);
        updateState({ mousePoint });
    };
