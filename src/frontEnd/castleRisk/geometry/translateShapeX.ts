import { Shape } from '../types/Shape';
import { translatePoint } from './translatePoint';
export const translateShapeX = (x: number) => (shape: Shape) => shape.map(translatePoint({ x, y: 0 }));
