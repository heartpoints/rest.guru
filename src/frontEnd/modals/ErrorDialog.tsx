import * as React from 'react';
import { Dialog, DialogTitle} from '@material-ui/core';
import { HPButton } from '../forms/HPButton';
import { doNothing } from '../../utils/axioms/doNothing';

export const ErrorDialog = ({titleText, isOpen, dialogCloseHandler}) => {
    return <Dialog onClose={doNothing} open={isOpen}>
        <DialogTitle>{titleText}</DialogTitle>
        <HPButton onClick={dialogCloseHandler} label="OK" />
    </Dialog>
}