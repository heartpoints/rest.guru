import { services } from './services';
import { projectId } from './projectId';
export const createNewBranch = async (branchName: string) => {
    return await services.Branches.create(projectId, branchName, "master")
        .catch(e => console.log(e));
};
