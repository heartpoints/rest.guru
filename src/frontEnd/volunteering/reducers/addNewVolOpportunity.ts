import { navTo } from '../../nav/navTo';
import { AddNewVolOpportunityType } from '../types/AddNewVolOpportunityType';

export const addNewVolOpportunity:AddNewVolOpportunityType = (currentState, parentOrg, newVolopportunity) => {
    const newState = {
        ...currentState,
        organizations: currentState.organizations.map(o => ({
            ...o,
            volOpportunities: parentOrg.href == o.href ? [...o.volOpportunities, newVolopportunity] : o.volOpportunities
        }))
    }
    return navTo(newState, `/volunteering/${newVolopportunity.jobID}`)
};