import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import {Select, Theme, FormHelperText} from '@material-ui/core';
import { Consumer } from '../../utils/axioms/Consumer';
import { isMobile } from '../site/isMobile';
import { ellipsis } from '../../utils/strings/ellipsis';

export type HPDropdownProps = { onItemSelected:Consumer<any>, label:string, children:any, labelStyle?:any, fieldStyle?:any, value:any, errorMessage?: string}

export const HPDropdown = ({children, onItemSelected, label, labelStyle, fieldStyle, value, errorMessage}:HPDropdownProps) => {
  const controlMargin = () => isMobile() ? 0 : 1;
  const controlWidth = () => isMobile() ? "100%" : "50%"

  const classes = makeStyles((theme:Theme) => ({
    formControl: {
      margin: theme.spacing(controlMargin()),
      width: controlWidth()
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    }
  }))
  const changeHandler = ({target: {value}}) => onItemSelected(value)
  const guid = `guid${Math.floor(Math.random() * 123812938)}`
  const selectInput = <FilledInput name="age" id={guid} />

  const truncateValue = (val) =>  isMobile() ? ellipsis(val as string, 25) : val;
  
  return (
      <FormControl error={errorMessage != undefined} variant="filled" className={classes().formControl}>
        <InputLabel style={labelStyle} htmlFor={guid}>{label}</InputLabel>
        <Select input={selectInput} value={value} onChange={changeHandler} style={fieldStyle} renderValue={value => truncateValue(value)}>
            {children}
        </Select>
        {errorMessage && <FormHelperText>{errorMessage}</FormHelperText>}
    </FormControl>
  )
}