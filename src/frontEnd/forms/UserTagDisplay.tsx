import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Avatar, Theme } from '@material-ui/core';

const classes = makeStyles((theme: Theme) => ({
    root: {
      display: 'flex'
    },
    typographyStyle: {
        paddingLeft: '5px',
        cursor:"pointer",
        alignSelf:'center',
        '&:hover': {
            color: 'blue',
            textDecoration:'underline',
        }
    }
  }));

export const UserTagDisplay = ({clickHandler, creatorName, userProfileImage=""}) => {
    
    return <div className={classes().root}>
        <Avatar src={userProfileImage.length == 0 ? "/images/defaultAvatarImage.png" : userProfileImage} onClick={clickHandler} sizes="big" style={{cursor:"pointer"}}></Avatar>       
        <Typography variant="subtitle2" style={{paddingLeft: '5px', alignSelf:'center'}}>createdBy: </Typography> 
        <Typography className={classes().typographyStyle} variant="subtitle2" onClick={clickHandler}>{creatorName}</Typography>
    </div>
}
