import * as React from 'react';
import { HPButton, PossibleChildren, HasLabel } from '../forms/HPButton';
import { isMobile } from '../site/isMobile';

export type ModalActionButtonProps = PossibleChildren & HasLabel
export const ModalActionButton = ({ children, label }:ModalActionButtonProps) => {
    return <HPButton shouldBeInline label={label} onClick={() => null}>
        {children}
    </HPButton>;
};
