import { identity } from '../../../utils/axioms/identity';
import { drawCurrentlyHoveredTerritory } from '../territories/general/drawCurrentlyHoveredTerritory';
import { Point } from '../types/Point';
import { territoryIfHoveredAndWithinEmpire } from '../territories/general/territoryIfHoveredAndWithinEmpire';
import { Empire } from '../types/Empire';
export const possibleHighlight = (mousePoint: Point) => (empire: Empire) => territoryIfHoveredAndWithinEmpire(mousePoint)(empire)
    .mapOrDefault(drawCurrentlyHoveredTerritory, identity);
