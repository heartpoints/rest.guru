import { Player } from '../types/Player';
export const reservePiecesForPlayer = (player: Player) => player.pieces.filter(p => p.territory == undefined);
