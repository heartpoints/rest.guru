import * as React from 'react';
import { SingleRowOfColumns } from '../../page/SingleRowOfColumns';
import { onMouseMove } from '../admin/onMouseMove';
import { context2dForRef } from '../canvas/context2dForRef';
import { gameCanvas } from '../canvas/gameCanvas';
import { OnCanvasMouseEventHandler } from '../canvas/OnCanvasMouseEventHandler';
import { canvasHeight } from '../config/canvasHeight';
import { rightSection } from '../players/rightSection';
import { CastleRiskPhaseProps } from "../types/AddPlayerProps";
import { drawPlaceCastles } from './drawPlaceCastles';
import { placeCastlesCanvasWidth } from './placeCastlesCanvasWidth';
import { territoryIfHoveredAndWithinEmpire } from '../territories/general/territoryIfHoveredAndWithinEmpire';
import { mapFirst } from '../../../utils/arrays/mapFirst';

export const PlaceCastles = (state:CastleRiskPhaseProps) => {
    const {players, currentPlayer, updateState, mousePoint } = state
    const canvasRef = React.useRef(null)

    React.useEffect(() => {
        const context = context2dForRef(canvasRef)
        currentPlayer && drawPlaceCastles
            (players)
            (currentPlayer)
            (mousePoint)
            (context)
    })
    
    const onMouseUp:OnCanvasMouseEventHandler = 
        e =>
        {
            //todo: ensure currentPlayer during this phase so as to avoid null checks
            if(currentPlayer == undefined) return
            territoryIfHoveredAndWithinEmpire(mousePoint)(currentPlayer.castles[0].empire).map(
                territory => {
                    const newPlayers = mapFirst(players)(p => p == currentPlayer)(p => ({
                        ...p,
                        territories: [...p.territories, territory],
                        castles: p.castles.map(c => ({
                            ...c,
                            territory
                        })),
                        pieces: mapFirst
                            (p.pieces)
                            (p => p.territory == undefined)
                            (p => ({...p, territory }))
                    }))
                    updateState({
                        players: newPlayers,
                        currentPlayer: newPlayers[1]
                    })
                }
                
            )
            //todo: currentPlayer.castles
            //todo: move an unused player piece to the clicked territory and display it
        }

    return <div>
        {SingleRowOfColumns(
            gameCanvas
                (canvasRef)
                ({x: placeCastlesCanvasWidth, y: canvasHeight})
                (onMouseUp)
                (onMouseMove(updateState)),
            rightSection(players)(currentPlayer!)
        )}
    </div>
}