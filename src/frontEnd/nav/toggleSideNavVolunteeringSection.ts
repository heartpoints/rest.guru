export const toggleSideNavVolunteeringSection = (state) => ({
    ...state,
    isSideNavVolunteeringSectionExpanded: !state.isSideNavVolunteeringSectionExpanded
});