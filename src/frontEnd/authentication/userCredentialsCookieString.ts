import Cookies from 'js-cookie';
import { userCredentialsCookieKey } from './userCredentialsCookieKey';

export const userCredentialsCookieString = () => {
    const session = Cookies.get(userCredentialsCookieKey);
    return session && JSON.parse(session);
}