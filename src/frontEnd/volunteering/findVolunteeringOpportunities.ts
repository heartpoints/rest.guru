import { combineOrgOppPairToSinglePropsObject } from "./combineOrgOppPairToSinglePropsObject";
import { orgOppPairsMatchingSoughtText } from "./orgOppPairsMatchingSoughtText";
import { CombinedOrgAndVolOpportunity } from "./types/CombinedOrgAndVolOpportunity";
import { roundDate } from '../date/roundDate';
import { DateFilter } from "./types/DateFilter";
import haversine from 'haversine';
import { Coord } from '../maps/types/Coord';

export const findVolunteeringOpportunities = 
    (volSearchBarValue = "", dateFilter:DateFilter, organizations, userZipCoordinates:Coord, zipFilter:string):CombinedOrgAndVolOpportunity[] => {
        const { dateFilterStart, dateFilterEnd } = dateFilter;
        const defaultCoords:Coord = {latitude: 0, longitude: 0};

        const shouldFilterByLocation = userZipCoordinates.latitude != defaultCoords.latitude && zipFilter.length == 5;

        const allOrgOppPairs = orgOppPairsMatchingSoughtText(volSearchBarValue, organizations)
            .map(combineOrgOppPairToSinglePropsObject).asArray

        const geoDistance = 
            (coord:Coord) =>
            (coord2:Coord) =>
            haversine(coord, coord2)
            
        const distanceFromZip =
            geoDistance(userZipCoordinates)

        const distanceFromOpp = 
            ({coordinates: { lat: latitude, lng: longitude }}:CombinedOrgAndVolOpportunity) =>
            distanceFromZip({latitude, longitude})

        const filterByDate = (a:CombinedOrgAndVolOpportunity[]):CombinedOrgAndVolOpportunity[] => {
            return a.filter(pair => 
                    dateFilterStart
                        .isBetween(
                            roundDate(pair.startDate, "down"),
                            roundDate(pair.endDate, "up"), 'day', '[]')
                    && dateFilterEnd
                        .isBetween(
                            roundDate(pair.startDate, "down"),
                            roundDate(pair.endDate, "up"), 'day', '[]')
                    )
        }

        const filterByRadius = (a:CombinedOrgAndVolOpportunity[]):CombinedOrgAndVolOpportunity[] => {
            
            return shouldFilterByLocation
                ? a.filter(pair => {
                    const latitude = pair.coordinates.lat;
                    const longitude = pair.coordinates.lng;
                    const coords:Coord = {latitude, longitude};

                    return haversine(coords, userZipCoordinates, {unit: 'mile'}) <= 10
                })
                : a
        }
        
        const filteredResults = filterByRadius(filterByDate(allOrgOppPairs))
        
        return shouldFilterByLocation 
            ? filteredResults
                .map(opp => ({ opp, distance: distanceFromOpp(opp) }))
                .sort((a, b) => a.distance - b.distance)
                .map(({opp}) => opp)
            : filteredResults
    }