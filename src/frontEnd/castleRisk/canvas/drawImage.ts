export const drawImage = 
    (background: CanvasImageSource) => 
    (x: number) => 
    (y: number) => 
    (context: CanvasRenderingContext2D) => 
    context.drawImage(
        background, 
        x, 
        y, 
        background.width as number, 
        background.height as number
    )
