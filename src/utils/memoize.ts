export const memoize = <T extends Function>(f: T): T => {
    let returnValsKeyedByParam = new Map();
    const memoized = (...args: any) => {
        const key = JSON.stringify(args);
        const keyedRetVals = returnValsKeyedByParam.has(key)
            ? returnValsKeyedByParam
            : returnValsKeyedByParam.set(key, f(...args));
        return keyedRetVals.get(key);
    };
    return memoized as unknown as T;
};
