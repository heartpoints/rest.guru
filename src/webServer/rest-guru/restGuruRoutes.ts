import { latLonForZipHandler } from "../routes/latLonForZipHandler";
import { Router } from "../routes/router";
import { responseText } from "./rootResource/responseText";
import { restGuruGet } from "./rootResource/restGuruGet";

export const restGuruRoutes:Router =
    (expressApp) => {
        expressApp.options("/rest-guru", (req, res, next) => res.send(responseText(req)))
        expressApp.get("/rest-guru", restGuruGet)
        expressApp.get("/latLonForZip", latLonForZipHandler)
    }