export const strokeText = (text: string) => (x: number) => (y: number) => (context: CanvasRenderingContext2D) => context.strokeText(text, x, y);
