import * as React from 'react';
import { Typography } from '@material-ui/core';
import { Space } from '../../page/Space';

export const HPNotFoundDisplay = ({message}) => <div style={{textAlign: "center", margin: "50px 0 20px"}}>
    <Typography variant="h4">Sorry, no results.</Typography>
    <Space />
    <img style={{width: "25%"}} src="/images/questionMark.png" />
    <Space />
    <Typography variant="body1">{message}</Typography>
</div>