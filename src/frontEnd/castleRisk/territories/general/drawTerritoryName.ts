import { drawText } from '../../canvas/drawText';
import { getTextWidth } from '../../canvas/getTextWidth';
import { centroidOfPolygon } from '../../geometry/centroidOfPolygon';
import { translatePointX } from '../../geometry/translatePointX';
import { RGBAColor } from '../../types/RGBAColor';
import { Territory } from "../../types/Territory";

export const drawTerritoryName = 
    (color:RGBAColor) =>
    (t: Territory) => 
    (context:CanvasRenderingContext2D) =>
    drawText
        (true)
        (color)
        (translatePointX
            (getTextWidth(t.name)(context) / -2)
            (centroidOfPolygon(t.boundary))
        )
        (t.name)
        (context)