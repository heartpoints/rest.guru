import { CastleRiskState } from "./CastleRiskState";
export type HasCastleRiskState = {
    castleRisk: CastleRiskState;
};
