import React, { useState, Fragment } from 'react';
import { HPDropdown } from '../../forms/HPDropdown';
import { WithOrganizations } from '../../organizations/data/WithOrganizations';
import { Page } from '../../page/Page';
import { PageTitle } from '../../page/PageTitle';
import { Space } from '../../page/Space';
import {WithNavTo} from '../../nav/WithNavTo';
import { WithAddNewVolOpportunityWithMagicStateAlreadyPassed } from "../types/WithAddNewVolOpportunityWithMagicStateAlreadyPassed";
import { noOrgsContent } from './noOrgsContent';
import { MenuItem, InputAdornment, Tooltip } from '@material-ui/core';
import { HPTextField } from '../../forms/HPTextField';
import { HPButton } from '../../forms/HPButton';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import { testFieldForMinimumCharacters } from '../../forms/data/testFieldForMinimumCharacters';
import { allFieldsValid } from '../../forms/data/allFieldsValid';
import { testSelectOptionSelected } from '../../forms/data/testSelectOptionSelected';
import { commonDateControl } from './commonDateControl';
import { VolunteeringLocationSelect } from '../../maps/VolunteeringLocationSelect';
import { testCoordinatesNotDefault, defaultCoordinates } from '../../forms/data/testCoordinatesNotDefault';
import { testIntegerInputForNumber } from '../../forms/data/testIntegerInputForNumber';
import { isMobile } from '../../site/isMobile';
import { HPRichText } from '../../forms/HPRichText';
import { ShiftBox, TopOrLeft, BottomOrRight } from '../../page/ShiftBox';
import { User } from 'firebase';
type WithUserCredentials = {userCredentials: User | {email: ""}}

type CreateVolOpportunityProps = WithOrganizations & WithUserCredentials & WithAddNewVolOpportunityWithMagicStateAlreadyPassed & WithNavTo
export const CreateVolOpportunity = ({organizations, userCredentials = {email: ""}, addNewVolOpportunity, navTo} : CreateVolOpportunityProps) => {
    const [ selectedOrgTitle, setSelectedOrgTitle ] = useState('');
    const [ jobTitle, setTitle ] = useState('')
    const [ jobDescription, setDescription ] = useState('')
    const [ heartpointsRewardString, setHeartpointsRewardString] = useState('');
    const [ coordinates, updateCoordinates ] = useState(defaultCoordinates);
    const [ address, updateAddress ] = useState({streetAddress: "", suite: "", city: "", state: "", zip: ""})

    const updateOppCoordinates = (e) => 
        updateCoordinates(e);

    const updateOppAddress = (a) => 
        updateAddress(a);

    const listOfOrganizationTitles = organizations
        .filter(o => o.creatorEmail == userCredentials.email)
        .map(org => org.title)
        
    const menuItems = listOfOrganizationTitles.map(
        (option,i) =>  <MenuItem key={i} value={option}>{option}</MenuItem> 
    ).concat(<MenuItem style={{color: 'blue', textDecoration:'underline'}} value='Create New Organization'>Create New Organization</MenuItem>)

    const orgTitleError = testSelectOptionSelected(selectedOrgTitle);
    const jobTitleError = testFieldForMinimumCharacters(jobTitle);
    const jobDescriptionError = testFieldForMinimumCharacters(jobDescription, 20);
    const rewardError = testIntegerInputForNumber(heartpointsRewardString);
    const { dateRangeError, endDate, startDate, uiComponent: dateRangeComponent } = commonDateControl(useState)    
    const coordinatesError = testCoordinatesNotDefault(coordinates);
    const shouldEnableButton = allFieldsValid([jobTitleError, jobDescriptionError, orgTitleError, dateRangeError, rewardError, coordinatesError]);
    const selectedOrganization = organizations.filter(o => o.title == selectedOrgTitle)[0];
    const newGUID = () => `guid${Math.floor(Math.random() * 10000000000)}`
    
    const onButtonClick = () => {
        const heartpointsReward = parseInt(heartpointsRewardString, 10);
        addNewVolOpportunity(selectedOrganization, { jobTitle, jobDescription, jobID: newGUID(), startDate, endDate, coordinates, heartpointsReward, address})
    }

    const onDropdownSelectChange = (newValue) =>
        newValue == 'Create New Organization' ? navTo("/organizations/new") : setSelectedOrgTitle(newValue)

    const contentWithAtLeastOneOrg = <Fragment>
        <ShiftBox>
            <TopOrLeft>
                <HPDropdown 
                    errorMessage={orgTitleError} 
                    onItemSelected={onDropdownSelectChange} 
                    label="Organization" 
                    value={selectedOrgTitle}>
                        {menuItems}
                </HPDropdown>
                <Space />
                {dateRangeComponent}
                <Space />
                <HPTextField 
                    label="Job Title" 
                    errorMessage={jobTitleError} 
                    value={jobTitle} 
                    onChange={e => setTitle(e.target.value)} 
                    InputProps={{
                        endAdornment: (
                            <Tooltip title="Tell us about a volunteering opportunity with your organization, so that others can find and possibly volunteer to help you" placement="right-end">
                                <InputAdornment position="end">
                                    <InfoOutlinedIcon color="primary"/>
                                </InputAdornment>
                            </Tooltip>
                        )
                    }} /><br />
                <HPRichText errorMessage={jobDescriptionError} changeHandler={value => setDescription(value)} markup={jobDescription} />
                <HPTextField label="HeartPoints Rewarded" errorMessage={rewardError} value={heartpointsRewardString} onChange={e => setHeartpointsRewardString(e.target.value)} /><br />
            </TopOrLeft>
            <BottomOrRight>
                <VolunteeringLocationSelect {...{coordinates, updateOppCoordinates, updateOppAddress, coordinatesError }}/>
            </BottomOrRight>
        </ShiftBox>
        {isMobile() && <Space />}
        <HPButton shouldBeInline shouldUseThemeSpacing={!isMobile()} disabled={!shouldEnableButton} label='Create Job' onClick={onButtonClick} />
    </Fragment>

    const content = listOfOrganizationTitles.length == 0
        ? noOrgsContent
        : contentWithAtLeastOneOrg

    return <Page requiresLoggedIn userCredentials={userCredentials}>
        <PageTitle>Create Volunteering Opportunity</PageTitle>      
        <Space />          
        {content}
    </Page>
}