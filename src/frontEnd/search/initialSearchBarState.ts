import moment from 'moment';

export const initialSearchBarState = () => ({
    searchBarSuggestions: [],
    volSearchBarValue: '',
    zipFilter: '',
    permissionMessageOpen: false,
    startDate: moment(),
    endDate: moment().add(7, 'days'),
    orgSearchBarValue: '',
})