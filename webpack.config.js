const webpack = require("webpack");
const path = require('path');
const localDir = d => path.join(__dirname, d)
const { env } = require("process")
const developmentMode = 'development'
const { mode = developmentMode } = env
const useSourceMaps = mode == developmentMode

module.exports = {
    entry: "./src/frontEnd",
    output: {
        filename: "bundle.js",
        path: path.join(__dirname, "./dist"),
        publicPath: '/'
    },
    mode: mode,
    ...(useSourceMaps && { devtool: 'inline-source-map' }),
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".jsx", ".json"]
    },
    devServer: {
        historyApiFallback: true,
        port: 3002,
        contentBase: ["./dist", "./src/public", "./node_modules"].map(localDir),
        proxy: {
            "/": {
                target: "http://localhost:5001"
            }
        }
    },

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: "awesome-typescript-loader",
                options: {
                    configFileName: "tsconfig.server.json"
                }
            },
            ...(useSourceMaps ? [{ enforce: "pre", test: /\.js$/, loader: "source-map-loader" }] : []),
            { 
                test: /\.svg$/,
                include: [/react-images-upload/],
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]?[hash]'
                }
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            'React':     'react'
        })
    ],
    // When importing a module whose path matches one of the following, just
    // assume a corresponding global variable exists and use that instead.
    // This is important because it allows us to avoid bundling all of our
    // dependencies, which allows browsers to cache those libraries between builds.
    externals: {
        "react": "React",
        "react-dom": "ReactDOM"
    }
};