export const addShadowToContext = (context: CanvasRenderingContext2D) => {
    context.shadowColor = "black";
    context.shadowOffsetX = 0;
    context.shadowOffsetY = 0;
    context.shadowBlur = 8;
};
