import * as React from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";
import { compose, withProps, withStateHandlers } from "recompose";
import { defaultCoordinates } from '../forms/data/testCoordinatesNotDefault';
import { isMobile } from '../site/isMobile';

export const AllOpportunitiesView = compose(
    withStateHandlers(() => ({
        isMarkerShown: false,
        markerPosition: null,
      })),
    withProps({
      googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyCCCW385gZ4e-6rSTXNVtcoklltFRat8ck&v=3.exp&libraries=geometry,drawing,places",
      loadingElement: <div style={{ height: `100%` }} />,
      containerElement: <div style={{ height: `400px` }} />,
      mapElement: <div style={{ height: `100%` }} />,
    }),
    withScriptjs,
    withGoogleMap
  )((props) => {
    const hasResults:boolean = props.filteredOpportunities[0];

    return <GoogleMap
      zoom={hasResults ? 12 : 3}
      center={hasResults ? props.filteredOpportunities[0].coordinates : defaultCoordinates}
    >
     {props.filteredOpportunities.map(v => <Marker onClick={() => props.navTo(`/volunteering/${v.jobID}`)} position={v.coordinates} />)}
    </GoogleMap>
  })