import * as React from 'react';
import { TextField, Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { isMobile } from '../site/isMobile';
import { TextFieldProps } from '@material-ui/core/TextField';

type PossibleError = { errorMessage?: string | undefined, placeholder?:string }
type HasVariant = { variant?: string }
type HPTextFieldProps = Partial<TextFieldProps> & PossibleError & Partial<HasVariant>
export const HPTextField = ({placeholder, errorMessage, children, variant = "filled", ...rest}:HPTextFieldProps) => {

  const { width, space } = isMobile()
    ? { width: "100%", space: 0 }
    : { width: "300px", space: 1 };

  const classes = makeStyles((theme: Theme) => ({
    textField: {
      marginLeft: theme.spacing(space),
      marginRight: theme.spacing(space),
      width
    },
  }));
  
  return <TextField
    error={errorMessage != undefined}
    helperText={errorMessage} 
    label={placeholder}
    placeholder={placeholder}
    margin="normal" 
    {...{variant}}
    className={classes().textField}
    {...rest as any} 
  >
    {children}
  </TextField>;
};
