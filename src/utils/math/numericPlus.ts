import { Plus } from './PlusOfT';

export const numericPlus: Plus<number> = a => b => a + b