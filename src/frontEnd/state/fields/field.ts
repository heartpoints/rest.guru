import { Consumer } from "../../../utils/axioms/Consumer"

export const field = 
    <T>
    (value: T) => 
    (setValue: Consumer<T>) => 
    (label: string) => 
    (placeholder: string) => 
    ({
        value,
        label,
        setValue,
        placeholder,
    })
