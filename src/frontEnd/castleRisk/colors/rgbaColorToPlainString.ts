import { RGBAColor } from '../types/RGBAColor';
export const rgbaColorToPlainString = (color: RGBAColor) => color.join(",");
