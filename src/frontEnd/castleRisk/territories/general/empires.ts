import { independentTerritories } from "../specific/independent/independentTerritories";
import { germanEmpire } from "../specific/german/germanEmpire";
import { frenchEmpire } from "../specific/french/frenchEmpire";
import { Empires } from "../../types/Empires";
import { ottomanEmpire } from "../specific/ottoman/ottomanEmpire";
import { russianEmpire } from "../specific/russian/russianEmpire";
import { austrianEmpire } from "../specific/austrian/austrianEmpire";
import { britishEmpire } from "../specific/british/britishEmpire";
import { Provider } from "../../../../utils/axioms/Provider";

export const empires:Provider<Empires> = () => [
    russianEmpire,
    germanEmpire,
    frenchEmpire,
    ottomanEmpire,
    austrianEmpire,
    britishEmpire,
]

export const empiresWithIndependents = () => [
    ...empires(),
    independentTerritories
]