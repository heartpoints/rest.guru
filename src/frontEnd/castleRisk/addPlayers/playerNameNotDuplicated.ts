import { Players } from '../types/Players'
import { Player } from '../types/Player'

export const playerNameNotDuplicated = 
    (players: Players) => 
    (possibleNewPlayer: Player) => 
    !players
        .map(p => p.name.toLowerCase())
        .includes(possibleNewPlayer.name.toLowerCase())
