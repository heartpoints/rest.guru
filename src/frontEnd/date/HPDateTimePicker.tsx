import * as React from 'react';
import { KeyboardDatePicker, KeyboardTimePicker} from '@material-ui/pickers';

export const HPDateTimePicker = ({dateVal, dateTimeChangeHandler, timeLabel, dateLabel}) => {
    return <React.Fragment>
        <KeyboardDatePicker
            autoOk
            disablePast
            variant="inline"
            label={dateLabel}
            format="MM/DD/YYYY"
            margin="normal"
            value={dateVal}
            onChange={dateTimeChangeHandler} />
        <KeyboardTimePicker
            autoOk
            margin="normal"
            label={timeLabel}
            value={dateVal}
            onChange={dateTimeChangeHandler} />
    </React.Fragment>
}