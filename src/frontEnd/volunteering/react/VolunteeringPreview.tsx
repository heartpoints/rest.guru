import * as React from 'react';
import { Typography } from '@material-ui/core';
import { isMobile } from '../../site/isMobile';
import { VolHeartpointsDisplay } from './VolHeartpointsDisplay';
import { Space } from '../../page/Space';
import { returnOneDateIfSame } from '../../date/formatDate';
import { HPMarkupView } from '../../forms/HPMarkupView';
import { previewContainerStyle } from '../../forms/previewContainerStyle';

export const VolunteeringPreview = (props) => {
    const { jobID, jobTitle, jobDescription, navTo, heartpointsReward, startDate, endDate, address } = props;
    const opportunityTitle = () => isMobile()
        ? "subtitle2"
        : "h5"

    const onSuggestionSelected = () => 
        navTo(`/volunteering/${jobID}`);

    const fullDateString = returnOneDateIfSame(startDate, endDate, "day");

    const {streetAddress, city, state, zip} = address;
    const suite = address.suite ? `${address.suite}, ` : ''
    const fullAddressString = `${streetAddress}, ${suite} ${city}, ${state}, ${zip}`;

    return <div style={previewContainerStyle} onClick={onSuggestionSelected}>
        <div style={{display: "flex", justifyContent: "space-between"}}>
            <Typography variant={opportunityTitle()}>{jobTitle}</Typography>
            <VolHeartpointsDisplay heartpointsToDisplay={heartpointsReward} />
        </div>
        <Typography variant="caption">{fullDateString}</Typography><br />
        <Typography variant="caption">{fullAddressString}</Typography>
        <Space />
        <HPMarkupView markup={jobDescription}/>
    </div>
}