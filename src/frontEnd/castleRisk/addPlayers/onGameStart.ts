import { UpdateStateForCastleRisk } from '../general/UpdateStateForCastleRisk';
import { Players } from '../types/Players';
import { transitionToCastlePlacementState } from './transitionToCastlePlacementState';

export const onGameStart = 
    (playersWithoutEmpires: Players) => 
    (updateState: UpdateStateForCastleRisk) => 
    () => 
    updateState(transitionToCastlePlacementState(playersWithoutEmpires))
