import { HasName } from "./HasName";
import { HasPieces } from "./HasPieces";
import { HasEmpire } from "./HasEmpire";
import { HasCastles } from "./HasCastles";
import { Location } from "./Location";

export type Territory = HasName & Location & HasCastles & HasEmpire & HasPieces;
