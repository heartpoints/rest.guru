import { saxony } from "./saxony";
import { Empire } from "../../../types/Empire";
import { orange } from "../../../colors/orange";
import { territory } from "../../general/territory";
import { Prussia } from "./Prussia";
import { Bavaria } from "./Bavaria";
import { Berlin } from "./Berlin";
import { Rhine } from "./Rhine";
import { darkenColor } from "../../../colors/darkenColor";

export const germanEmpire:Empire = {
    color: darkenColor(0.15)(orange),
    name: "German",
    get territories() {
        const germanTerritory = territory(() => germanEmpire)
        return [
            germanTerritory("Saxony")(saxony),
            germanTerritory("Prussia")(Prussia),
            germanTerritory("Bavaria")(Bavaria),
            germanTerritory("Berlin")(Berlin),
            germanTerritory("Rhine")(Rhine),
        ]
    }
}
