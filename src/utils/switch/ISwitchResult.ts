import { Maybe } from "../maybe/MaybeType";

export interface ISwitchResult<V> {
    result: Maybe<V>;
}
