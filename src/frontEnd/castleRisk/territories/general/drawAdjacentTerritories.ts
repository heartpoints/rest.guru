import { multiplex } from '../../../../utils/composition/multiplex';
import { Territories } from '../../types/Territories';
import { drawAdjacentTerritory } from './drawAdjacentTerritory';

export const drawAdjacentTerritories = 
    (adjacentTerritories: Territories) => 
    multiplex(adjacentTerritories.map(drawAdjacentTerritory))
