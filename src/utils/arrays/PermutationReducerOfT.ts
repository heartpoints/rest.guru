import { PermutationReduction } from './PermutationReduction';
export type PermutationReducer = <T>(reduction: PermutationReduction<T>, cur: T) => PermutationReduction<T>;
