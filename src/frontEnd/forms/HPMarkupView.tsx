import * as React from 'react'

type HasMarkup = { markup:string}
type HasStyle = { style:any}
type HPMarkupViewProps = HasMarkup & Partial<HasStyle>
export const HPMarkupView = ({markup, style}:HPMarkupViewProps) => {
    return <div style={{...style, fontFamily: "Roboto"}} dangerouslySetInnerHTML={{__html: markup}}></div>
}