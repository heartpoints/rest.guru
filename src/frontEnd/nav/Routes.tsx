import { OrgRoutes } from "../organizations/react/OrgRoutes";
import { OtherRoutes } from "./OtherRoutes";
import { VolRoutes } from "../volunteering/react/VolRoutes";

export const Routes = () => [
  OrgRoutes,
  VolRoutes,
  OtherRoutes
];
