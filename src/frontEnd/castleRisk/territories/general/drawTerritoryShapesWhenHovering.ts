import { Territory } from '../../types/Territory';
import { drawAdjacentTerritories } from './drawAdjacentTerritories';
import { drawCurrentlyHoveredTerritory } from './drawCurrentlyHoveredTerritory';
import { drawNonNearbyTerritories } from './drawNonNearbyTerritories';
import { multiplex } from '../../../../utils/composition/multiplex';

export const drawTerritoryShapesWhenHovering = 
    (currentTerritory: Territory) => 
    {
        const { adjacentTerritories } = currentTerritory;
        return multiplex([
            drawNonNearbyTerritories(adjacentTerritories)(currentTerritory),
            drawAdjacentTerritories(adjacentTerritories),
            drawCurrentlyHoveredTerritory(currentTerritory)
        ])
    };
