import React from 'react';
import { SearchBar as OrgSearchBar } from './SearchBar';
import { Typography } from '@material-ui/core';
import { Space } from '../../../page/Space';
import { Page } from '../../../page/Page';
import { EntityPreview } from '../../../page/EntityPreview';

export const ViewAllOrganizations = (props) => {
    const {organizations, navTo} = props;

    const listOfOrganizations = organizations.map(org => <EntityPreview title={org.title} details={org.mission} image={org.imageThumbnailURL} clickHandler={() => navTo(org.href)} />)
    return <Page>
        <OrgSearchBar {...props}/>
        <Space />
        <Space />
        <Typography variant="h6">List of Organizations : </Typography>
        {listOfOrganizations}
    </Page>
}