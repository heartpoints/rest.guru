import * as React from "react";
import { Button, Avatar, Popper, Paper, ClickAwayListener, MenuList, MenuItem, Fade, Typography } from "@material-ui/core";
import PopupState, { bindToggle, bindPopper } from 'material-ui-popup-state';

export const UserLoggedInButton = props => {
    const {userCredentials, logoutUserCredentials, navTo } = props;
    const profilePicUrl = userCredentials.photoURL;
    const userName = userCredentials.displayName;
    
    return <PopupState variant="popper" popupId="demoPopper">
        {popupState => (
            <div>
                <Button color="inherit" {...bindToggle(popupState)}>
                    <Avatar src={profilePicUrl} alt={userName} />
                </Button>
                <Popper {...bindPopper(popupState)} transition>
                    {({ TransitionProps }) => (
                        <Fade {...TransitionProps} timeout={350}>
                            <Paper>
                                <ClickAwayListener onClickAway={() => popupState.close()}>
                                    <MenuList>
                                        <MenuItem onClick={() => navTo(`/user/${userCredentials.email}`)}>Profile</MenuItem>
                                        <MenuItem onClick={logoutUserCredentials}>Logout {userName}</MenuItem>
                                    </MenuList>
                                </ClickAwayListener>
                            </Paper>
                        </Fade>
                    )}
                </Popper>
            </div>
        )}
    </PopupState>
}