import { None } from "./None"
import { Some } from "./Some"
import { Maybe } from "./MaybeType"

export const maybe = 
    <T>(possiblyNullOrUndefinedValue?: T): Maybe<T> => 
    possiblyNullOrUndefinedValue !== null && possiblyNullOrUndefinedValue !== undefined 
        ? Some(possiblyNullOrUndefinedValue) 
        : None
