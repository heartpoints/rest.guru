import { PointPair } from "./PointPair";

export const quasiCommuteAdjacentPoints = 
    ([pN, pNPlus1]: PointPair) => 
    pN.x * pNPlus1.y - pNPlus1.x * pN.y;
