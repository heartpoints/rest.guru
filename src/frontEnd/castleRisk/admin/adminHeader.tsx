import * as React from 'react';
import { PhaseTitle } from "../general/PhaseTitle";
import { Typography } from '@material-ui/core';

export const adminHeader = 
    <React.Fragment>
        <PhaseTitle>Administer Borders</PhaseTitle>
        <Typography variant="body2">
            Not for use in-game, but rather to identify the borders of a given map and generate the data structure
            to be used for the geometric portion of the game logic.
            </Typography>
    </React.Fragment>
