import { RGBAColor } from "../types/RGBAColor";
import { Shape } from "../types/Shape";
import { drawPoint } from './drawPoint';
import { drawPolygon } from "./drawPolygon";
import { black } from "../colors/black";
import { multiplex } from "../../../utils/composition/multiplex";

export const drawShape = 
    (shape: Shape) => 
    (pointFillStyle: RGBAColor ) =>
    (shapeFillStyle: RGBAColor) => 
    multiplex([
        drawPolygon(false)(black)(shapeFillStyle)(shape),
        ...shape.map(drawPoint(pointFillStyle))
    ])
