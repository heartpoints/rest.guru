import { Point } from "../types/Point";
import { Mapper } from '../../../utils/axioms/Mapper';
export type CoordToNumber = Mapper<Point, number>;
