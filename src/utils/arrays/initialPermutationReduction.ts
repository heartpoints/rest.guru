import { PermutationReduction } from './PermutationReduction';
export const initialPermutationReduction = <T>() => ({
    permutations: [],
    toBePermuted: undefined
}) as PermutationReduction<T>;
