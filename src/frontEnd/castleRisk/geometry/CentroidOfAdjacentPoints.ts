import { PointPair } from "./PointPair";
export type CentroidOfAdjacentPoints = (adjacentPair: PointPair) => number;
