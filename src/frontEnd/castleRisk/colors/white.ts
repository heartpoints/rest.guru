import { RGBAColor } from '../types/RGBAColor';
export const white: RGBAColor = [255, 255, 255, 255];
