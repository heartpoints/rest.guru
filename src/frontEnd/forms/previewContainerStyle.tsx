export const previewContainerStyle = {
    "margin": "10px 0",
    "padding": "10px",
    "backgroundColor": "#E9E9E9",
    "borderRadius": "5px",
    "cursor": "pointer",
};
