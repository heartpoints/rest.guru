import { Player } from '../types/Player'

export const playerNameRequirementsMet = 
    (possibleNewPlayer: Player) => 
    possibleNewPlayer.name.trim().length > 1
