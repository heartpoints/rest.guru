import { heartpointsMutableState } from '../../../state/heartpointsMutableState';
export const getValueForStringProp = (fieldNameAndLabel: string) => heartpointsMutableState.read().possibleNewOrganization[fieldNameAndLabel];
