export const updateZipFilter = (state, zip:string) => ({
    ...state,
    zipFilter: zip
})