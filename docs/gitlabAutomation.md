Gitlab Branch Create Automation
------------------------------------------

Because of the convenience of the `issueNumber`-`issue`-`name`-`here` syntax for naming branches, we have automated the method for creating branches based on existing issues. Please follow the below procedure for creating branches based on issues.


# Prerequisites

As you may have expected, there is some setup required before these commands can be executed. The below steps will outline everything you need to prepare to run these new processes.

## Get User Access Token

Every Heartpoints developer must recieve their own Gitlab access token to authenticate themselves. To get your access token, `Log in To Gitlab` and navigate to [this page](https://gitlab.com/profile/personal_access_tokens).

This will load a simple form for naming your access token, as well as specifying some options. The only options you need to worry about are the `Name` and the `Scopes` properties. 
  * `Name your token something simple and memorable`
  * `Check the 'api checkbox`
  * `Click "Create personal access token"`

This will generate a popup with your newly created access token.

### IMPORTANT! YOU WILL NOT BE ABLE TO SEE THIS TOKEN AGAIN ONCE THE POPUP IS CLOSED!

Copy this token to your clipboard and paste it in a text file for convenience, and to ensure it is not lost or forgotten. It will not be checked in as part of the repository.

### IMPORTANT! DO NOT PLACE THIS TEXT FILE IN THE HEARPOINTS REPOSITORY! 

We don't want keys to be public, so it should **never** appear in our open source respository. The following procedure will walk you through the process of adding your access token to an [environment variable](https://medium.com/chingu/an-introduction-to-environment-variables-and-how-to-use-them-f602f66d15fa).

## Add User Access Token as an Environment Variable on your Machine

In order to use your unique token with the Gitlab API, it must be stored as an environemnt variable. 

### Follow These Steps

  * Locate your locally stored Gitlab Access Token and copy it with `CMD + C`
  * Without copying anything else to your clipboard, run `./hp createGitlabToken`
  * If you see the terminal output `Token has been copied from clipboard to ~/heartpoints-gitlab-token.key` then the action was successful, and you can now use the Heartpoints gitlab cli

Verify that the key is being recognized by running the following command:

  `./hp verifyUserHasGitlabToken`

An output of `/Users/[your_user_name]/heartpoints-gitlab-token.key` indicates success!

# Execution

Simple bash commands have been created as a means of running the underlying Typescript logic behind the automation. Since these processes interact with the Gitlab server, Internet access is required.

## List All Open Issues

To see all currently open issues in the current sprint, run the following bash command:

  `./hp lab boardIssuesInOrder` or its alias `./hp bi`

A successful completion of this call will return an array containing basic information about all open issues. Each issue's `id`, `title`, and `description` will be printed `to the terminal` for your convenience. Examine this list to discover the next issue on which you plan to work, and `identify its issue id (iid)`.

## Create Branch From an Issue

In order to create a branch with this process, an `issue id (iid)` is required (ideally from the previous step, but can also be identified using the Gitlab site's UI).

Once you have the issue id for your next ticket, you can run the following bash command:

`./hp lab newBranchForIssue [issueNumber]` or its alias `./hp nb [issueNumber]`

where `issueNumber` is the issue id of the issue you identified as your next ticket from the previous step.

This process will do several things:
  * Create a remote branch named with the `issueNumber`-`issue`-`name`-`here` syntax
  * Create a Merge Request of the same name
  * Checkout and pull the latest master branch
  * Checkout the new branch based on the latest master

### IMPORTANT!
Please make sure your latest changes have been saved and pushed to the remote repository. This process will not allow you to perform ./hp lab newBranchFromIssue unless your working tree is clean. Avoid frustration by performing a ./hp g command before creating a new branch!