import * as React from 'react';
import { KeyboardDatePicker} from '@material-ui/pickers';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import { HPSearchFilter } from '../search/HPSearchFilter';
import { DateFilter } from '../volunteering/types/DateFilter';
import { DateRange } from "react-date-range";
import { dateRangeTheme } from '../buttons/data/dateRangeTheme';

type HasDateFilter = {dateFilter:DateFilter};
type HasDateFilterUpdateHandler = {dateFilterUpdateHandler:any}
type HasUpdateDateFilterEnabled = {updateDateFilterEnabled:any}
type VolOpportunitiesDateFilterProps = HasDateFilter & HasDateFilterUpdateHandler & HasUpdateDateFilterEnabled

export const dateRangeContainerStyle:React.CSSProperties = {
    pointerEvents: "none",
    fontFamily: "Roboto",
}

export const VolOpportunitiesDateFilter = ({dateFilter, dateFilterUpdateHandler, updateDateFilterEnabled}:VolOpportunitiesDateFilterProps) => {
    const { dateFilterStart, dateFilterEnd } = dateFilter;
    const numberOfMonthsInFilter = dateFilterEnd.diff(dateFilterStart, "months") + 1;
      
    return <HPSearchFilter onFilterClear={updateDateFilterEnabled} filterName="Date">
        <MuiPickersUtilsProvider utils={MomentUtils}>
            <KeyboardDatePicker
                autoOk
                disablePast
                style={{padding: "0", margin: "0"}}
                variant="inline"
                label="Start Date"
                format="MM/DD/YYYY"
                margin="normal"
                value={dateFilterStart}
                onChange={e => dateFilterUpdateHandler({...dateFilter, dateFilterStart: e})} />
            <KeyboardDatePicker
                autoOk
                disablePast
                style={{padding: "0", margin: "0"}}
                variant="inline"
                label="End Date"
                format="MM/DD/YYYY"
                margin="normal"
                value={dateFilterEnd}
                onChange={e => dateFilterUpdateHandler({...dateFilter, dateFilterEnd: e})} />
            <div style={dateRangeContainerStyle} tabIndex={-1}>
                <DateRange
                showMonthArrow={false}
                calendars={numberOfMonthsInFilter}
                startDate={dateFilterStart}
                endDate={dateFilterEnd}
                    theme={dateRangeTheme} />
            </div>
        </MuiPickersUtilsProvider>
    </HPSearchFilter>
}