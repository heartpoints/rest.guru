import { GroupOperator } from '../abstractAlgebra/GroupOperator';
import { Predicate } from '../predicates/Predicate';
import { mapFirstReducer } from './mapFirstReducer';
export const mapFirst = <T>(items: T[]) => (predicate: Predicate<T>) => (operator: GroupOperator<T>): Array<T> => items.reduce(mapFirstReducer(predicate)(operator), {found: false, result: [] }).result;
