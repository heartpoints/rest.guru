import { haProxyNamespace } from "./haProxyNamespace"
import * as k8s from "@pulumi/kubernetes"
import { defaultBackendPort } from "./defaultBackendPort"

const ingressDefaultBackendName = "ingress-default-backend";

export const ingressDefaultBackend = new k8s.apps.v1.Deployment(ingressDefaultBackendName, {
  metadata: {
    name: ingressDefaultBackendName,
    namespace: haProxyNamespace.metadata.name,
    labels: {
      run: ingressDefaultBackendName
    },
  },
  spec: {
    replicas: 1,
    selector: {
      matchLabels: {
        run: ingressDefaultBackendName
      }
    },
    template: {
      metadata: {
        labels: {
          run: ingressDefaultBackendName
        },
      },
      spec: {
        containers: [{
          name: ingressDefaultBackendName,
          image: "gcr.io/google_containers/defaultbackend:1.0",
          ports: [{
            containerPort: defaultBackendPort
          }]
        }]
      }
    }
  }
})
