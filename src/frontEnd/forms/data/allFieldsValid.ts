export const allFieldsValid = (array:Array<string | undefined>) => {
    return array.every(index => index == undefined);
}