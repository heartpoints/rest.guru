import { divideBy } from '../math/numericDivide';

export const arrayDivideByNumber = 
    (divisor: number) => 
    (a: Array<number>) => 
    a.map(divideBy(divisor));
