import { CastleRiskState } from "./CastleRiskState";
import { HasUpdateState } from '../../state/HasUpdateState';

export type HasUpdateStateForCastleRisk = HasUpdateState<CastleRiskState>;
