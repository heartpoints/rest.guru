import { Territory } from '../../types/Territory';
import { drawTerritoryWithoutDots } from './drawTerritoryWithoutDots';

export const drawNormalTerritory = 
    (t: Territory) => 
    drawTerritoryWithoutDots(t.empire.color)(t.boundary);
