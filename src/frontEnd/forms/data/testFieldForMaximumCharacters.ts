export const testFieldForMaximumCharacters = (field:string, maxChars:number):string | undefined => {
    const overMaximum = field.trim().length > maxChars;

    return overMaximum
        ? `Please Limit to ${maxChars} Characters`
        : undefined
}