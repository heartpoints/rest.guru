import { summer } from '../../../utils/math/summer';
import { black } from './black';
import { plusColors } from './plusColors';
export const sumColors = summer(plusColors)(black);
