import { spikeyShape } from '../geometry/spikeyShape';
import { armyPieceRadius } from './armyPieceRadius';

export const armyPiece = spikeyShape(4)(armyPieceRadius);
