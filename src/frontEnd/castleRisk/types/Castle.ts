import { HasTerritory } from "./HasTerritory";
import { HasOwner } from "./HasOwner";
import { HasEmpire } from "./HasEmpire";

export type Castle = HasEmpire & HasOwner & Partial<HasTerritory>;
