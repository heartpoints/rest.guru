export const updateUserZipCoordinates = (state, userZipCoordinates) => {
    return{
        ...state,
        userZipCoordinates
    }
}