const process = require("process")

process.stdin.setEncoding('utf8');

let json = ""
process.stdin.on('readable', () => {
  while ((chunk = process.stdin.read()) !== null) {
    json += chunk
  }
});

const v = setTimeout(
    () => null,
    5000
)

process.stdin.on('end', () => {
    console.log("")
    console.log("")
    console.log("")
    // console.log(json)
    console.log(JSON.stringify(JSON.parse(json).issues.map(i => i.iid), null, 3))
    console.log("")
    console.log("")
    clearTimeout(v)
});

