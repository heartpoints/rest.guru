import { Shape } from "../types/Shape"
import { setShadow } from "./setShadow";
import { RGBAColor } from "../types/RGBAColor";
import { drawPath } from "./drawPath";
import { multiplex } from "../../../utils/composition/multiplex";
import { doNothing } from "../../../utils/axioms/doNothing";
import { setStrokeStyle } from "./setStrokeStyle";
import { setFillStyle } from "./setFillStyle";

export const drawPolygon = 
    (useShadow:boolean) =>
    (strokeColor: RGBAColor) =>
    (fillColor: RGBAColor) => 
    (shape: Shape) => 
    shape.length > 2
        ? multiplex([
            setStrokeStyle(strokeColor),
            setFillStyle(fillColor),
            setShadow(useShadow),
            drawPath(shape),
            setShadow(false),
        ])
        : doNothing
