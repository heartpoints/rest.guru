import * as React from 'react';
import { Button } from '@material-ui/core';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { isMobile } from '../site/isMobile';

export type OnClick = () => any
export type HasOnClick = { onClick: OnClick }
export type HasLabel = { label: string }
export type PossibleThemeSpacing = { shouldUseThemeSpacing?: boolean }
export type PossibleInline = { shouldBeInline?: boolean }
export type PossibleChildren = { children?: any }
export type PossibleDisabled = Partial<HasDisabled>
export type HasDisabled = { disabled: boolean }
export type HasColor = {color: string}
export type HPButtonProps = HasOnClick & HasLabel & PossibleThemeSpacing & PossibleInline & PossibleChildren & PossibleDisabled & Partial<HasColor>

export const HPButton = (props:HPButtonProps) => {
  const { onClick, children, label, shouldUseThemeSpacing = true, shouldBeInline = false, disabled = false, color = "red"} = props;
  const spacing = shouldUseThemeSpacing ? 1 : 0;

  const { display, width } = shouldBeInline && isMobile()
    ? {display: "inline", width: "100%" }
    : {display: "auto", width: "auto" }

  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      button: {
        display,
        width,
        margin: theme.spacing(spacing),
        backgroundColor: color,
        color: 'white',
        '&:hover': {
          backgroundColor: '#ff5252'
        }
      }
    }),
  );
  
  const classes = useStyles();

  return <Button className={classes.button} onClick={onClick} variant="contained" disabled={disabled}>
      {label} 
      <div style={{display: "inline-block", verticalAlign: "middle"}}>{children}</div>
  </Button>
}