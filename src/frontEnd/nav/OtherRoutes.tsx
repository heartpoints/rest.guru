import * as React from "react";

import { HomePage } from "../homePage/HomePage";
import { SearchBar as VolunteeringSearchBar } from "../volunteering/react/SearchBar";
import { CastleRisk } from "../castleRisk/general/CastleRisk";
import { Router } from "./Router";
import { RestGuru } from "../RestGuru/RestGuru";
import { Gsap } from '../animation/Gsap';
import { FirebaseAuthLogin } from '../site/FirebaseAuthLogin';

export const OtherRoutes = (url, props, router: Router) => router
  .case("/", <HomePage {...props} />)
  .case("/dev", <FirebaseAuthLogin {...props} />)
  .case("/castleRisk", <CastleRisk {...props.castleRisk} {...props} />)
  .case("/rest-guru", <RestGuru {...props} />)
  .case("/volunteering/search", <VolunteeringSearchBar {...props} />)
  .case("/animationExample", <Gsap />);
