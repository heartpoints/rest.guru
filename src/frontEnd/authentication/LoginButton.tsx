import * as React from 'react';
import { HPButton } from '../forms/HPButton';

export const LoginButton = ({clickHandler}) => {
    return <HPButton color="blue" onClick={clickHandler} label="Log In" />
}