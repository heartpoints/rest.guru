import { Organization } from "./organization";
import moment from 'moment';

export const defaultOrganizations:Array<Organization> = [
    {
        href: "/organizations/1",
        imageThumbnailURL: "/images/aclu-logo.gif",
        title: 'ACLU',
        mission: 'The American Civil Liberties Union (ACLU) is a nonprofit organization whose stated mission is "to defend and preserve the individual rights and liberties guaranteed to every person in this country by the Constitution and laws of the United States."',
        homepage: "https://www.aclu.org",
        creatorEmail: "ptmetsch@gmail.com",
        creatorName: "Patrick Metsch",
        creatorProfilePicture: "",
        volOpportunities: [
            {
                jobID: '11',
                jobTitle: 'Data Driven: Advancing Racial Justice & Police Accountability in San Diego',
                jobDescription: "On December 3, the ACLU of San Diego & Imperial Counties will release a commissioned report that analyzes traffic and pedestrian stop data from the San Diego Police Department and San Diego County Sheriff’s Department. You are invited to attend a special event to learn about this new report and to explore ways to address racially-biased policing and police violence, improve accountability and make San Diego more just for all. The report’s author, Samuel Sinyangwe, data scientist and co-founder of Campaign Zero, will present on the report’s findings and recommendations.",
                startDate: moment('2019-12-03 18:00:00'),
                endDate: moment('2019-12-03 22:00:00'),
                heartpointsReward: 25,
                coordinates: {lat: 41.505247, lng: -81.655760},
                address: {streetAddress: "4506 Chester Ave", suite: "", city: "Cleveland", state: "OH", zip: "44103"}
            },
            {
                jobID: '12',
                jobTitle: 'School-to-Prison Pipeline Town Hall',
                jobDescription: 'Schools should be places where students are included, respected, and supported. But in New York, students lose hundreds of thousands of days in the classroom each year because of suspensions, often for normal youthful behavior. These punishments disproportionately impact Black and Latinx students and those with disabilities. They can also push students into the school-to-prison pipeline. Join students, organizers, advocates, and policy experts to discuss the impact that policing has on students in New York City. This event is part of a series of town halls taking place across New York State.',
                startDate: moment('2019-12-04 18:00:00'),
                endDate: moment('2019-12-04 22:00:00'),
                heartpointsReward: 25,
                coordinates: {lat: 41.505247, lng: -81.655760},
                address: {streetAddress: "4506 Chester Ave", suite: "", city: "Cleveland", state: "OH", zip: "44103"}
            },
        ]
    },
    {
        href: "/organizations/2",
        imageThumbnailURL: "/images/billAndMelindaGatesFoundation.png",
        title: 'Bill & Melinda Gates Foundation',
        mission: 'Guided by the belief that every life has equal value, the Bill & Melinda Gates Foundation works to help all people lead healthy, productive lives. In developing countries, it focuses on improving people\'s health and giving them the chance to lift themselves out of hunger and extreme poverty.',
        homepage: "https://www.gatesfoundation.org",
        creatorEmail: "tom@tommysullivan.me",
        creatorName: "Tommy Sullivan",
        creatorProfilePicture: "",
        volOpportunities: [
            {
                jobID: '21',
                jobTitle: 'Giving Marketplace',
                jobDescription: 'Don’t miss the holiday giving event of the season! Find unique, ethically sourced gifts at dozens of pop-up shops that support women’s and girls’ empowerment, refugees, global health, and the environment. Enjoy family friendly activities, performances by the Pete Martin Mandolin Trio and Garfield High School Jazz Band. Then discover ways to take action on issues you care about. Join & share our Facebook event!',
                startDate: moment('2019-12-06 10:00:00'),
                endDate: moment('2019-12-07 17:00:00'),
                heartpointsReward: 25,
                coordinates: {lat: 38.900925, lng: -77.030606},
                address: {streetAddress: "1300 I St NW", suite: "", city: "Washington", state: "DC", zip: "20005"}
            },
            {
                jobID: '22',
                jobTitle: 'Youth-Led Gallery Discussion',
                jobDescription: 'Participate in a youth-led dialogue exploring how young people are creating change in their communities, and discover local connections to the social movements represented in the exhibit "We the Future." Ideal for young people and adults.',
                startDate: moment("2019-12-07 13:00:00"),
                endDate: moment("2019-12-07 20:00:00"),
                heartpointsReward: 25,
                coordinates: {lat: 38.900925, lng: -77.030606},
                address: {streetAddress: "1300 I St NW", suite: "", city: "Washington", state: "DC", zip: "20005"}
            },
            {
                jobID: '23',
                jobTitle: 'Demanding Intersectionality in Disability Justice Movements',
                jobDescription: 'Join Lydia X. Z. Brown in conversation with local leaders on disability justice in the movement for racial justice. Lydia is a community organizer, writer, speaker, educator, attorney, and advocate for disability justice. They work to disrupt and end all forms of systemic and interpersonal violence targeting disabled people at the margins of the margins. Co-hosted with exhibit partner Amplifier. Free. PANELISTS: ChrisTiana ObeySumner is a Black and Indigenous, Queer, and Multiply disabled person. They are a DEI and Intersectional Disability advocate and consultant and founded Epiphanies of Equity LLC and The Eleanor Elizabeth Institute for Black Empowerment.   NEVE is a choreographer, writer, composer, and multidisciplinary performance artist based in Duwamish and other Unceded Coast Salish Territories.   Seema Bahl is a faculty member in the Sociology Department at Bellevue College focusing in the areas of gender, race, feminism, and disability.   Shain A. M. Neumeier is an autistic activist for disability, youth, and queer justice and liberation. They are a solo attorney in Western Massachusetts whose practice focuses on disability and transgender law.',
                startDate: moment("2019-12-12 17:30:00"),
                endDate: moment("2019-12-12 21:00:00"),
                heartpointsReward: 25,
                coordinates: {lat: 38.900925, lng: -77.030606},
                address: {streetAddress: "1300 I St NW", suite: "", city: "Washington", state: "DC", zip: "20005"}
            }
        ]
    },
    {
        href: "/organizations/3",
        imageThumbnailURL: "/images/heartpointsIcon.png",
        title: 'Heartpoints.org',
        mission: 'Heartpoints.org and its members understand that today\'s currencies all have one thing in common: their value derives from exploitation. Our mission is to bring about a "currency of good" whose value is linked to the health and well-being of every individual on Earth; and whose exchange is fundamentally enabled by open-source blockchain technologies that enable unprecedented, decentralized traceability into the ethics of each transaction in the supply chain of any product or service traded within the heartpoints ecosystem',
        homepage: "http://www.heartpoints.org",
        creatorEmail: "tom@tommysullivan.me",
        creatorName: "Tommy Sullivan",
        creatorProfilePicture: "",
        volOpportunities: [
            {
                jobID: '31',
                jobTitle: 'Help Us Build Something Amazing',
                jobDescription: 'Heartpoint.org is an open source platform dedicated to decentralized, exploitation-free altruism. Presently it is just a concept and a bare-bones website, but with your help we can make it so much more. We are looking for volunteers to donate their time towards all facets of the platform. UX/UI development, system architecture, database administration, public outreach, and so much more. Help us turn the "currency of good" into more than just a dream.',
                startDate: moment("2019-01-01 0:00:00"),
                endDate: moment("2022-12-31 23:59:59"),
                heartpointsReward: 25,
                coordinates: {lat: 37.760031, lng: -122.434633},
                address: {streetAddress: "549 Castro St", suite: "", city: "San Francisco", state: "CA", zip: "94114"}
            }
        ]
    },
    {
        href: "/organizations/4",
        imageThumbnailURL: "/images/stAnthonys.jpeg",
        title: 'St. Anthony\'s - San Francisco',
        mission: 'St. Anthony’s mission is to feed, heal, shelter, clothe, lift the spirits of those in need, and create a society in which all persons flourish. This mission is guided by five values: healing, community, personalism, justice, and gratitude.',
        homepage: "https://www.stanthonysf.org",
        creatorEmail: "tastulae@mail.usf.edu",
        creatorName: "John Doe",
        creatorProfilePicture: "",
        volOpportunities: [
            {
                jobID: '41',
                jobTitle: 'DR Meal Service',
                jobDescription: 'Dining Room Meal Service is more than a meal: it is building community, a place to share stories and smiles. Working with 30-40 fellow volunteers, Individual Dining Room Servers help prepare and provide around 2000 meals each day. Service includes: delivering meals, preparing food and drinks, clearing tables, and helping with other tasks as needed. Volunteers provide our guests with a nutritious meal and a much needed sense of belonging.',
                startDate: moment("2019-01-01 9:15:00"),
                endDate: moment("2022-12-31 13:45:00"),
                heartpointsReward: 25,
                coordinates: {lat: 37.781974, lng: -122.413204},
                address: {streetAddress: "150 Golden Gate Ave", suite: "", city: "San Francisco", state: "CA", zip: "94102"}
            },
            {
                jobID: '42',
                jobTitle: 'FCP Clothing Distribution',
                jobDescription: "The Free Clothing Program serves approximately 150 guests a day in Men’s Service, Women's Service, and Family Service. It is our goal to distribute the highest quality of clothing possible to ensure that our guests have a positive and dignified shopping experience. Clothing Program volunteers may work in our store and our processing space. While processing volunteers will be sorting, quality checking, and hanging clothing donations for distribution to our guests and help prepare our free clothing store. While in the store, volunteers assist guests with shopping, helping to find sizes, colors and styles. Volunteers help keep the store organized and presentable during services. ",
                startDate: moment("2019-01-01 13:15:00"),
                endDate: moment("2022-12-13 16:00:00"),
                heartpointsReward: 25,
                coordinates: {lat: 37.781974, lng: -122.413204},
                address: {streetAddress: "150 Golden Gate Ave", suite: "", city: "San Francisco", state: "CA", zip: "94102"}
            }
        ]
    },
    {
        href: "/organizations/5",
        imageThumbnailURL: "/images/st_jude_logo.jpg",
        title: "St. Jude\'s Children\'s Research Hospital",
        mission: "The mission of St. Jude Children\’s Research Hospital is to advance cures, and means of prevention, for pediatric catastrophic diseases through research and treatment. Consistent with the vision of our founder Danny Thomas, no child is denied treatment based on race, religion or a family\'s ability to pay.",
        homepage: "https://www.stjude.org",
        creatorEmail: "ptmetsch@gmail.com",
        creatorName: "Patrick Metsch",
        creatorProfilePicture: "",
        volOpportunities: [
            {
                jobID: '481516',
                jobTitle: 'Phone Bank Volunteer',
                jobDescription: "The purpose of this role is to answer calls placed by potential donors, interact with the potential donors and assist with any questions to become donors through either ticket sales or Partner in Hope. The Phone Bank Volunteer is responsible for answering phones through headset, communicating with donors and representing St. Jude, and entering donor information on a laptop computer. Please be able to work and hear comfortably in a loud environment, communicate fluently in English, and remain attentive to detail with a commitment to accuracy.",
                startDate: moment("2020-02-27 6:00:00"),
                endDate: moment("2020-02-27 09:00:00"),
                heartpointsReward: 25,
                coordinates: {lat: 35.154420, lng: -90.042778},
                address: {streetAddress: "262 Danny Thomas Pl", suite: "", city: "Memphis", state: "TN", zip: "38105"}
            },
            {
                jobID: '2342',
                jobTitle: 'Phone Bank Volunteer',
                jobDescription: "The purpose of this role is to answer calls placed by potential donors, interact with the potential donors and assist with any questions to become donors through either ticket sales or Partner in Hope. The Phone Bank Volunteer is responsible for answering phones through headset, communicating with donors and representing St. Jude, and entering donor information on a laptop computer. Please be able to work and hear comfortably in a loud environment, communicate fluently in English, and remain attentive to detail with a commitment to accuracy.",
                startDate: moment("2020-04-09 6:00:00"),
                endDate: moment("2020-04-09 09:00:00"),
                heartpointsReward: 25,
                coordinates: {lat: 35.154420, lng: -90.042778},
                address: {streetAddress: "262 Danny Thomas Pl", suite: "", city: "Memphis", state: "TN", zip: "38105"}
            },
            {
                jobID: "815",
                jobTitle: "Event Setup",
                jobDescription: "The purpose of this role is to assist with setting up the event prior to the event. Key responsibilities include following instructions to set up items for the event. Duties may include but are not limited to decorations, equipment set-up, set up and take down tables/chairs, moving tables and equipment, organizing and clean-up. Please be detail-oriented, demonstrating attention to detail as well as the ability to work with a team. Must be comfortable with lifting heavy items and equipment.",
                startDate: moment("2020-04-03 21:00:00"),
                endDate: moment("2020-04-03 23:00:00"),
                heartpointsReward: 25,
                coordinates: {lat: 35.154420, lng: -90.042778},
                address: {streetAddress: "262 Danny Thomas Pl", suite: "", city: "Memphis", state: "TN", zip: "38105"}
            },
            {
                jobTitle:"Greeter",
                jobDescription: "The purpose of this role is to greet and engage guests and potential donors. Key responsibilities include welcoming guest/donors upon arrival, directing guests/donors to registration/check-in area, answering questions donors have on the event and providing them with additional information as needed, and counting number of guests that enter the event. Please be able to communicate courteously with guests/donors, demonstrate strong interpersonal skills and ability to maintain professionalism. Must be punctual and able to stand for duration of shift.",
                jobID: "guid8993360188",
                startDate: moment("2020-04-04 22:00:00.00"),
                endDate: moment("2020-04-05 00:30:00.00"),
                heartpointsReward: 25,
                coordinates: {lat: 35.154420, lng: -90.042778},
                address: {streetAddress: "262 Danny Thomas Pl", suite: "", city: "Memphis", state: "TN", zip: "38105"}
            }
        ]
    },
    {
        href: "/organizations/6",
        imageThumbnailURL: "/images/project_sunshine_logo.jpg",
        title: 'Project Sunshine',
        mission: "Project Sunshine recruits and trains volunteers – community members, college students, corporations, and youth – to deliver activities. Our programs promote creative expression, socialization, and learning. Most of all, they let young patients act and feel like kids or teens during emotionally and physically challenging times. We partner with medical facilities across the United States and in four international locations to meet the psychosocial and developmental needs of pediatric patients. Project Sunshine’s programming supports these critical needs by providing increased opportunities for play and authentic engagement in the medical environment – restoring a crucial sense of normalcy for patients and their families.",
        homepage: "https://www.projectsunshine.org",
        creatorEmail: "nishijain2512@gmail.com",
        creatorName: "Nishi Jain",
        creatorProfilePicture: "/images/profileImage1.png",
        volOpportunities: [
            {
                jobID: '61',
                jobTitle: 'Register Your College',
                jobDescription: "Project Sunshine has active college chapters on over 50 campuses nationwide. College volunteers keep pediatric patients company and provide fun enrichment activities while also offering respite to their parents. Given their close proximity in age and approachability, college volunteers are uniquely positioned to serve as role models to children facing medical challenges. Through volunteering, college students develop leadership skills, engage in service opportunities, and raise awareness about Project Sunshine across the country. Chapters participate in volunteer program activities, organize Sending Sunshine events, and fundraise on campus to support Project Sunshine’s mission. The enthusiasm of our college volunteers carries out the legacy of Project Sunshine’s then-college student founder.",
                startDate: moment("2019-01-01 0:00:00"),
                endDate: moment("2022-12-31 23:59:59"),
                heartpointsReward: 25,
                coordinates: {lat: 40.7512663, lng: -73.9731576},
                address: {streetAddress: "211 E 43rd St #401", suite: "", city: "New York", state: "NY", zip: "10017"}
            },
            {
                jobID: '62',
                jobTitle: 'Give Today and Brighten a Child\'s Tomorrow',
                jobDescription: 'Over 3 million children are admitted to hospitals in the United States each year. They miss out on crucial opportunities to socialize with friends, spend time with family members, learn key lessons in school, and just be a kid. Project Sunshine\’s programs fill a gap in pediatric patients\’ lives by bringing the joys of the outside world into the hospital. Because of you, children facing medical challenges have a chance to smile. Help us spread sunshine today.',
                heartpointsReward: 25,
                startDate: moment("2019-01-01 0:00:00"),
                endDate: moment("2022-12-31 23:59:59"),
                coordinates: {lat: 40.7512663, lng: -73.9731576},
                address: {streetAddress: "211 E 43rd St #401", suite: "", city: "New York", state: "NY", zip: "10017"}
            }
        ]
    },
    {
        href: "/organizations/7",
        imageThumbnailURL: "/images/hands_on_atl_logo.jpg",
        title: 'Hands On Atl',
        mission: "We mobilize the Atlanta community to tackle our city’s most pressing needs.",
        homepage: "https://www.handsonatlanta.org/",
        creatorEmail: "nishijain2512@gmail.com",
        creatorName: "Nishi Jain",
        creatorProfilePicture: "/images/profileImage1.png",
        volOpportunities: []
    }
];