export const scale = 
    (scaleFactor: number) => 
    (nums: number[]) => 
    nums.map(n => n * scaleFactor)
