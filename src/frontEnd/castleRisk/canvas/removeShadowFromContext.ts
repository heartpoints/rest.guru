export const removeShadowFromContext = 
    (context: CanvasRenderingContext2D) => {
        context.shadowOffsetX = 0;
        context.shadowOffsetY = 0;
        context.shadowBlur = 0;
        context.shadowColor = "transparent";
    }
