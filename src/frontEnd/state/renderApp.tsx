import * as React from "react";
import * as ReactDOM from "react-dom";
import { updateStateToUseOnBackAndForwardNav } from "../nav/updateStateToUseOnBackAndForwardNav";
import { Site } from "../site/Site";
import { rootSiteElement } from "./rootSiteElement";
import { sitePropsFromState } from "./sitePropsFromState";
import { heartpointsMutableState } from "./heartpointsMutableState";

export const renderApp = () => {
    window.onresize = () => renderApp()
    updateStateToUseOnBackAndForwardNav()
    
    const siteProps = {
        ...sitePropsFromState(heartpointsMutableState.read()),
    }

    ReactDOM.render(
        <Site {...siteProps} />,
        rootSiteElement()
    );
};
