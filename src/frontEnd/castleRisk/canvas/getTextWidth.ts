export const getTextWidth = 
    (text: string) => 
    (context: CanvasRenderingContext2D) => 
    context.measureText(text).width;
