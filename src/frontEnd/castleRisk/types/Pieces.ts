import { Piece } from "./Piece";
export type Pieces = Array<Piece>;
