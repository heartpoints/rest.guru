import { RGBAColor } from '../types/RGBAColor';
export type Colors = Array<RGBAColor>;
