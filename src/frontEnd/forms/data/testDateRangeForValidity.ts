import moment = require("moment");

export const testDateRangeForValidity = (start:moment.Moment, end:moment.Moment) => {
    return start.isAfter(end, "minute")
        ? "End time must be after start time"
        : undefined
}