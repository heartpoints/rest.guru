import { udpateStateAndRerender } from '../../../nav/udpateStateAndRerender';
import { heartpointsMutableState } from '../../../state/heartpointsMutableState';
export const setValueForStringProp = (propName: string) => (value: string) => {
    const previousState = heartpointsMutableState.read();
    udpateStateAndRerender({
        ...previousState,
        possibleNewOrganization: {
            ...previousState.possibleNewOrganization,
            [propName]: value
        }
    });
};
