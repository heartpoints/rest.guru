import * as React from 'react';
import EditIcon from '@material-ui/icons/Edit';
import { IconButton, Tooltip } from '@material-ui/core';

export const EditButton = ({ navTo, onClick, buttonTitle }) => <Tooltip title={buttonTitle} placement="right">
    <IconButton onClick={onClick}>
        <EditIcon />
    </IconButton>
</Tooltip>

