import * as k8s from "@pulumi/kubernetes"
import { appName } from "./appName";
import { appLabels } from "./appLabels";
import { namespace } from "./heartpointsNamespace";
import { dockerImage } from "../container/dockerImage";
import { probeCommons } from "./probeCommons";

export const heartpointsDeployment = new k8s.apps.v1.Deployment(appName, {
    metadata: {
        labels: appLabels,
        namespace: namespace.metadata.name,
    },
    spec: {
        selector: { matchLabels: appLabels },
        replicas: 1,
        template: {
            metadata: {
                labels: appLabels,
            },
            spec: {
                containers: [
                    {
                        name: appName,
                        image: dockerImage.id,
                        imagePullPolicy: "IfNotPresent",
                        resources: {
                            requests: {
                                cpu: "15m",
                                memory: "350Mi"
                            },
                        },
                        readinessProbe: {
                            ...probeCommons,
                            periodSeconds: 5
                        },
                        livenessProbe: {
                            ...probeCommons,
                            periodSeconds: 10
                        }
                    }
                ],
            },
        },
    },
})
