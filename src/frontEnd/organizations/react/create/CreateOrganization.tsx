import * as React from 'react';
import { Page } from '../../../page/Page';
import { PageTitle } from '../../../page/PageTitle';
import { Space } from '../../../page/Space';
import { OrgAddEdit } from '../general/OrgAddEdit';
import { defaultOrgLogoSrc } from '../../data/defaultOrgLogoSrc';
import { stringFieldMatchingLabelAndPlaceholder } from './stringFieldMatchingLabelAndPlaceholder';
import { stringFieldForCreate } from './stringFieldForCreate';

export const CreateOrganization = (props:any) => {
    const { addNewOrganization, userCredentials = {email: ""}, url } = props;
    const { email } = userCredentials

    const title = stringFieldMatchingLabelAndPlaceholder("title")
    const homepage = stringFieldMatchingLabelAndPlaceholder("homepage")
    const mission = stringFieldMatchingLabelAndPlaceholder("mission")
    const imageThumbnailURL = stringFieldForCreate("imageThumbnailURL")(defaultOrgLogoSrc)

    const fields = {
        title,
        homepage,
        mission,
        imageThumbnailURL,
    }

    const orgAddEditProps = {
        ...fields,
        buttonLabel: "Create Organization",
        clickHandler: () => addNewOrganization(email)
    }

    return <Page requiresLoggedIn userCredentials={userCredentials}>
        <PageTitle>Create Organization</PageTitle>
        <OrgAddEdit {...orgAddEditProps} />
        <Space />
    </Page>
}