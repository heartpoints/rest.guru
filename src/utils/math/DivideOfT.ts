export type Divide<T, S> = (divisor: T) => (itemToDivide: S) => S;
