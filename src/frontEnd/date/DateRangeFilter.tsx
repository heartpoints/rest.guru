import * as React from 'react';
import MomentUtils from '@date-io/moment';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import { Space } from '../page/Space';
import { isMobile } from '../site/isMobile';

export const DateRangeFilter = ({startDate, onStartDateChange, endDate, onEndDateChange, block = false}) => {

    const { width, margin } = !block
        ? {width: "100%", margin: "0px"}
        : {width: "300px", margin: "8px"}

    return <MuiPickersUtilsProvider utils={MomentUtils}>
        <KeyboardDatePicker
            style={{width, margin: `0 ${margin}`}}
            autoOk
            disablePast
            variant="inline"
            inputVariant="filled"
            label="Start Date"
            format="MM/DD/YYYY"
            margin="normal"
            value={startDate}
            onChange={onStartDateChange} />{!block && <Space />}
        <KeyboardDatePicker
            style={{width, margin: `0 ${margin}`}}
            autoOk
            disablePast
            variant="inline"
            inputVariant="filled"
            label="End Date"
            format="MM/DD/YYYY"
            margin="normal"
            value={endDate}
            onChange={onEndDateChange} />
    </MuiPickersUtilsProvider>
}