import { territories } from './territories';
import { Empire } from '../../types/Empire';
import { notInTerritories } from './notInTerritories';
export const outsideTerritories = (empire: Empire) => territories().filter(notInTerritories(empire.territories));
