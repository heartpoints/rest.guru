import moment, { Moment } from "moment-timezone";
import jstz from 'jstz';

type Granularity = "year" | "month" | "day" | "hour" | "minute" | "second";

export const userLocale = jstz.determine();

export const formatDateWithoutMutate = (date:Moment, format:string) => 
    moment.tz(date, userLocale.name()).format(format)

export const returnOneDateIfSame = (startDate:Moment, endDate:Moment, granularity:Granularity, format="MM/DD/YY") =>
    startDate.isSame(endDate, granularity)
        ? `${formatDateWithoutMutate(startDate, format)}`
        : `${formatDateWithoutMutate(startDate, format)} - ${formatDateWithoutMutate(endDate, format)}`