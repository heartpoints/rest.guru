import { User } from "firebase";
import Cookies from 'js-cookie';
import { userCredentialsCookieKey } from './userCredentialsCookieKey';
import { userCredentialsCookieString } from '../authentication/userCredentialsCookieString';

export const onFirebaseLoginComplete = (state, authResult:User) => {
    Cookies.set(userCredentialsCookieKey, authResult);
    const postLoginState = {
        ...state,
        shouldShowCelebration: true,
        userCredentials: userCredentialsCookieString()
    }

    return postLoginState;
}