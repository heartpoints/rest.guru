export const testFieldForMinimumCharacters = (field:string, minChars:number = 3):string | undefined => {
    const underMinimum = field == undefined || (field.trim() == "" || field.trim().length < minChars);

    return underMinimum
        ? `Please enter at least ${minChars} characters`
        : undefined
}