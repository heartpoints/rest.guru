import { canvasWidth } from '../config/canvasWidth';
import { canvasHeight } from '../config/canvasHeight';
export const boardStyle = { width: canvasWidth, height: canvasHeight };
