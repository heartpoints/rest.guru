import { RGBAColor } from '../types/RGBAColor';
import { minimum } from '../../../utils/math/minimum';
import { maximum } from '../../../utils/math/maximum';
export const colorSaturation = ([r, g, b]: RGBAColor): number => {
    const rgbComponents = [r, g, b];
    const max = maximum(rgbComponents);
    const min = minimum(rgbComponents);
    const denominator = max + min;
    return denominator == 0
        ? 0
        : (max - min) / denominator;
};
