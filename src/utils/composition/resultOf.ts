import { Provider } from '../axioms/Provider';
export const resultOf = <T>(producer: Provider<T>) => producer();
