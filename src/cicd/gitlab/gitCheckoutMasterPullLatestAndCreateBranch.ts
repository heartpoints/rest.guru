import { runBashCommand } from './runBashCommand';
export const gitCheckoutMasterPullLatestAndCreateBranch = async (branchName: string) => {
    await runBashCommand("git fetch --all");
    await runBashCommand("git checkout master");
    await runBashCommand("git pull origin master");
    await runBashCommand(`git checkout ${branchName}`);
};
