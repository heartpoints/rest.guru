import { Mapper } from '../axioms/Mapper';
export type GroupOperator<T> = Mapper<T, T>;
