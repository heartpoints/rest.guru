import * as React from 'react';
import { IconButton, Tooltip } from '@material-ui/core';
import ClearIconRounded from '@material-ui/icons/ClearOutlined';

export const ClearFilterButton = ({filterName, buttonClickHandler}) => <Tooltip title={`Clear ${filterName} Filter`} placeholder="right">
    <IconButton onClick={buttonClickHandler}>
        <ClearIconRounded />
    </IconButton>
</Tooltip>