import { pointRadius } from '../config/pointRadius';
import { Point } from "../types/Point";
import { RGBAColor } from "../types/RGBAColor";
import { setFillStyle } from "./setFillStyle";

export const drawPoint = 
    (fillColor: RGBAColor) =>
    ({ x, y }: Point) => 
    (context: CanvasRenderingContext2D) => 
    {
        setFillStyle(fillColor)(context)
        context.beginPath();
        context.arc(x, y, pointRadius, 0, 2 * Math.PI);
        context.closePath()
        context.fill();
    };
