import { Typography } from '@material-ui/core';
import "animate.css/animate.min.css";
import gsap, { Power3 } from 'gsap';
import * as React from 'react';
import ScrollAnimation from 'react-animate-on-scroll';
import { Parallax } from 'react-parallax';
import { doNothing } from '../../utils/axioms/doNothing';
import { Page } from '../page/Page';
import { PageTitle } from '../page/PageTitle';
import { isMobile } from '../site/isMobile';
import './gsap.css';

export const Gsap = () => {

    const parallaxForegroundHeaderVariant = isMobile() ? "h5" : "h2";
    const parallaxForegroundSubtitleVariant = isMobile() ? "button" : "h4";

    const demoAnimation = () =>
        gsap.to(['#logo', "#header", "#subtitle", "#button"], {top: 0, opacity: 1, ease: Power3.easeInOut, duration: 1, stagger: .5 })

    const demoAnimation2 = () => 
        gsap.to(['#give', '#heart', '#take'], {opacity: 1, left: 0, duration: 1, ease: Power3.easeInOut, stagger: .5, onComplete: () => 
            gsap.to(['#give', '#heart', '#take'], {opacity: 0, width: '100%', top: -100, duration: .5, delay: 2})
        });

    type ChainableTween = (onComplete:()=>any) => () => any
    type ChainableTweens = ChainableTween[]
    const chainableTween = 
        (target:string) =>
        (config:any):ChainableTween =>
        (onComplete:()=>any) =>
        () => 
        gsap.to(target, {...config, onComplete})

    const heart2T = chainableTween('#heart2')({duration: .3, width: "95px", ease: Power3.easeIn})
    const heart22T = chainableTween('#heart2')({duration: .3, opacity: 1, width: "120px", ease: Power3.easeOut})
    const card3T = chainableTween('#card3')({duration: .5, rotate: -10, ease: Power3.easeInOut })
    const card32T = chainableTween('#card3')({ duration: .7, width: "250px", height:"250px", ease: Power3.easeIn })
    const card2T = chainableTween('#card2')({ rotation: -300, left: 2000, duration: .8, ease: Power3.easeOut }) 
    const card1T = chainableTween('#card1')({ rotation: 300, left: -1000, duration: .8, ease: Power3.easeOut })
    const chooseT = chainableTween('#choose')({opacity: 1, duration: 1, ease: Power3.easeInOut })

    const sequenceOfAnimations = [
        chooseT,
        card1T,
        card2T,
        card32T,
        card3T,
        heart22T,
        heart2T
    ]

    const chainAnimations = 
        (chainableTweens:ChainableTweens) =>
        chainableTweens.reverse().reduce(
            (acc, cur) => cur(acc),
            doNothing as any
        )


    const demoAnimation3 = chainAnimations(sequenceOfAnimations)

    return <Page>
        <PageTitle>GSAP Animation</PageTitle>
        <ScrollAnimation animateIn="fadeIn" duration={1} animateOnce={true} afterAnimatedIn={demoAnimation}>
            <h1>Myes</h1>
            <div className="animation">
                <div style={{width: "100%", textAlign: "center", backgroundImage: "url(images/volunteerBackground.png)", backgroundSize: "cover", overflow: "hidden", backgroundPosition: "center"}}>
                    <img id="logo" src="/images/logo_mobile.png" />
                    <h1 className="white-text" id="header">Heartpoints.org</h1>
                    <h4 className="white-text" id="subtitle">The currency of good</h4>
                    <button id="button">help us build something amazing</button>
                </div>
            </div>
        </ScrollAnimation>
        <div style={{width: "100%", height: "1000px"}}></div>
        <PageTitle>GSAP Animation 2</PageTitle>
        <ScrollAnimation animateIn="fadeIn" duration={1} animateOnce={true} afterAnimatedIn={demoAnimation2}>
            <h1>Mnooo</h1>
            <div className="animation2">
                <div style={{width: "100%", height: "400px", textAlign: "center", overflow: "hidden", backgroundImage: "linear-gradient(to bottom right, red, white)"}}>
                    <h1 className="white-text" id="give">It's dangerous to go alone...</h1>
                    <img id="heart" src="/images/zeldaHeart.png" />
                    <h1 className="white-text" id="take">...take this!</h1>
                </div>
            </div>
        </ScrollAnimation>
        <div style={{width: "100%", height: "1000px"}}></div>
        <PageTitle>GSAP Animation 3</PageTitle>
        <ScrollAnimation animateIn="fadeIn" duration={1} animateOnce={true} afterAnimatedIn={demoAnimation3}>
            <div className="animation3">
                <div style={{width: "100%", height: "450px", textAlign: "center", backgroundImage: "linear-gradient(to bottom right, red, white)"}}>
                    <h1 className="white-text" id="choose">Choose who you Help</h1>
                    <div className="card-holder">
                        <div className="card" id="card1" style={{backgroundImage: "url(images/card1.png)"}}></div>
                        <div className="card" id="card2" style={{backgroundImage: "url(images/card2.png)"}}></div>
                        <div className="card" id="card3" style={{backgroundImage: "url(images/card3.png)"}}></div>
                        <img id="heart2" src="/images/zeldaHeart.png" />
                    </div>
                </div>
            </div>
        </ScrollAnimation>
        <div style={{width: "100%", height: "1000px"}}></div>
        <PageTitle>Parallax Animation</PageTitle>
        <ScrollAnimation animateIn="fadeIn" duration={1} animateOnce={true}>
            <div>
                <Parallax
                    blur={8}
                    bgImage={'images/volunteerBackground.png'}
                    bgImageAlt="background"
                    strength={300}>
                    <div className="foreground">
                        <div>
                            <Typography variant={parallaxForegroundHeaderVariant}>Heartpoints.org</Typography>
                            <Typography variant={parallaxForegroundSubtitleVariant}>This is a parralax component example</Typography>
                        </div>
                    </div>
                </Parallax>
            </div>
        </ScrollAnimation>
        <div style={{width: "100%", height: "1000px"}}></div>
    </Page>    
}