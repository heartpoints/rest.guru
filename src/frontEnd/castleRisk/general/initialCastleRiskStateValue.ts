import { Phase } from "../types/Phase";
import { originPoint } from "../geometry/originPoint";

export const initialCastleRiskStateValue = {
    phase: Phase.AddPlayers,
    players: [],
    newPlayer: undefined,
    admin: {
        currentlyDrawingShape: [],
    },
    mousePoint: originPoint
};
