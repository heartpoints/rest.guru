import { renderApp } from "../state/renderApp";
import { heartpointsMutableState } from "../state/heartpointsMutableState";

export const udpateStateAndRerender = <S>(state: S) => {
    heartpointsMutableState.write(state);
    renderApp();
};
