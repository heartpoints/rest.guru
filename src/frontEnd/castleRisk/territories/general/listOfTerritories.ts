import { List } from '../../../../utils/list/List';
import { territories } from './territories';
export const listOfTerritories = List(territories());
