import * as React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import { colorStyle } from './colorStyle';
import { RGBAColor } from '../types/RGBAColor';
import { rgbaColorToPlainString } from './rgbaColorToPlainString';

export const ColorMenuItem = (color: RGBAColor) => {
    const colorString = rgbaColorToPlainString(color);
    return <MenuItem value={colorString} key={colorString} style={colorStyle(color)}>&nbsp;</MenuItem>;
};
