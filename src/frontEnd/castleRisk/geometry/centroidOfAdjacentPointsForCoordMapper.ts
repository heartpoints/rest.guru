import { numericSum } from '../../../utils/math/numericSum';
import { quasiCommuteAdjacentPoints } from "./quasiCommuteAdjacentPoints";
import { PointPair } from "./PointPair";
import { CentroidOfAdjacentPoints } from "./CentroidOfAdjacentPoints";
import { CoordToNumber } from "./CoordToNumber";

export const centroidOfAdjacentPointsForCoordMapper = 
    (coordToNumber: CoordToNumber): CentroidOfAdjacentPoints => 
    (adjacentPair: PointPair) => 
    quasiCommuteAdjacentPoints(adjacentPair) *
    numericSum(adjacentPair.map(coordToNumber))
