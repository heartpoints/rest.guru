import { throwError } from '../debugging/throwError';
export const maximum = (nums: number[]): number => nums.length > 0
    ? nums.sort().reverse()[0]
    : throwError(new Error("Cannot get maximum of empty list"));
