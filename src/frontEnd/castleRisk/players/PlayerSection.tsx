import * as React from 'react';
import { PlayerBox } from './PlayerBox';
import { Players } from '../types/Players';
import { Player } from '../types/Player';

export const PlayerSection = 
    (players: Players) => 
    (currentPlayer?: Player) =>
    <React.Fragment>
        {players.map(player => PlayerBox(player)(currentPlayer != undefined && currentPlayer.name == player.name))}
    </React.Fragment>
