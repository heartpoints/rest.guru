import { HasTerritory } from "./HasTerritory";
import { HasOwner } from "./HasOwner";
import { HasColor } from "./HasColor";
export type Piece = HasOwner & HasColor & Partial<HasTerritory>;
