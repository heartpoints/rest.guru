import { CastleRiskState } from "../types/CastleRiskState";
import { tommy } from "../players/tommy";
import { preexistingPlayers } from "../players/preexistingPlayers";
import { initialCastleRiskStateValue } from "./initialCastleRiskStateValue";
import { initialPhase } from "../config/initialPhase";
export const initialStateForCastlePlacementTesting: CastleRiskState = {
    ...initialCastleRiskStateValue,
    players: preexistingPlayers,
    phase: initialPhase,
    currentPlayer: tommy
};
