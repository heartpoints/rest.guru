import * as React from "react";
import { findVolunteeringOpportunities } from "../findVolunteeringOpportunities";
import { Page } from "../../page/Page"
import { Space } from "../../page/Space"
import { PageTitle } from "../../page/PageTitle";
import { AllOpportunitiesView } from '../../maps/AllOpportunitiesView';
import { VolunteeringCalendar } from "../../date/VolunteeringCalendar";
import { LocationPermissonDialog } from '../../modals/LocationPermissionDialog';
import { getZipFromCoords } from '../../maps/data/getZipFromCoords';
import { DateRangeFilter } from '../../date/DateRangeFilter';
import { HPNotFoundDisplay } from "./HPNotFoundDisplay";
import { DropdownTextArea } from '../../forms/DropdownTextArea';
import { sampleDropdownData } from '../data/sampleDropdownData';
import { DateFilter } from '../types/DateFilter';
import haversine from "haversine";
import { ListResult } from './ListResult';
import { Coord } from '../../maps/types/Coord';
import { ZipSearchField } from './ZipSearchField';

export const SearchBar = ({ userZipCoordinates, updateUserZipCoordinates, volSearchBarValue, onVolSearchBarValueChange, zipFilter, updateZipFilter, navTo, organizations, startDate, updateStartDate, endDate, updateEndDate }) => {

    const [ persmissionMessageOpen, updatePermissionMessageOpen ] = React.useState(false);
    const [ zipGeneratedFromLocation, toggleZipGeneratedFromLocation ] = React.useState(false);

    const showPosition = async (position) => {
        onZipTextChange(await getZipFromCoords(position.coords.latitude, position.coords.longitude));
        await toggleZipGeneratedFromLocation(true);
    }

    const userLocation = async () => 
        navigator.geolocation.getCurrentPosition(await showPosition, () => console.log("error"));

    const permissionGrantedHandler = () => {
        updatePermissionMessageOpen(false);
        userLocation();
    }

    const validateStartDate = (date) => {
        updateStartDate(date);
        endDate.isBefore(date) && updateEndDate(date);
    }

    const validateEndDate = (date) => {
        updateEndDate(date);
        startDate.isAfter(date) && updateStartDate(date);
    }

    const clearUserLocationZipHandler = () => {
        onZipTextChange('');
        toggleZipGeneratedFromLocation(false)
    }

    const dateFilter:DateFilter = {dateFilterStart: startDate, dateFilterEnd: endDate};

    const onZipTextChange = async (value:string) => {
        updateZipFilter(value)
        if(value.length == 5) {
            const response = await fetch(`/latLonForZip?zip=${value}`)
            const zipcodeLatLon = (await response.json()) as Coord

            updateUserZipCoordinates(zipcodeLatLon);
        }
    }

    const filteredOpportunities = findVolunteeringOpportunities(volSearchBarValue, dateFilter, organizations, userZipCoordinates, zipFilter);

    const listResults = () => {
        const results = filteredOpportunities.map(o => {
            const oppCoordinates:Coord = {latitude: o.coordinates.lat, longitude: o.coordinates.lng}
            const distance:number = Math.round(haversine(oppCoordinates, userZipCoordinates, {unit: 'mile'}) * 100) / 100;

            return <ListResult entity={o} distance={distance} navTo={navTo} /> });

        return <React.Fragment>
            {results.length > 0 ? results : <HPNotFoundDisplay message="No results match your search. Please refine your search criteria and try again!" />}
        </React.Fragment>
    }
    
    return <Page>
        <div style={{display: "flex", width: "100%"}}>
            <div style={{flex: "0 1 66%", marginRight: "25px"}}>
                <PageTitle>Volunteering Opportunity Search</PageTitle>
                <DropdownTextArea data={sampleDropdownData} searchBarValue={volSearchBarValue} onSearchBarValueChange={onVolSearchBarValueChange} popperTitle="Popular Searches"  />
                <ZipSearchField {...{zipGeneratedFromLocation, zipFilter, updatePermissionMessageOpen, onZipTextChange, clearUserLocationZipHandler}} />
                <Space />
                <DateRangeFilter
                    block
                    startDate={startDate}
                    onStartDateChange={date => validateStartDate(date)}
                    endDate={endDate}
                    onEndDateChange={date => validateEndDate(date)} />
                {listResults()}
            </div>
            <div style={{flex: "1 0 34%"}}>
                <AllOpportunitiesView {...{filteredOpportunities, navTo}} /><br />
                <VolunteeringCalendar {...{filteredOpportunities, startDate, endDate, navTo}}/>
            </div>
        </div>
        <LocationPermissonDialog isOpen={persmissionMessageOpen} permissionGrantedHandler={permissionGrantedHandler} toggleIsOpen={updatePermissionMessageOpen} />
    </Page>
}