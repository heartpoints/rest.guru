import * as React from "react";
import { Typography, Grid, Avatar } from "@material-ui/core";
import { isMobile } from "../site/isMobile";
import { HPMarkupView } from '../forms/HPMarkupView';

export const imageStyle = {
    "width": "50px",
    "height": "50px",
}
type HasImage = {image:string}
type HasTitle={title:string}
type HasDetails={details:string}
type HasChildren={children:React.ReactNode}
type HPSearchResultProps = HasImage & HasTitle & HasDetails & Partial<HasChildren>

export const HPSearchResult = ({image, title, details, children}:HPSearchResultProps) => {

    const { titleVariant, markupOverflowY } = isMobile()
        ? { titleVariant: "subtitle2" as "subtitle2", markupOverflowY: "auto"}
        : { titleVariant: "h5" as "h5", markupOverflowY: "scroll"}

    return <Grid container direction="row" justify="flex-start" alignItems="center" wrap="nowrap" spacing={2}>
        <Grid item>
            <Avatar src={image} style={imageStyle} />
        </Grid>
        <Grid item>
            <Typography variant={titleVariant} align="left"> {title}</Typography>
            {children}
            <HPMarkupView style={{height: "auto", overflowY: markupOverflowY, textAlign: "left"}} markup={details} />
        </Grid>
    </Grid>
}