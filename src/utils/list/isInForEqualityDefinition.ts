import { Predicate } from '../predicates/Predicate';
import { EqualityComparer } from "./EqualityComparer";
export const isInForEqualityDefinition = <T>(equals: EqualityComparer<T>) => (items: T[]): Predicate<T> => t => items.some(equals(t));
