export const everythingButIndex = (indexToOmit: number) => <T>(a: Array<T>): Array<T> => [...a.slice(0, indexToOmit), ...a.slice(indexToOmit + 1)];
