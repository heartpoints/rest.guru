import { throwError } from '../../../utils/debugging/throwError';

export const context2dForRef = 
    (ref: React.MutableRefObject<null>): CanvasRenderingContext2D => 
    ref.current != null
        ? (ref.current as any).getContext("2d")
        : throwError(new Error(`Could not get context for ref ${ref}`));
