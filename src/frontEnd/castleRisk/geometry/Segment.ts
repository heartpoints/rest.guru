import { PointPair } from "./PointPair";

export type Segment = PointPair;
