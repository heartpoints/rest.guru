import * as React from 'react';
import { isMobile } from '../site/isMobile';

export const ShiftBox = ({children}) => {
    const shiftBox:React.CSSProperties = isMobile()
        ? {display: "inline"}
        : {display: "flex", flexWrap: "wrap"}

    return <div style={shiftBox}>{children}</div>
}

export const TopOrLeft = ({children}) => {
    const topOrLeft:React.CSSProperties = isMobile()
        ? {display: "inline"}
        : {flex: "0 1 50%"}

    return <div style={topOrLeft}>{children}</div>
}

export const BottomOrRight = ({children}) => {
    const bottomOrRight:React.CSSProperties = isMobile()
        ? {display: "inline"}
        : {flex: 1}  

    return <div style={bottomOrRight}>{children}</div>
}