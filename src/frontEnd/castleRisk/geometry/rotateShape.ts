import { Shape } from '../types/Shape';
import { Radians } from './Radians';
import { rotatePoint } from './rotatePoint';
export const rotateShape = (angle: Radians) => (shape: Shape) => shape.map(rotatePoint(angle));
