import { stringFieldForCreate } from "./stringFieldForCreate";

export const stringFieldMatchingLabelAndPlaceholder = 
    (fieldNameLabelAndPlaceholder: string) => 
    stringFieldForCreate
        (fieldNameLabelAndPlaceholder)
        (fieldNameLabelAndPlaceholder);
