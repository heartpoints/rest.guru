import { Maybe } from "./MaybeType";
export type Maybes<T> = {
    [P in keyof T]: Maybe<T[P]>;
};
