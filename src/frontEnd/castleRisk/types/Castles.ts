import { Castle } from "./Castle";

export type Castles = Array<Castle>;
