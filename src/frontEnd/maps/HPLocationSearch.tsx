import React, { useState } from 'react';
import Geocode from 'react-geocode';
import { HPTextField } from '../forms/HPTextField';
import { HPButton } from '../forms/HPButton';
import { testFieldForMinimumCharacters } from '../forms/data/testFieldForMinimumCharacters';
import { testFieldForValidZip } from '../forms/data/testFieldForValidZip';
import { allFieldsValid } from '../forms/data/allFieldsValid';
import { usStates } from '../volunteering/data/usStates';
import { HPGenericDropdown } from '../forms/HPGenericDropdown';
import { testSelectForItemSelected } from '../forms/data/testSelectForItemSelected';
import { haveAtLeastOneElementInCommon } from '../../utils/arrays/haveAtLeastOneElementInCommon';
import { ErrorDialog } from '../modals/ErrorDialog';
import { defaultCoordinates } from '../forms/data/testCoordinatesNotDefault';
import { constructVerifiedAddressFromGoogle } from '../maps/data/constructVerifiedAddressFromGoogle';
import { isMobile } from '../site/isMobile';
import { Space } from '../page/Space';
export const HPLocationSearch = ({updateOppCoordinates, updateOppAddress, startingAddress}) => {

  Geocode.setApiKey("AIzaSyCCCW385gZ4e-6rSTXNVtcoklltFRat8ck")

  const [ address, updateAddress ] = useState(startingAddress);
  const [ shouldShowPopup, toggleShowPopup ] = useState(false);
  const [ popupText, updatePopupText ] = useState("");

  const { streetAddress, suite, city, state, zip } = address;
  const fullAddressString = `${streetAddress}, ${city}, ${state}, ${zip}`;

  const streetAddressError = testFieldForMinimumCharacters(streetAddress, 10);
  const cityError = testFieldForMinimumCharacters(city);
  const stateError = testSelectForItemSelected(state);
  const zipError = testFieldForValidZip(zip);
  const shouldEnableButton = allFieldsValid([streetAddressError, cityError, stateError, zipError]);

  const acceptableLocationTypes = ["premise", "establishment", "point_of_interest", "tourist_attraction", "store", "street_address", "subpremise"]

  const updateLocationToSearch = () => {
    Geocode.fromAddress(fullAddressString).then(
      response => {
        const results = response.results[0];
        
        haveAtLeastOneElementInCommon(acceptableLocationTypes, results.types)
          ? locationAcceptedHandler(results)
          : results.partial_match
            ? updateErrorDialogState(true, "One or more address field could not be found. Please verify the address information and try again.")
            : updateErrorDialogState(true, `Location is of type ${results.types.map(t => t)}, but must of type ${acceptableLocationTypes.join(', ')}`)
      },
      error => {
        updateErrorDialogState(true, "No Matches Found")
      }
    );
  }

  const locationAcceptedHandler = (results) => {
    const { lat, lng } = results.geometry.location
    const {streetAddress, city, state, zip} = constructVerifiedAddressFromGoogle(results.address_components);

    updateOppCoordinates({ lat, lng});
    updateOppAddress({streetAddress, suite: address.suite, city, state, zip});
  }

  const updateErrorDialogState = (shouldShow, text="") => {
    updateOppCoordinates(defaultCoordinates);
    toggleShowPopup(shouldShow);
    updatePopupText(text);
  }

  return <React.Fragment>
      <div style={{display: "flex"}}>
        <div style={{flex: 1}}>
          <HPTextField variant="standard" placeholder="Address" errorMessage={streetAddressError} value={address.streetAddress} onChange={e => updateAddress({...address, streetAddress: e.target.value})}/>
          <div>
            <HPTextField variant="standard" placeholder="Room/Apt" value={address.suite} onChange={e => updateAddress({...address, suite: e.target.value})} />
            <HPGenericDropdown label="State" startingValue={address.state} errorMessage={stateError} data={usStates} handleChange={s => updateAddress({...address, state: s})} />
          </div>
          
        </div>
        <div style={{flex: 1}}>
          <HPTextField variant="standard" placeholder="City" errorMessage={cityError} value ={address.city} onChange={e => updateAddress({...address, city: e.target.value})}/>
          <HPTextField variant="standard" placeholder="Zip" errorMessage={zipError} value={address.zip} onChange={e => updateAddress({...address, zip: e.target.value})}/>   
        </div> 
      </div>
      <HPButton shouldBeInline shouldUseThemeSpacing={!isMobile()} disabled={!shouldEnableButton} onClick={updateLocationToSearch} label="Validate Location" />
      { isMobile() && <Space />}
      <ErrorDialog titleText={popupText} isOpen={shouldShowPopup} dialogCloseHandler={() => updateErrorDialogState(false)} />
  </React.Fragment>
}