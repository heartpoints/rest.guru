import { haProxyServiceAccount } from "./haProxyServiceAccount"
import { interpolate } from "@pulumi/pulumi"
import * as k8s from "@pulumi/kubernetes"
import { haProxyNamespace } from "./haProxyNamespace"
import { ingressDefaultBackendService } from "./ingressDefaultBackendService"

const haProxyIngressDeploymentName = "haproxy-ingress";

export const haProxyIngressDeployment = new k8s.apps.v1.Deployment(haProxyIngressDeploymentName, {
  metadata: {
    labels: {
      run: haProxyIngressDeploymentName
    },
    name: haProxyIngressDeploymentName,
    namespace: haProxyNamespace.metadata.name
  },
  spec: {
    replicas: 1,
    selector: {
      matchLabels: {
        run: haProxyIngressDeploymentName
      }
    },
    template: {
      metadata: {
        labels: {
          run: haProxyIngressDeploymentName,
        }
      },
      spec: {
        serviceAccountName: haProxyServiceAccount.metadata.name,
        containers: [
          {
            name: haProxyIngressDeploymentName,
            image: "haproxytech/kubernetes-ingress:1.2.5",
            args: [
              interpolate`--default-ssl-certificate=${haProxyNamespace.metadata.name}/heartpoints-wildcard-tls-secrets`,
              interpolate`--configmap=${haProxyNamespace.metadata.name}/haproxy-configmap`,
              interpolate`--default-backend-service=${haProxyNamespace.metadata.name}/${ingressDefaultBackendService.metadata.name}`,
            ],
            resources: {
              requests: {
                cpu: "100m",
                memory: "80Mi",
              }
            },
            livenessProbe: {
              httpGet: {
                path: "/healthz",
                port: 1042
              }
            },
            ports: [
              {
                name: "http",
                containerPort: 80
              },
              {
                name: "https",
                containerPort: 443
              },
              {
                name: "stat",
                containerPort: 1024
              }
            ],
            env: [
              {
                name: "POD_NAME",
                valueFrom: {
                  fieldRef: {
                    fieldPath: "metadata.name"
                  }
                }
              },
              {
                name: "POD_NAMESPACE",
                valueFrom: {
                  fieldRef: {
                    fieldPath: "metadata.namespace"
                  }
                }
              }
            ]
          }
        ]
      }
    }
  }
})
