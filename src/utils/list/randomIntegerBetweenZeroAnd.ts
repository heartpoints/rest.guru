export const randomIntegerBetweenZeroAnd = 
    (maxExclusive: number) => 
    Math.floor(
        Math.random() * maxExclusive
    )
