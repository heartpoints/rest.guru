import { None } from "./None";
import { MaybeFlatmapper } from "./MaybeFlatmapper";
import { Maybe } from "./MaybeType";

export const firstLegitValue = 
<T, S>(inputVal: T, ...ops: Array<MaybeFlatmapper<T, S>>): Maybe<S> => {
    const opThatReturnedSomeValue = ops.find(op => op(inputVal).hasValue());
    return opThatReturnedSomeValue == undefined
        ? None
        : opThatReturnedSomeValue(inputVal);
};
