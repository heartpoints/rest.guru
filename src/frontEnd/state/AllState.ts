import { WithOrganizations } from '../organizations/data/WithOrganizations';
export type AllState = WithOrganizations;
