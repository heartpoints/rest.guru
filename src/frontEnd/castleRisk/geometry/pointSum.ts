import { originPoint } from './originPoint';
import { pointPlus } from './pointPlus';
import { summer } from '../../../utils/math/summer';

export const pointSum = summer(pointPlus)(originPoint);
