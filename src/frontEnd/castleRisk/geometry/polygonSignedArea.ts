import { Shape } from '../types/Shape'
import { numericSum } from '../../../utils/math/numericSum'
import { quasiCommuteAdjacentPoints } from './quasiCommuteAdjacentPoints'
import { cyclicPermutations } from '../../../utils/arrays/cyclicPermutations'

export const polygonSignedArea = 
    (points: Shape) => 
    0.5 * 
    numericSum(
        cyclicPermutations(points).map(quasiCommuteAdjacentPoints)
    )
