import { developers } from "./developers"

export const isDeveloper = 
    userCredentialsCookieString =>
        userCredentialsCookieString && developers.includes(userCredentialsCookieString.email)