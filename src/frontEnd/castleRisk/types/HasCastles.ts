import { Castles } from "./Castles";

export type HasCastles = {
    castles: Castles;
};
