import { stringFieldForEditOrg } from "./stringFieldForEditOrg";

//Hypothesis: orgPath at end simplifies this later
//todo: should we kill the placeholders (biz question)
export const stringFieldMatchingLabelAndPlaceholder = 
    (orgPath:string) =>
    (fieldNameLabelAndPlaceholder: string) => 
    stringFieldForEditOrg
        (orgPath)
        (fieldNameLabelAndPlaceholder)
        (fieldNameLabelAndPlaceholder);
