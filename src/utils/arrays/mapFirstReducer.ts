import { GroupOperator } from '../abstractAlgebra/GroupOperator';
import { Predicate } from '../predicates/Predicate';
import { MapFirstReduction } from './MapFirstReduction';
export const mapFirstReducer = <T>(predicate: Predicate<T>) => (operator: GroupOperator<T>) => ({found, result}: MapFirstReduction<T>, cur: T): MapFirstReduction<T> => found
    ? {found, result: [...result, cur] }
    : predicate(cur)
        ? {found: true, result: [...result, operator(cur)] }
        : {found: false, result: [...result, cur] };
