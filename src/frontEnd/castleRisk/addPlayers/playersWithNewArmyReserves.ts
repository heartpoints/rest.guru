import { throwError } from '../../../utils/debugging/throwError';
import { Players } from '../types/Players';
import { numArmies } from './numArmies';
import { isValidNumPlayers } from './isValidNumPlayers';
import { playerWithNewArmyReserves } from './playerWithNewArmyReserves';

export const playersWithNewArmyReserves = (players: Players): Players => isValidNumPlayers(players.length)
    ? players.map(playerWithNewArmyReserves(numArmies(players.length)))
    : throwError(new Error(`Invalid number of players`));
