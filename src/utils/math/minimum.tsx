import { throwError } from '../debugging/throwError';
export const minimum = (nums: number[]): number => nums.length > 0
    ? nums.sort()[0]
    : throwError(new Error("Cannot get minimum of empty list"));
