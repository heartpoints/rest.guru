import { CastleRiskState } from '../types/CastleRiskState'
import { activelyDrawingFillStyle } from '../config/activelyDrawingFillStyle'
import { activelyDrawingPointStyle } from '../config/activelyDrawingPointStyle'
import { drawPoint } from '../canvas/drawPoint'
import { drawShape } from '../canvas/drawShape'
import { overlappingPointsWithinAdminShapes } from './overlappingPointsWithinAdminShapes'
import { green } from '../colors/green';
import { multiplex } from '../../../utils/composition/multiplex'

export const drawAdminStuff = 
    (state: CastleRiskState) => 
    {
        const { admin, mousePoint } = state
        const { currentlyDrawingShape } = admin
        const overlappingPoints = overlappingPointsWithinAdminShapes(mousePoint)
        const drawAllGreenPoints = overlappingPoints.map(drawPoint(green))

        return multiplex([
            drawShape(currentlyDrawingShape)(activelyDrawingPointStyle)(activelyDrawingFillStyle),
            ...drawAllGreenPoints
        ])
    }
