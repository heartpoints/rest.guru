import { JSONObject } from '../../utils/json/JSONObject';
import { gitlabIssue } from './gitlabIssue';
import { gitCheckoutMasterPullLatestAndCreateBranch } from './gitCheckoutMasterPullLatestAndCreateBranch';
import { createMergeRequest } from "./createMergeRequest";
import { createNewBranch } from "./createNewBranch";

export const newBranchForIssue = async (issueNumber: number) => {
    const issue = await gitlabIssue(issueNumber) as JSONObject;
    const issueTitle = issue.title ? issue.title.toString() : undefined;
    if (issueTitle) {
        const formattedIssueTitle = issueTitle.replace(/\s+/g, '-').toLowerCase();
        const newBranchName = `${issueNumber}-${formattedIssueTitle}`;
        const branchCreated = await createNewBranch(newBranchName).catch(e => console.log(e));
        const mrCreated = await createMergeRequest(newBranchName).catch(e => console.log(e));
        console.log(branchCreated && mrCreated ? await gitCheckoutMasterPullLatestAndCreateBranch(newBranchName) : "Error creating new remote. Working branch not changed!");
    }
    else {
        console.log(`Issue #${issueNumber} Not Found!`);
    }
};
