import { Territories } from "./Territories";
export type HasTerritories = {
    territories: Territories;
};
