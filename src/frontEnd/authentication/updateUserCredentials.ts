import { User } from 'firebase';
import Cookies from 'js-cookie';
import { inDevMode } from '../developers/inDevMode';
import {navTo } from '../nav/navTo';
import { userCredentialsCookieKey } from './userCredentialsCookieKey';
import { userCredentialsCookieString } from '../authentication/userCredentialsCookieString';

export const updateUserCredentials = (state, userCredentials:User, logInButtonEnabled:boolean) => {
    console.log(userCredentials);
    Cookies.set(userCredentialsCookieKey, userCredentials);
    const postLoginState = {
        ...state,
        userCredentials: userCredentialsCookieString(),
        logInButtonEnabled,
        inDevMode: inDevMode()
    }

    return navTo(postLoginState, "/")
}
    