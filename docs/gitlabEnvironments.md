Gitlab Environments
===================

When a PR is created, a Gitlab CI/CD job will automatically create a [Gitlab environment](https://docs.gitlab.com/ee/ci/environments.html). The job uses our [Pulumi](./pulumi.md) code to deploy the application to our K8s cluster in its own namespace. This environment is also given its own URL so anyone can review the changes without having to pull the code and start the servers locally.

The Gitlab environment is easily accessible from the [Environments](https://gitlab.com/heartpoints/heartpoints.org/-/environments) page, or from the top of a PR.
