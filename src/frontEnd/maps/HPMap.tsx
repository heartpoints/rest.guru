import * as React from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";
import { compose, withProps, withStateHandlers } from "recompose";
import { defaultCoordinates } from '../forms/data/testCoordinatesNotDefault';

export const HPMap = compose(
    withProps({
      googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyCCCW385gZ4e-6rSTXNVtcoklltFRat8ck&v=3.exp&libraries=geometry,drawing,places",
      loadingElement: <div style={{ height: `100%` }} />,
      containerElement: <div style={{ height: `400px` }} />,
      mapElement: <div style={{ height: `100%` }} />,
    }),
    withScriptjs,
    withGoogleMap
  )((props) => {
    return <GoogleMap
      zoom={props.searchCoordinates != defaultCoordinates ? 16 : 4}
      center={props.searchCoordinates ? props.searchCoordinates : defaultCoordinates}>
      {props.searchCoordinates != defaultCoordinates && <Marker position={props.searchCoordinates} />}
    </GoogleMap>
  })