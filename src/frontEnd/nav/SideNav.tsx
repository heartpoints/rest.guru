import * as React from "react";
import { Drawer, IconButton, Divider, List, ListItemIcon, ListItemText, withStyles, ListItem, AppBar, Collapse } from "@material-ui/core";
import InboxIcon from '@material-ui/icons/MoveToInbox';
import Language from '@material-ui/icons/Language';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import DonutSmallIcon from '@material-ui/icons/DonutSmall';
import AddCircleOutline from '@material-ui/icons/AddCircleOutline';
import Search from '@material-ui/icons/Search';
import PanTool from '@material-ui/icons/PanTool';
import Favorite from '@material-ui/icons/Favorite';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ListIcon from '@material-ui/icons/List';
import GamesIcon from '@material-ui/icons/Games'
import { isMobile } from '../site/isMobile';

export const SideNavUnstyled = ({isSideNavOpen, isSideNavOrgSectionExpanded, toggleSideNavOrgSection, classes, navigateAndAutoCollapseIfMobile, toggleSideNavVolunteeringSection, isSideNavVolunteeringSectionExpanded, userCredentials}) => {

    return <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="left"
          open={isSideNavOpen}
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <div className={classes.drawerHeader}>
            <IconButton onClick={() => navigateAndAutoCollapseIfMobile()}>
              <ChevronLeftIcon />
            </IconButton>
          </div>
          <Divider />
          <List>
            <ListItem button onClick={() => navigateAndAutoCollapseIfMobile("/") }>
              <ListItemIcon><InboxIcon /></ListItemIcon>
              <ListItemText primary="Home" />
            </ListItem>
            <ListItem button onClick={toggleSideNavOrgSection}>
              <ListItemIcon><Language /></ListItemIcon>
              <ListItemText primary="Organizations" />
              {isSideNavOrgSectionExpanded ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={isSideNavOrgSectionExpanded} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
             <ListItem button className={classes.nested} onClick={() => navigateAndAutoCollapseIfMobile("/organizations/new")}>
              <ListItemIcon>
                <AddCircleOutline />
              </ListItemIcon>
              <ListItemText primary="Create New" />
             </ListItem>
             <ListItem button className={classes.nested} onClick={() => navigateAndAutoCollapseIfMobile("/organizations/search")}>
              <ListItemIcon>
                <Search />
              </ListItemIcon>
              <ListItemText primary="Search" />
             </ListItem>
             <ListItem button className={classes.nested} onClick={() => navigateAndAutoCollapseIfMobile("/organizations/viewAll")}>
              <ListItemIcon>
                <ListIcon />
              </ListItemIcon>
              <ListItemText primary="View All" />
             </ListItem>
            </List>
            </Collapse>
            <ListItem button onClick={toggleSideNavVolunteeringSection}>
              <ListItemIcon><PanTool /></ListItemIcon>
              <ListItemText primary="Volunteering" />
              {isSideNavVolunteeringSectionExpanded ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={isSideNavVolunteeringSectionExpanded} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
             <ListItem button className={classes.nested} onClick={() => navigateAndAutoCollapseIfMobile("/volunteering/new")}>
              <ListItemIcon>
                <AddCircleOutline />
              </ListItemIcon>
              <ListItemText primary="Create New" />
             </ListItem>
             <ListItem button className={classes.nested} onClick={() => navigateAndAutoCollapseIfMobile("/volunteering/search")}>
              <ListItemIcon>
                <Search />
              </ListItemIcon>
              <ListItemText primary="Search" />
             </ListItem>
            </List>
            </Collapse>
            <ListItem button onClick={() => navigateAndAutoCollapseIfMobile(`/user/${userCredentials.email}`) }>
              <ListItemIcon><Favorite /></ListItemIcon>
              <ListItemText primary="My Profile" />
            </ListItem>
            <ListItem button onClick={() => navigateAndAutoCollapseIfMobile("/castleRisk") }>
              <ListItemIcon><DonutSmallIcon /></ListItemIcon>
              <ListItemText primary="Castle Risk" />
            </ListItem>
            <ListItem button onClick={() => navigateAndAutoCollapseIfMobile("/rest-guru") }>
              <ListItemIcon><DonutSmallIcon /></ListItemIcon>
              <ListItemText primary="rest.guru" />
            </ListItem>
            <ListItem button onClick={() => navigateAndAutoCollapseIfMobile("/animationExample") }>
              <ListItemIcon><GamesIcon /></ListItemIcon>
              <ListItemText primary="GSAP Animation" />
            </ListItem>
          </List>
    </Drawer>
}

const drawerWidth = () => isMobile()
  ? window.innerWidth
  : 240;

const styles = theme => ({
  drawer: {
    display: 'flex',
    width: drawerWidth as any,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth as any,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
});

export const SideNav = withStyles(styles, { withTheme: true })(SideNavUnstyled);