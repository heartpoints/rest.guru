import * as React from 'react'
import { HPButton } from '../../forms/HPButton'
import { HPTextField } from '../../forms/HPTextField'
import { ColorPicker } from '../colors/ColorPicker'
import { allowedToAdd } from './allowedToAdd'
import { enterKeyCode } from '../../../utils/strings/enterKeyCode'
import { Player } from '../types/Player'
import { UpdateStateOf } from '../../state/UpdateStateOf'
import { CastleRiskState } from '../types/CastleRiskState'
import { Players } from '../types/Players'
import { Colors } from '../colors/Colors';

export const newPlayerForm = 
    (player: Player) => 
    (players: Players) => 
    (remainingColors: Colors) => 
    (updateState: UpdateStateOf<CastleRiskState>) => 
    {
        const onKeyUp = (e: React.KeyboardEvent<HTMLInputElement>) => e.keyCode == enterKeyCode && allowedToAdd(players)(player) && onButtonClick()
        const onButtonClick = () => {
            updateState({
                players: [...players, player],
                newPlayer: undefined
            })
        }
        const updatePlayerName = (name) => updateState({ newPlayer: { ...player, name } })
        const updateColor = (color) => updateState({ newPlayer: { ...player, color } })
        return <div style={{ width: "100%" }}>
            <HPTextField style={{ width: "100%" }} label="Player Name" type="text" value={player.name} onChange={e => updatePlayerName(e.target.value)} {...{ onKeyUp }} />
            <br />
            <ColorPicker colors={remainingColors} onColorChange={updateColor} value={player.color} />
            <br />
            <HPButton label="Add player" onClick={onButtonClick} disabled={!allowedToAdd(players)(player)} />
        </div>
    }
