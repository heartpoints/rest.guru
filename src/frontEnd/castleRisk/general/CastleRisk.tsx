import * as React from 'react';
import { Page } from '../../page/Page';
import { PageTitle } from '../../page/PageTitle';
import { Space } from '../../page/Space';
import { componentForPhase } from './componentForPhase';
import { CastleRiskPhaseProps } from '../types/AddPlayerProps';
import { Phase } from '../types/Phase';
import { UpdateStateOf } from '../../state/UpdateStateOf';
import { CastleRiskState } from '../types/CastleRiskState';

const phaseChangeLInk =
    (updateState:UpdateStateOf<CastleRiskState>) =>
    (label:string) =>
    (phase:Phase) =>
    <a style={{marginRight: "1em", cursor: "pointer", color: "blue"}} onClick={() => updateState({phase})}>{label}</a>

export const CastleRisk = (props:CastleRiskPhaseProps) => {
    const { phase, updateState } = props
    const PhaseSpecificComponent = componentForPhase(phase)
    const phaseChangeLinkM = phaseChangeLInk(updateState)
    return <Page>
        <PageTitle>Castle Risk</PageTitle>
        {phaseChangeLinkM("Add Players")(Phase.AddPlayers)}
        {phaseChangeLinkM("Place Castles")(Phase.PlaceCastles)}
        {phaseChangeLinkM("Admin")(Phase.AdminBorders)}
        <Space />
        <PhaseSpecificComponent {...props} />
    </Page>
}