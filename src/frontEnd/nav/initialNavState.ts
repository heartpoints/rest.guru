import { urlFromString } from "../../utils/url/urlFromString";
import { isMobile } from "../site/isMobile";

export const initialNavState = () => ({
    isSideNavOrgSectionExpanded: false,
    isSideNavVolunteeringSectionExpanded: false,
    isSideNavOpen: false,
    url: urlFromString(window.location.href),
})