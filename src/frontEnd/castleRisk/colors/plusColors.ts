import { RGBAColor } from '../types/RGBAColor';
import { arraySum } from '../../../utils/arrays/arraySum';
export const plusColors = (c: RGBAColor) => (c2: RGBAColor) => arraySum(c)(c2) as RGBAColor;
