import { MaybeFlatmapper } from "./MaybeFlatmapper";
import { Some } from "./Some";
import { Maybe } from "./MaybeType";

export const reduceMaybe = 
    <T>(inputVal: T, ...ops: Array<MaybeFlatmapper<T, T>>): Maybe<T> => 
    ops.reduce(
        (acc, current) => acc.flatMap(current), 
        Some(inputVal) as Maybe<T>
    )
