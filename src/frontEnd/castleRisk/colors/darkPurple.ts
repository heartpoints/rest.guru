import { RGBAColor } from "../types/RGBAColor";
import { darkenColor } from "./darkenColor";
import { purple } from "./purple";
export const darkPurple: RGBAColor = darkenColor(0.2)(purple);
