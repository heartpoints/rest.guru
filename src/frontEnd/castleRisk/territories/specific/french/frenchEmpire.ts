import { netherlands } from "./netherlands";
import { Empire } from "../../../types/Empire";
import { pink } from "../../../colors/pink";
import { darkenColor } from "../../../colors/darkenColor";
import { territory } from "../../general/territory";
import { Bugundy } from "./Bugundy";
import { Paris } from "./Paris";
import { Marseille } from "./Marseille";
import { Gascony } from "./Gascony";
import { Brittany } from "./Brittany";

export const frenchEmpire:Empire = {
    color: darkenColor(0.3)(pink),
    name: "French",
    get territories() { 
        const frenchTerritory = territory(() => frenchEmpire)
        return [
            frenchTerritory("Netherlands")(netherlands),
            frenchTerritory("Bugundy")(Bugundy),
            frenchTerritory("Paris")(Paris),
            frenchTerritory("Marseille")(Marseille),
            frenchTerritory("Gascony")(Gascony),
            frenchTerritory("Brittany")(Brittany),
        ]
    }
}