import { lightenColor } from './lightenColor';
import { blueGreen } from './blueGreen';

export const seaColor = lightenColor(0.7)(blueGreen)
