import { Empires } from "./Empires";
export type HasEmpires = {
    empires: Empires;
};
