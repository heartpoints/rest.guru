import { drawPolygon } from '../../canvas/drawPolygon';
import { black } from '../../colors/black';
export const drawTerritoryWithoutDots = drawPolygon(false)(black);
