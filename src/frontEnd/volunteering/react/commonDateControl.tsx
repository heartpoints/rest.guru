import React from 'react';
import { VolunteeringDateRangeSelector } from '../../date/VolunteeringDateRangeSelector';
import { testDateRangeForValidity } from '../../forms/data/testDateRangeForValidity';
import moment from 'moment';
import { UseState } from "../types/UseState";

export const commonDateControl = (useState: UseState, originalStartDate:moment.Moment = moment(), originalEndDate:moment.Moment = originalStartDate) => {
    const [startDate, updateStartDate] = useState(originalStartDate);
    const [endDate, updateEndDate] = useState(originalEndDate);
    const onStartDateChange = (date:moment.Moment) => {
        updateStartDate(date);
        endDate.isBefore(date, "date") && updateEndDate(date)
    };
    const onEndDateChange = (date) => updateEndDate(date);
    const dateRangeError = testDateRangeForValidity(startDate, endDate);
    const uiComponent = <VolunteeringDateRangeSelector {...{onStartDateChange, onEndDateChange, startDate, endDate }} errorText={dateRangeError} />;
    return {
        uiComponent,
        dateRangeError,
        startDate,
        endDate
    };
};
