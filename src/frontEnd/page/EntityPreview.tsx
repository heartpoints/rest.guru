import * as React from 'react';
import { HPSearchResult } from '../search/HPSearchResult';
import { previewContainerStyle } from '../forms/previewContainerStyle';

type HasTitle = { title: string }
type HasImage = { image: string }
type HasDetails = { details: string }
type HasClickHandler = { clickHandler: ()=> void }
type EntityPreviewProps = HasTitle & HasImage & HasDetails & HasClickHandler

export const EntityPreview = ({ image, title, details, clickHandler } :EntityPreviewProps) => <div style={previewContainerStyle} onClick={clickHandler}>
        <HPSearchResult {...{image, title, details}} />
    </div>
