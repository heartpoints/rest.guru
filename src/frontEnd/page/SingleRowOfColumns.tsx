import { Rows } from './Rows';
import { Columns } from './Columns';

export const SingleRowOfColumns = 
    (...children: JSX.Element[]) => 
    Rows(Columns(...children));
