export const range = 
    (n: number): number[] => 
    n == 0 
        ? [] 
        : [n - 1, ...range(n - 1)];
