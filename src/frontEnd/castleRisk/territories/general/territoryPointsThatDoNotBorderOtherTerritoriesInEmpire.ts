import { Territory } from '../../types/Territory';
import { Points } from '../../types/Points';
import { Empire } from '../../types/Empire';
import { Point } from '../../types/Point';
import { List } from '../../../../utils/list/List';
import { otherEmpireTerritories } from './otherEmpireTerritories';
import { pointsEqual } from '../../geometry/pointsEqual';
export const territoryPointsThatDoNotBorderOtherTerritoriesInEmpire = (empire: Empire) => (territory: Territory): Points => {
    const otherTerritories = otherEmpireTerritories(empire)(territory);
    const otherPoints = List(otherTerritories.map(t => t.boundary)).flatMap(List).asArray;
    const pointDoesntOverlap = (p: Point) => !otherPoints.some(p2 => pointsEqual(p)(p2));
    return territory.boundary.filter(pointDoesntOverlap);
};
