import { Point } from '../types/Point';

export const translatePointY = 
    (yDiff: number) => 
    ({ x, y }: Point) => 
    ({
        x,
        y: y + yDiff
    });
