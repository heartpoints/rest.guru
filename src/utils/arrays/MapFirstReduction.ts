export type MapFirstReduction<T> = {
    found: boolean;
    result: Array<T>;
};
