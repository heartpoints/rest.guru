import { haProxyNamespace } from "./haProxyNamespace"
import * as k8s from "@pulumi/kubernetes"

export const haProxyConfigMap = new k8s.core.v1.ConfigMap("haproxy-configmap", {
    metadata: {
        name: "haproxy-configmap",
        namespace: haProxyNamespace.metadata.name,
    },
    data: {}
})
