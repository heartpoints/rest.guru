import { Consumer } from "../axioms/Consumer";
import { callEachWith } from "./callEachWith";

export type Consumers<T> = Array<Consumer<T>>
export type Multiplex = <T>(funcs:Consumers<T>) => Consumer<T>
export const multiplex:Multiplex = (funcs) => t => callEachWith(funcs, t);
