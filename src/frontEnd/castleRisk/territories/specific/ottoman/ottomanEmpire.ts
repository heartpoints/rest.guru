import { rumania } from "./rumania";
import { Serbia } from "./Serbia";
import { bulgaria } from "./bulgaria";
import { Empire } from "../../../types/Empire";
import { darkenColor } from "../../../colors/darkenColor";
import { territory } from "../../general/territory";
import { montenegro } from "./montenegro";
import { turkey } from "./turkey";
import { cypress } from "./cypress";

export const ottomanEmpire:Empire = {
    color: darkenColor(0.3)([130,255,210,255]),
    name: "Ottoman",
    get territories() {
        const ottomanTerritory = territory(() => ottomanEmpire)
        return [
            ottomanTerritory("Rumania")(rumania),
            ottomanTerritory("Bulgaria")(bulgaria),
            ottomanTerritory("Serbia")(Serbia),
            ottomanTerritory("Montenegro")(montenegro),
            ottomanTerritory("Turkey")(turkey),
            ottomanTerritory("Cypress")(cypress),
        ]
    }
}