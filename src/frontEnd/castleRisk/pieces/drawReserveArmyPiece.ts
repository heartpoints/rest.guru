import { drawPolygon } from '../canvas/drawPolygon';
import { black } from '../colors/black';
import { translatePointX } from '../geometry/translatePointX';
import { translateShape } from '../geometry/translateShape';
import { Point } from '../types/Point';
import { RGBAColor } from '../types/RGBAColor';
import { Shape } from '../types/Shape';

export const drawReserveArmyPiece = 
    (pieceShape:Shape) =>
    (color: RGBAColor) =>
    (origin: Point) => 
    {
        const piecePadding = 8;
        const drawArmyPiece = drawPolygon(true)(black)(color);
        return drawArmyPiece
            (translateShape
                (translatePointX(piecePadding)(origin))
                (pieceShape)
            )
    };
