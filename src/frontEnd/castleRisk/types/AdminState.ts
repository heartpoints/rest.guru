import { Shape } from "./Shape";

export type AdminState = {
    currentlyDrawingShape: Shape
};
