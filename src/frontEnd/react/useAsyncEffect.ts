import { Provider } from '../../utils/axioms/Provider';
import React from "react"

export const useAsyncEffect = (f: Provider<Promise<any>>) => React.useEffect(() => { f(); });
