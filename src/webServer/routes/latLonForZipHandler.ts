import { Request, Response } from "express";
import { lookup } from "zipcodes";

export const latLonForZipHandler = (request: Request, response: Response) => {
    const queryZip = request.query.zip;
    const location = lookup(parseInt(queryZip));
    response.send(JSON.stringify(location));
};
