export type Scrambles = <T>(a: Array<T>) => Array<T>;
