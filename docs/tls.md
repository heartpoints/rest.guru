TLS
===

Transport Layer Security (TLS) certificates for *.heartpoints.org and heartpoints.org was created from the following interactive command (from an Ubuntu image):

```
apt-get install letsencrypt
certbot certonly --manual --preferred-challenges=dns --email info@heartpoints.org --server https://acme-v02.api.letsencrypt.org/directory --agree-tos -d '*.heartpoints.org' -d heartpoints.org
```

These certs are not autorenewed so they must be kept up-to-date.

Insecure HTTP requests are passed thru to the express web server, which forces redirect to the
equivalent HTTPS address if Request Header `x-forwarded-proto` has value `http`, which indicates
presence of a load-balanced, insecure request.