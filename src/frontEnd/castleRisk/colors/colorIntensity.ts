import { RGBAColor } from '../types/RGBAColor';
import { averageNumber } from '../../../utils/math/averageNumber';
export const colorIntensity = ([r, g, b]: RGBAColor): number => averageNumber([r, g, b]);
