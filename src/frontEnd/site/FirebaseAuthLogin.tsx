import * as React from 'react';
import firebase from 'firebase';
import { firebaseConfig } from './firebaseConfig';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import { LoginButton } from '../authentication/LoginButton';
import ClearIconRounded from '@material-ui/icons/ClearOutlined';
import {UserLoggedInButton} from '../authentication/UserLoggedInButton';

export const FirebaseAuthLogin = (props) => {
  const {userCredentials, updateUserCredentials, logInButtonEnabled, toggleLogInButtonEnabled, onFirebaseLoginComplete} = props;
  
  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
  }

  const uiConfig = {
    signInFlow: 'popup',
    signInSuccessUrl: '/',
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      firebase.auth.FacebookAuthProvider.PROVIDER_ID,
      //firebase.auth.GithubAuthProvider.PROVIDER_ID
    ],
    callbacks: {
      signInSuccessWithAuthResult: (authResult, redirectUrl) => {
        onFirebaseLoginComplete(authResult.getUser());
        return false;
      }
    }
  };

  const logInWithFirebase = () => {
    firebase.auth().onAuthStateChanged(
      (user) => {
        if(user){
          updateUserCredentials(user, false);
        } else {
          toggleLogInButtonEnabled(true);
        }
      }
    );
  }

  const FireBaseLogin = () => {
    return <div>
      <ClearIconRounded style={{color: "white"}} onClick={() => toggleLogInButtonEnabled(false)} />
      <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={firebase.auth()} />
    </div>
  }

  return userCredentials.email != ""
      ? <UserLoggedInButton {...props}/>
      : logInButtonEnabled
        ? <FireBaseLogin />
        : <LoginButton clickHandler={logInWithFirebase} />
}