import moment, { Moment } from 'moment';

type Direction = "up" | "down";

export const roundDate = (date:Moment, direction:Direction) => {
    return direction == "up"
        ? moment(date).endOf('day')
        : moment(date).startOf('day')
}