import { Point } from '../types/Point'
import { Radians } from './Radians'
import { lengthFromOrigin } from './lengthFromOrigin'
import { angleForPointOnUnitCircle } from './angleForPointOnUnitCircle'
import { normalizedPoint } from './normalizedPoint'

export const rotatePoint = 
    (angle: Radians) => 
    (pointToRotate: Point) => {
        const h = lengthFromOrigin(pointToRotate)
        const startAngle = angleForPointOnUnitCircle(normalizedPoint(pointToRotate))
        const newAngle = startAngle + angle
        return {
            x: Math.cos(newAngle) * h,
            y: Math.sin(newAngle) * h,
        }
    }
