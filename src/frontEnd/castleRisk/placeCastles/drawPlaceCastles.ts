import { multiplex } from '../../../utils/composition/multiplex';
import { clearCanvas } from '../canvas/clearCanvas';
import { drawPolygon } from '../canvas/drawPolygon';
import { seaColor } from '../colors/seaColor';
import { rectangle } from '../geometry/rectangle';
import { drawReserveArmies } from '../pieces/drawReserveArmies';
import { darkName } from '../territories/general/darkName';
import { drawDarkTerritory } from '../territories/general/drawDarkTerritory';
import { drawNormalTerritory } from '../territories/general/drawNormalTerritory';
import { normalName } from '../territories/general/normalName';
import { outsideTerritories } from '../territories/general/outsideTerritories';
import { Player } from '../types/Player';
import { Players } from '../types/Players';
import { Point } from '../types/Point';
import { drawPlacedArmyForPlayer } from '../pieces/drawPlacedArmyForPlayer';
import { possibleHighlight } from './possibleHighlight';

export const drawPlaceCastles =
    (players:Players) =>
    (currentPlayer:Player) =>
    (mousePoint: Point) => 
    {
        const { castles: [{empire}] } = currentPlayer
        const { territories } = empire
        const nonEmpireTerritories = outsideTerritories(empire)
        return multiplex([
            clearCanvas,
            drawPolygon(false)(seaColor)(seaColor)(rectangle(1000)(1000)),
            ...nonEmpireTerritories.map(drawDarkTerritory),
            ...territories.map(drawNormalTerritory),
            possibleHighlight(mousePoint)(empire),
            ...players.map(drawPlacedArmyForPlayer),
            ...territories.map(normalName),
            ...nonEmpireTerritories.map(darkName),
            // drawShape(empireBorderPoints(empire))(white)(clear),
            drawReserveArmies(players)
        ])
    }
