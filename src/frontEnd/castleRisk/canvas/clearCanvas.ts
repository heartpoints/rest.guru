import { canvasWidth } from '../config/canvasWidth';
import { canvasHeight } from '../config/canvasHeight';

export const clearCanvas = 
    (context: CanvasRenderingContext2D) => 
    context.clearRect(0, 0, canvasWidth, canvasHeight);
