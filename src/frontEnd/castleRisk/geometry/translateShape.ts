import { Shape } from '../types/Shape';
import { Point } from '../types/Point';
import { translatePoint } from './translatePoint';

export const translateShape = 
    (displacement: Point) => 
    (shape: Shape) => 
    shape.map(translatePoint(displacement));
