import { canvasHeight } from '../config/canvasHeight';
import { playerBoxWidth } from './playerBoxWidth';
export const playerBoxSize = { x: playerBoxWidth, y: canvasHeight };
