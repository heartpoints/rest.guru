import { Points } from '../../types/Points';
import { Empire } from '../../types/Empire';
import { territoryPointsThatDoNotBorderOtherTerritoriesInEmpire } from './territoryPointsThatDoNotBorderOtherTerritoriesInEmpire';

//todo: these are not in the right order, need to do adjacent territories
//and ensure each adjacent one is hit only once    
export const empireBorderPoints = (empire: Empire) => empire.territories.reduce((soFar, territory) => [
    ...soFar,
    ...territoryPointsThatDoNotBorderOtherTerritoriesInEmpire(empire)(territory)
], [] as Points);
