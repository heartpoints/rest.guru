import * as React from 'react';
import { HPSearchFilter } from '../search/HPSearchFilter';
import { TextField } from '@material-ui/core';
import { testFieldForValidZip } from '../forms/data/testFieldForValidZip';
import { HPRadioGroup } from '../forms/HPRadioGroup';

export const VolOpportunitiesZipFilter = ({zipFilter, zipFilterUpdateHandler, updateZipFilterEnabled, selectedZipRadius, updateZipRadius}) => {
    const radiiOptions = ["10", "50", "100", "500"];

    const errorText = testFieldForValidZip(zipFilter);

    return <HPSearchFilter onFilterClear={updateZipFilterEnabled} filterName="Zip">
        <TextField error={errorText != undefined} helperText={errorText} label="Filter By ZIP code" value={zipFilter} onChange={e => zipFilterUpdateHandler(e.target.value)} />
        <HPRadioGroup groupName="Radius in Miles" data={radiiOptions} selectedItem={selectedZipRadius} updateItemValue={(updateZipRadius)} />
    </HPSearchFilter>
}