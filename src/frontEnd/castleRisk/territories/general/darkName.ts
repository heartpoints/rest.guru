import { gray } from '../../colors/gray';
import { drawTerritoryName } from './drawTerritoryName';
export const darkName = drawTerritoryName(gray);
