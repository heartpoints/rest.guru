import { Integer } from '../../../utils/math/Integer'
import { regularAngles } from './regularAngles'

export const regularPolygon = 
    (numSides: Integer) => 
    (radius: number) => 
    regularAngles(numSides).map(
        angle => ({ 
            x: Math.cos(angle) * radius,
            y: Math.sin(angle) * radius, 
        })
    )
