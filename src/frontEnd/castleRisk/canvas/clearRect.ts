import { translatePoint } from '../geometry/translatePoint';
import { Point } from '../types/Point';

export const clearRect = 
    (upperLeft: Point) => 
    (size: Point) => 
    (context) => {
        const { x, y } = upperLeft;
        const { x: x2, y: y2 } = translatePoint(size)(upperLeft);
        context.clearRect(x, y, x2, y2);
    };
