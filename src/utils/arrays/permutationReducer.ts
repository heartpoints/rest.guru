import { LikePairs } from './LikePairs'
import { PermutationReducer } from './PermutationReducerOfT'

export const permutationReducer: PermutationReducer =
    <T>({permutations, toBePermuted }, cur) => 
    ({
        permutations: [
            ...permutations,
            ...(
                toBePermuted != undefined 
                    ? [[toBePermuted, cur]] 
                    : []
            ) as LikePairs<T>
        ],
        toBePermuted: cur
    })
