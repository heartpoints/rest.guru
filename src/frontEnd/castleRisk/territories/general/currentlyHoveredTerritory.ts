import { pointInShape } from '../../geometry/pointInShape';
import { listOfTerritories } from '../general/listOfTerritories';
import { Point } from '../../types/Point';

export const currentlyHoveredTerritory = 
    (mousePoint: Point) => 
    listOfTerritories.first(t => pointInShape(mousePoint)(t.boundary));
