import { MutableStateType } from "./MutableStateType";

export const createMutableState = 
    <S>(initialState:S):MutableStateType<S> => {
        let currentMutableState = initialState;
        return {
            read: () => currentMutableState,
            write: (newState) => currentMutableState = newState
        }
    }