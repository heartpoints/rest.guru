import * as _ from 'lodash';

export const defaultCoordinates = { lat: 38.30574, lng: -99.356136 };
export const testCoordinatesNotDefault = (coordinates) => {

    return _.isEqual(coordinates, defaultCoordinates) 
        ? "Please Enter a Valid Location"
        : undefined
}