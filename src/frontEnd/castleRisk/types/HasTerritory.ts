import { Territory } from "./Territory";
export type HasTerritory = {
    territory: Territory;
};
