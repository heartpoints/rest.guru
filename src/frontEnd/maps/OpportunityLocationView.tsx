import * as React from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow } from "react-google-maps";
import { compose, withProps, withStateHandlers } from "recompose";
import { Typography } from '@material-ui/core';

export const OpportunityLocationView = compose(
    withStateHandlers(() => ({
        isMarkerShown: false,
        markerPosition: null,
        zoomLevel: 10,
        shouldShowInfo: false
      }), {
        onMarkerClick: ({shouldShowInfo}) => () => ({
          shouldShowInfo: !shouldShowInfo
        })
      }),
    withProps({
      googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyCCCW385gZ4e-6rSTXNVtcoklltFRat8ck&v=3.exp&libraries=geometry,drawing,places",
      loadingElement: <div style={{ height: `100%` }} />,
      containerElement: <div style={{ height: `400px` }} />,
      mapElement: <div style={{ height: `100%` }} />,
    }),
    withScriptjs,
    withGoogleMap
  )((props) => {
    const { streetAddress, suite, city, state, zip } = props.address;
    return <GoogleMap
      zoom={props.zoomLevel}
      center={props.coordinates}>
      <Marker onClick={props.onMarkerClick} position={props.coordinates}>
        { props.shouldShowInfo && <InfoWindow>
          <Typography variant="caption">
            {`${streetAddress} ${suite}`}<br />
            {`${city}, ${state}, ${zip}`}
          </Typography>
        </InfoWindow>}
      </Marker>
    </GoogleMap>
  })