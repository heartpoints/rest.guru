import { Point } from "../types/Point";
import { drawShape } from "./drawShape";
import { rectangle } from "../geometry/rectangle";
import { translateShape } from "../geometry/translateShape";

export const drawRectangle =
    (origin:Point) =>
    ({x, y}:Point) =>
    drawShape(translateShape(origin)(rectangle(x)(y)))