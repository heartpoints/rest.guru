import * as React from 'react';
import FavoriteIconTwoTone from '@material-ui/icons/FavoriteBorderTwoTone';
import { Typography } from '@material-ui/core';
import { isMobile } from '../../site/isMobile';
export const containerStyle = {
    "display": "inline-block",
    "backgroundImage": "linear-gradient(#cd0000, #A70101)",
    "borderRadius": "10px",
    "border": "thin solid black",
    "padding": "5px 20px",
    "verticalAlign": "middle",
    "color": "white",
    "boxShadow": "2px 2px 3px darkgrey"
}

export const VolHeartpointsDisplay = ({heartpointsToDisplay}) => {
    return <div style={containerStyle}>
        <Typography variant="button">{!isMobile() && "Worth"}<FavoriteIconTwoTone />{heartpointsToDisplay}</Typography>
    </div>
}