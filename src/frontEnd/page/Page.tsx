import * as React from "react";
import { Paper } from "@material-ui/core";
import { PageTitle } from "./PageTitle";
import { User } from 'firebase';

type WithChildren = { children: React.ReactNode}
type RequiresLoggedIn = {requiresLoggedIn: boolean}
type WithUserCredentials = {userCredentials: User | {email: ""}}
type PageProps = WithChildren & Partial<RequiresLoggedIn> & Partial<WithUserCredentials>
 
export const Page = ({children, requiresLoggedIn = false, userCredentials = {email: ""} }:PageProps) => {

    const shouldDisplayContent = !requiresLoggedIn || userCredentials.email != "";

    const promptToLogIn = () => <React.Fragment>
            <PageTitle>Please Log In!</PageTitle>
        </React.Fragment>

    const content = () => children

    return <div style={{marginTop: "60px"}}>
        <Paper style={{padding: "15px"}}>
            {shouldDisplayContent ? content() : promptToLogIn()}
        </Paper>
    </div>
}
