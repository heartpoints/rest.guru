import { red } from './red';
import { black } from './black';
import { yellow } from './yellow';
import { green } from './green';
import { darkPurple } from "./darkPurple";
import { blue } from './blue';
export const pieceColors = [
    red,
    green,
    yellow,
    black,
    darkPurple,
    blue
];
