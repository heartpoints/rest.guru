import { numericPlus } from './numericPlus';
import { summer } from './summer';

export const numericSum = summer(numericPlus)(0);
