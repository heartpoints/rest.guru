import { Piece } from '../types/Piece';
import { Player } from '../types/Player';
export const newArmyReservePiece = (owner: Player): Piece => ({
    color: owner.color,
    owner,
    territory: undefined
});
