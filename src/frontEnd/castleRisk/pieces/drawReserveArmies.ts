import { multiplex } from '../../../utils/composition/multiplex';
import { clearRect } from '../canvas/clearRect';
import { translatePointY } from '../geometry/translatePointY';
import { paddingBetweenPlayerBoxes } from '../players/paddingBetweenPlayerBoxes';
import { playerBoxOrigin } from '../players/playerBoxOrigin';
import { playerBoxSize } from '../players/playerBoxSize';
import { Players } from '../types/Players';
import { drawPlayerReserveArmies } from './drawPlayerReserveArmies';

export const drawReserveArmies = 
    (players: Players) => 
    {
        const playerBoxHeight = (playerBoxSize.y - paddingBetweenPlayerBoxes * players.length) / players.length
        const playerOriginPairs = players.map(
            (player, i) => 
            ({ 
                player, 
                origin: translatePointY
                    ((playerBoxHeight + paddingBetweenPlayerBoxes) * i)
                    (playerBoxOrigin)
            })
        )

        return multiplex([
            clearRect(playerBoxOrigin)(playerBoxSize),
            ...playerOriginPairs.map(
                ({player, origin}) => 
                drawPlayerReserveArmies(player)(origin)
            )
        ])
    }