import * as React from 'react';
import { HPLocationSearch } from './HPLocationSearch';
import { HPMap } from './HPMap';
import { materialUiStyle } from '../date/VolunteeringDateRangeSelector';

export const VolunteeringLocationSelect = 
    ({updateOppCoordinates, updateOppAddress, coordinates, coordinatesError, startingAddress = {streetAddress: "", suite: "", city: "", state: "", zip: ""}}) => {
    
    const borderBottom = coordinatesError
        ? "2px solid #f44336"
        : "1px solid #9e9e9e"

    return <div style={{...materialUiStyle, borderBottom}}>
        <HPLocationSearch updateOppAddress={updateOppAddress} updateOppCoordinates={updateOppCoordinates} startingAddress={startingAddress} />    
        <HPMap searchCoordinates={coordinates} />
    </div>
}