import * as React from "react";
import { SearchBar as VolSearchBar } from "./SearchBar";
import { Router } from "../../nav/Router";
import { regexMatch } from "../../../utils/strings/regexMatch";
import {ViewVolunteeringOption} from "./ViewVolunteeringOption";
import { CreateVolOpportunity } from "./CreateVolOpportunity";
import { EditVolOpportunity } from './EditVolOpportunity';
import { isMobile } from "../../site/isMobile";
import { MobileSearchBar } from './MobileSearchBar';

export const VolRoutes = (url, props, router:Router) => 
    router
        .case("/volunteering/search", isMobile() ? <MobileSearchBar {...props} /> : <VolSearchBar {...props} />)
        .case("/volunteering/new", <CreateVolOpportunity {...props} />)
        .matches(regexMatch("/volunteering/.+/edit"), <EditVolOpportunity {...props} />)
        .matches(regexMatch("/volunteering/(.+)"), <ViewVolunteeringOption {...props} />)