import * as React from 'react';
import { Dialog, DialogTitle} from '@material-ui/core';
import { HPButton } from '../forms/HPButton';
import { doNothing } from '../../utils/axioms/doNothing';

type HasIsOpen = {isOpen:boolean}
type HasTitleText = {titleText:string}
type HasOnYesClicked = {onYesClicked: () => void}
type HasOnNoClicked = {onNoClicked: () => void}
type HasChildren = {children: React.ReactNode}
type YesOrNoDialogType = HasIsOpen & HasTitleText & HasOnYesClicked & HasOnNoClicked & Partial<HasChildren>
export const YesOrNoDialog = ({ isOpen, titleText, onYesClicked, onNoClicked, children}:YesOrNoDialogType) => {
    // const { isOpen, titleText, onYesClicked, onNoClicked} = props;
    return <Dialog onClose={doNothing} open={isOpen}>
        <DialogTitle>{titleText}</DialogTitle>
        {children}
        <div style={{textAlign: "center"}}>
            <HPButton onClick={onYesClicked} label="Yes" />
            <HPButton onClick={onNoClicked} label="No" />
        </div>
    </Dialog>
}