import * as React from 'react'
import { Typography } from '@material-ui/core'

export const instructions = <React.Fragment>
    <Typography variant="body2">Instructions</Typography>
    <ol>
        <li>Click on an existing point to highlight it blue (include it)</li>
        <li>Click a new spot to add a point to that spot (blue by default)</li>
        <li>Once new territory is highlighted, copy JSON and add to codebase</li>
    </ol>
</React.Fragment>
