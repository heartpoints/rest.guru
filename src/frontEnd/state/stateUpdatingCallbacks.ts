import { navTo } from "../nav/navTo";
import { onHamburgerClicked } from "../nav/onHamburgerClicked";
import { toggleSideNavOrgSection } from "../nav/toggleSideNavOrgSection";
import { toggleSideNavVolunteeringSection } from "../nav/toggleSideNavVolunteeringSection";
import { navigateAndAutoCollapseIfMobile } from "../nav/navigateAndAutoCollapseIfMobile";
import { onOrgSearchBarValueChange } from "../search/onOrgSearchBarValueChange";
import { onVolSearchBarValueChange } from '../search/onVolSearchBarValueChange';
import { onCelebrationXClicked } from "../modals/onCelebrationXClicked";
import { updateState } from "../castleRisk/general/updateState";
import { onDisplayHomeSnackbar } from "../homePage/onDisplayHomeSnackbar";
import { orgCallbacks } from "../organizations/reducers/orgCallbacks";
import { addNewVolOpportunity } from "../volunteering/reducers/addNewVolOpportunity";
import { updateVolOpportunity } from "../volunteering/reducers/updateVolOpportunity";
import { updateZipFilter } from "../volunteering/reducers/updateZipFilter";
import { updatePermissionMessageOpen } from '../volunteering/reducers/updatePermissionMessageOpen';
import { updateStartDate } from '../volunteering/reducers/updateStartDate';
import { updateEndDate } from '../volunteering/reducers/updateEndDate';
import { Dictionary } from "../../restguru/Dictionary";
import { updateUserZipCoordinates } from '../user/reducers/updateUserZipCoordinates';
import { updateUserCredentials } from '../authentication/updateUserCredentials';
import { toggleLogInButtonEnabled } from '../authentication/toggleLogInButtonEnabled';
import { onFirebaseLoginComplete } from '../authentication/onFirebaseLoginComplete';
import { logoutUserCredentials } from '../authentication/logoutUserCredentials';

type StateUpdatingCallback = (state:any, ...args:any[]) => any

export const stateUpdatingCallbacks:Dictionary<StateUpdatingCallback> = {
    ...orgCallbacks(),
    navTo,
    onCelebrationXClicked,
    onHamburgerClicked,
    toggleSideNavOrgSection,
    toggleSideNavVolunteeringSection,
    onVolSearchBarValueChange,
    onOrgSearchBarValueChange,
    navigateAndAutoCollapseIfMobile,
    onDisplayHomeSnackbar,
    updateState,
    addNewVolOpportunity,
    updateVolOpportunity,
    updateZipFilter,
    updatePermissionMessageOpen,
    updateStartDate,
    updateEndDate,
    updateUserZipCoordinates,
    updateUserCredentials,
    toggleLogInButtonEnabled,
    onFirebaseLoginComplete,
    logoutUserCredentials
};
