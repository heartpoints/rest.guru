import { cyclicPermutations } from "../../../utils/arrays/cyclicPermutations";

export const triangleSegments = cyclicPermutations;
