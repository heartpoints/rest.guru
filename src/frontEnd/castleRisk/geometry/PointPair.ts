import { Point } from "../types/Point";
import { LikePair } from '../../../utils/arrays/LikePair';

export type PointPair = LikePair<Point>;
