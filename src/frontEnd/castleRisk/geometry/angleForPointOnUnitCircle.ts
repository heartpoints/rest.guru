import { Point } from '../types/Point';
export const angleForPointOnUnitCircle = ({ x, y }: Point) => y >= 0
    ? Math.acos(x)
    : Math.PI + Math.acos(-x);
