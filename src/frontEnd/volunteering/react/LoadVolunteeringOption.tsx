import React, { useState } from 'react';
import {Grid, Typography, makeStyles, Theme, Tooltip} from '@material-ui/core';
import { Space } from '../../page/Space';
import { Image } from "../../forms/viewEditToggleables/Image";
import { PageTitle } from '../../page/PageTitle';
import { defaultOrgLogoSrc } from '../../organizations/data/defaultOrgLogoSrc';
import { Page } from '../../page/Page';
import { DeleteButton } from '../../buttons/DeleteButton';
import { YesOrNoDialog } from '../../modals/YesOrNoDialog';
import { isMobile } from '../../site/isMobile';
import { NodeAsNav } from '../../buttons/NodeAsNav';
import { VolHeartpointsDisplay } from './VolHeartpointsDisplay';
import { OpportunityLocationView } from '../../maps/OpportunityLocationView';
import { EditButton } from '../../buttons/EditButton';
import { returnOneDateIfSame } from '../../date/formatDate';
import { HPMarkupView } from '../../forms/HPMarkupView';
import { UserTagDisplay } from '../../forms/UserTagDisplay';

export const LoadVolunteeringOption = ({creatorEmail, creatorProfilePicture="", creatorName, imageThumbnailURL, title, href, jobTitle, jobDescription, startDate, endDate, navTo, deleteOpportunity, address, userCredentials, jobID, coordinates, heartpointsReward}) => {

    const userEmail = userCredentials.email;
    const userIsCreator = userEmail == creatorEmail;
    const { streetAddress, suite, city, state, zip } = address;

    const [shouldShowDialog, toggleDialog] = useState(false);

    const confirmOppDelete = () => 
        deleteOpportunity(jobTitle, jobID)

    const cancelOppDelete = () => 
        toggleDialog(false);

    const deleteCurrentOpportunityRequested = () => 
        toggleDialog(true);

    const navigateToRelatedOrganization = () =>
         navTo(href);

    const renderVolEditButtons = () => 
        userIsCreator && <React.Fragment>
            <EditButton {...{navTo, onClick: ()=> navTo('/volunteering/'+`${jobID}/edit`), buttonTitle: "Edit Volunteering Opportunity"}} />
            <DeleteButton onClick={deleteCurrentOpportunityRequested} {...{buttonTitle: "Delete Volunteering Opportunity"}}/>
        </React.Fragment>

    const renderTimeframe = () =>
         returnOneDateIfSame(startDate, endDate, 'minute', "MM/DD/YYYY hh:mm:ss")

    const renderAddress = () =>
        `${streetAddress} ${suite && suite} ${city}, ${state}, ${zip}`

    return <Page>
        <Grid container direction="row" justify="flex-start" alignItems="center" spacing={2}>
        <div style={{width: "100%", textAlign: "right"}}>
            {isMobile() && renderVolEditButtons()}
        </div>
            <Grid item>
                <NodeAsNav clickHandler={navigateToRelatedOrganization}>
                    <Image field={{value: imageThumbnailURL || defaultOrgLogoSrc}} isEditMode={false} />
                </NodeAsNav>
            </Grid>
            <Grid item>
                <PageTitle>
                    {jobTitle}
                    {!isMobile() && renderVolEditButtons()}
                </PageTitle>
                <Space />
                <VolHeartpointsDisplay heartpointsToDisplay={heartpointsReward} />
                <Space />
                    <Typography variant="h6" style={{color:'blue', textDecoration:'underline'}}>
                            <NodeAsNav clickHandler={navigateToRelatedOrganization}  toottipTitle='Learn more'>{title}</NodeAsNav>
                    </Typography>
                
            </Grid>
        </Grid>
        <Space />
        <HPMarkupView markup={jobDescription} />
        <Space />
        <Typography variant="body2"><strong>Date: </strong>{renderTimeframe()}</Typography>
        <Typography variant="body2"><strong>Address: </strong>{renderAddress()}</Typography>
        <Space />
        <UserTagDisplay clickHandler={()=>navTo(`/user/${creatorEmail}`)} creatorName={creatorName} userProfileImage={creatorProfilePicture}/>
        <Space />
        {coordinates.lat != 0 && <OpportunityLocationView coordinates={coordinates} address={address} />}
        { shouldShowDialog && 
            <YesOrNoDialog 
                isOpen={shouldShowDialog}
                titleText={`Delete ${jobTitle}?`}
                onYesClicked={confirmOppDelete}
                onNoClicked={cancelOppDelete} />}
    </Page>
}