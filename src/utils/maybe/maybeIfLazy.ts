import { Provider } from "../axioms/Provider";
import { None } from "./None";
import { Some } from "./Some";
import { Maybe } from "./MaybeType";
export const maybeIfLazy = <T>(predicate: boolean, valueProviderIfTrue: Provider<T>): Maybe<T> => predicate ? Some(valueProviderIfTrue()) : None;
