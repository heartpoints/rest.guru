import { notIn } from '../../../../utils/list/notIn';
import { territoryEquals } from './territoryEquals';

export const notInTerritories = notIn(territoryEquals);
