import { range } from '../../../utils/list/range';
import { Integer } from '../../../utils/math/Integer';
import { angleForRegularPolygon } from './angleForRegularPolygon';

export const regularAngles = 
    (numAngles: Integer) => 
    range(numAngles).map(i => angleForRegularPolygon(numAngles) * i);
