import { Divide } from '../../../utils/math/DivideOfT';
import { RGBAColor } from '../types/RGBAColor';
import { arrayDivideByNumber } from '../../../utils/arrays/arrayDivideByNumber';
export const colorDivideBy = arrayDivideByNumber as any as Divide<number, RGBAColor>;
