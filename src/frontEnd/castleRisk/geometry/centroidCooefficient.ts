import { Shape } from '../types/Shape';
import { polygonSignedArea } from "./polygonSignedArea";

export const centroidCooefficient = 
    (points: Shape) => 
    1 / (6 * polygonSignedArea(points))
