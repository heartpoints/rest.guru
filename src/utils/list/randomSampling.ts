import { randomIntegerBetweenZeroAnd } from "./randomIntegerBetweenZeroAnd";
import { everythingButIndex } from "./everythingButIndex";
export const randomSampling = (num: number) => <T>(a: Array<T>): Array<T> => {
    return a.length == 0 || num == 0
        ? []
        : (() => {
            const randomIndex = randomIntegerBetweenZeroAnd(a.length);
            const randomElement = a[randomIndex];
            return [randomElement, ...randomSampling(num - 1)(everythingButIndex(randomIndex)(a))];
        })();
};
