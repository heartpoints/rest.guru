import { averageColor } from "./averageColor";
import { red } from "./red";
import { blue } from "./blue";

export const purple = averageColor([red, blue])
