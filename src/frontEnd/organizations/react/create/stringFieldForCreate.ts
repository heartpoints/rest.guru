import { field } from "../../../state/fields/field"
import { setValueForStringProp } from './setValueForStringProp'
import { getValueForStringProp } from "./getValueForStringProp"
import { stringFieldForCreateOrEditOrg } from "../general/stringFieldForCreateOrEditOrg"

export const stringFieldForCreate =
    stringFieldForCreateOrEditOrg
        (getValueForStringProp)
        (setValueForStringProp)
