import { VolOpportunity, Organization } from '../../organizations/data/organization';
import { AllState } from '../../state/AllState';
export type AddNewVolOpportunityType = (currentState: AllState, parentOrg: Organization, newVolopportunity: VolOpportunity) => AllState;
