export type Field<FieldValueType> = {
    value: FieldValueType
    label: string
    setValue(t: FieldValueType): void
    placeholder: string
};
