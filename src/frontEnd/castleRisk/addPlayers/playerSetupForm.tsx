import * as React from 'react';
import { Space } from '../../page/Space'
import { PhaseTitle } from '../general/PhaseTitle'
import { HPButton } from '../../forms/HPButton'
import { PlayerSection } from '../players/PlayerSection'
import { newPlayerForm } from './newPlayerForm'
import { onGameStart } from './onGameStart'
import { Players } from '../types/Players'
import { Player } from '../types/Player'
import { Colors } from '../colors/Colors'
import { UpdateStateForCastleRisk } from '../general/UpdateStateForCastleRisk'

export const playerSetupForm = 
    (allowNewPlayers: boolean) => 
    (players: Players) => 
    (player: Player) => 
    (remainingColors: Colors) => 
    (updateState: UpdateStateForCastleRisk) => 
    <div style={{ width: "100%" }}>
        <PhaseTitle>Who's Playing?</PhaseTitle>
        <p>
            Enter unique player names, one at a time, and for each player, choose the
            desired color for that player's army pieces.
            </p>
        {allowNewPlayers && newPlayerForm(player)(players)(remainingColors)(updateState)}
        <HPButton label="Begin game" onClick={onGameStart(players)(updateState)} disabled={players.length < 2} />
        <Space />
        {PlayerSection(players)()}
    </div>
