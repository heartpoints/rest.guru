import { RGBAColor } from '../types/RGBAColor';
export const clear: RGBAColor = [0, 0, 0, 0];
