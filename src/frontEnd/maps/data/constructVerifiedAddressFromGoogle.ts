type VerifiedAddress = {streetAddress: string, city: string, state: string, zip: string};
type AddressComponent = {long_name:string, short_name:string, types:string[]}
type AddressComponents = AddressComponent[];

export const constructVerifiedAddressFromGoogle = (components:AddressComponents):VerifiedAddress => {

    const extractAddressComponent = (componentType:string):AddressComponent =>{
        const results = components.find(c => c.types.indexOf(componentType) > -1);

        return results ? results : {long_name: "error", short_name: "error", types:[]}
    }

    const streetNumber = extractAddressComponent("street_number").long_name;
    const streetName = extractAddressComponent("route").short_name;
    const city = extractAddressComponent("locality").long_name;
    const state = extractAddressComponent("administrative_area_level_1").short_name;
    const zip = extractAddressComponent("postal_code").short_name;

    const streetAddress = `${streetNumber} ${streetName}`;

    return {streetAddress, city, state, zip};
}