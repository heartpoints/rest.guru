import * as React from "react";
import { regexMatch } from "../../../utils/strings/regexMatch";
import { Router } from "../../nav/Router";
import { ViewUser } from '../../user/ViewUser';
import { CreateOrganization } from "./create/CreateOrganization";
import { EditOrganization } from "./edit/EditOrganization";
import { OrganizationSearchPage } from './search/OrganizationSearchPage';
import { ViewAllOrganizations } from './search/ViewAllOrganizations';
import { ViewOrganization } from "./view/ViewOrganization";

export const OrgRoutes = (url, props, router:Router) => 
    router
        .case("/organizations/search", <OrganizationSearchPage {...props} />)
        .case("/organizations/new", <CreateOrganization {...props} />)
        .case("/organizations/viewAll", <ViewAllOrganizations {...props} />)
        .matches(regexMatch("/organizations/.+/edit"), <EditOrganization {...props} />)
        .matches(regexMatch("/organizations/.+"), <ViewOrganization {...props} />)
        .matches(regexMatch("/user/.+"), <ViewUser {...props} />)
        // .matches(regexMatch("/organizations/.+"), <ViewOrEditOrganization {...props} />)