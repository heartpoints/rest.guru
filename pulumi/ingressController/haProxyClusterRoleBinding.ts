import { haProxyNamespace } from "./haProxyNamespace"
import { haProxyClusterRole } from "./haProxyClusterRole"
import * as k8s from "@pulumi/kubernetes"
import { haProxyServiceAccount } from "./haProxyServiceAccount"

export const haProxyClusterRoleBinding = new k8s.rbac.v1.ClusterRoleBinding("haproxy-ingress-cluster-role-binding", {
  metadata: {
    name: "haproxy-ingress-cluster-role-binding",
    namespace: haProxyNamespace.metadata.name,
  },
  roleRef: {
    apiGroup: "rbac.authorization.k8s.io",
    kind: "ClusterRole",
    name: haProxyClusterRole.metadata.name,
  },
  subjects: [{
    kind: "ServiceAccount",
    name: haProxyServiceAccount.metadata.name,
    namespace: haProxyNamespace.metadata.name,
  }]
})
