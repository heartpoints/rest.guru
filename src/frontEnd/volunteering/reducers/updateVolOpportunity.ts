import { navTo } from '../../nav/navTo';
import {AddNewVolOpportunityType } from '../types/AddNewVolOpportunityType';

export const updateVolOpportunity: AddNewVolOpportunityType = (currentState, parentOrg, newVolopportunity) => {
    const newState = {
        ...currentState,
        organizations: currentState.organizations.map(org => ({
            ...org,
            volOpportunities: org.href == parentOrg.href  
                                ? org.volOpportunities.map(opp => (opp.jobID == newVolopportunity.jobID ? newVolopportunity : opp))
                                : org.volOpportunities
        }))

        };
    
    return navTo(newState, `/volunteering/${newVolopportunity.jobID}`);
};
