import * as k8s from "@pulumi/kubernetes"
import { haProxyNamespace } from "./haProxyNamespace"
import { haProxyIngressDeployment } from "./haProxyIngressDeployment"

const haProxyServiceName = "haproxy-ingress";

export const haProxyService = new k8s.core.v1.Service(haProxyServiceName, {
    metadata: {
        labels: {
            run: haProxyServiceName
        },
        name: haProxyServiceName,
        namespace: haProxyNamespace.metadata.name
    },
    spec: {
        selector: haProxyIngressDeployment.spec.selector.matchLabels,
        type: "LoadBalancer",
        ports: [
            {
                "name": "http",
                "port": 80,
                "protocol": "TCP",
                "targetPort": 80
            },
            {
                "name": "https",
                "port": 443,
                "protocol": "TCP",
                "targetPort": 443
            },
            {
                "name": "stat",
                "port": 1024,
                "protocol": "TCP",
                "targetPort": 1024
            }
        ]
    }
},
{ protect: true })