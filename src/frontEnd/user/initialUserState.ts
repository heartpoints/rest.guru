export const initialUserState = () => ({
    userZipCoordinates: {latitude: 0, longitude: 0},
    userCredentials: {email: ""},
    logInButtonEnabled: false
})