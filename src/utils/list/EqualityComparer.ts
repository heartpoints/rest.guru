export type EqualityComparer<T> = (t2: T) => (t2: T) => boolean;
