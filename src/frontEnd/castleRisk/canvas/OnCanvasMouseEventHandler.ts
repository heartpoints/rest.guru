import { Consumer } from "../../../utils/axioms/Consumer";
import { CanvasMouseEvent } from "./CanvasMouseEvent";

export type OnCanvasMouseEventHandler = Consumer<CanvasMouseEvent>