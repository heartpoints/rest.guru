import { Point } from "../types/Point";
export type Triangle = [Point, Point, Point];
