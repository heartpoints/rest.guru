import React, { useState } from 'react';
import { DeleteButton } from '../../../buttons/DeleteButton';
import { EditButton } from '../../../buttons/EditButton';
import { PageTitle } from '../../../page/PageTitle';
import { Typography, Grid } from '@material-ui/core';
import { Space } from '../../../page/Space';
import { defaultOrgLogoSrc } from '../../data/defaultOrgLogoSrc';
import { Image } from "../../../forms/viewEditToggleables/Image";
import { VolunteeringPreview } from "../../../volunteering/react/VolunteeringPreview";
import { YesOrNoDialog } from '../../../modals/YesOrNoDialog';
import { isMobile } from '../../../site/isMobile';
import { AddOpportunityButton } from '../../../buttons/AddOpportunityButton';
import { HPMarkupView } from '../../../forms/HPMarkupView';
import { UserTagDisplay } from '../../../forms/UserTagDisplay';


//todo: should these also use fields? maybe not "settable" fields but field readers (whether something is loaded / valid / etc)?
//todo: can we have fields that toggle between edit vs display mode over a field?\

export const LoadedOrganization = ({ creatorEmail, creatorName, creatorProfilePicture="", title, mission, imageThumbnailURL, homepage, navTo, href, volOpportunities, deleteOrganization, userCredentials }) => {

    const userEmail = userCredentials.email;
    const userIsCreator = userEmail == creatorEmail;
    const [shouldShowDialog, toggleDialog] = useState(false);

    const confirmOrgDelete = () =>
        deleteOrganization(href, title)

    const cancelOrgDelete = () =>
        toggleDialog(false)

    const deleteCurrentOrganizationRequested = () =>
        toggleDialog(true)

    const hasOpportunities = volOpportunities.length > 0;

    const renderOrgEditButtons = () => userIsCreator && <React.Fragment>
        <EditButton {...{navTo, onClick: () => navTo(`${href}/edit`), buttonTitle: "Edit Organization"}} />
        <DeleteButton onClick={deleteCurrentOrganizationRequested} {...{buttonTitle: "Delete Organization"}} />
    </React.Fragment>

    const renderVolunteeringOpportunities = () => 
        hasOpportunities 
            ? <React.Fragment>
                <Typography variant="h5">
                    Opportunities{userIsCreator && <AddOpportunityButton {...{navTo}}/>}
                </Typography>
                {volOpportunities.map(op => <VolunteeringPreview {...op} {...{navTo}} /> )}
            </React.Fragment>
            : <React.Fragment>
                <Typography variant="h5">
                    No Volunteering Opportunities Yet!{userIsCreator && <AddOpportunityButton {...{navTo}}/>}
                </Typography>
                <Space />
            </React.Fragment>

    return <div>
        <Grid container direction="row" justify="flex-start" alignItems="center" spacing={2}>
            <div style={{width: "100%", textAlign: "right"}}>
                {isMobile() && renderOrgEditButtons()}
            </div>
            <Grid item>
                <Image field={{value: imageThumbnailURL || defaultOrgLogoSrc}} isEditMode={false} />
            </Grid>
            <Grid item>
                <PageTitle>
                    {title}
                    {!isMobile() && renderOrgEditButtons()}
                </PageTitle>
                <Typography variant="caption">
                    {!isMobile() && <strong>Homepage: </strong> }
                    <a href={homepage}>{homepage}</a><br />
                    <UserTagDisplay clickHandler={()=>navTo(`/user/${creatorEmail}`)} creatorName={creatorName}  userProfileImage={creatorProfilePicture}/>
                </Typography>
            </Grid>
        </Grid>
        <Space />
        <HPMarkupView markup={mission} />
        <Space />
        <Space />
        {renderVolunteeringOpportunities()}
        {shouldShowDialog && 
            <YesOrNoDialog 
                isOpen={shouldShowDialog} 
                titleText={`Delete ${title}?`} 
                onYesClicked={confirmOrgDelete} 
                onNoClicked={cancelOrgDelete} />}
    </div>
}