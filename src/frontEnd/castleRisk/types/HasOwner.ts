import { Player } from "./Player";
export type HasOwner = {
    owner: Player;
};
