import { BodiesOfWater } from "./BodiesOfWater";
import { Boundary } from "./Boundary";
import { Territories } from "./Territories";

export type Location = {
    adjacentBodiesOfWater: BodiesOfWater;
    adjacentTerritories: Territories;
    boundary: Boundary;
};
