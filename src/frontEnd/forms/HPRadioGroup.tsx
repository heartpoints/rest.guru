import * as React from 'react';
import { FormControl, FormControlLabel, RadioGroup, Radio, FormLabel } from '@material-ui/core';

type HasGroupName = {groupName: string};
type HasData = {data: any[]}
type HasSelectedItem = {selectedItem: any}
type HasUpdateItemValue = {updateItemValue: (e) => void}
type HPRadioGroupProps = HasGroupName & HasData & HasSelectedItem & HasUpdateItemValue
export const HPRadioGroup = ({groupName, data, selectedItem, updateItemValue}:HPRadioGroupProps) => {

    return <FormControl component="fieldset">
        <FormLabel component="legend">{groupName}</FormLabel>
        <RadioGroup row value={selectedItem} onChange={e => updateItemValue(e.target.value)}>
            {data.map(i => <FormControlLabel value={i} control={<Radio />} label={i} />)}
        </RadioGroup>
    </FormControl>
}