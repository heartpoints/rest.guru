import { lightenColor } from '../../colors/lightenColor';
import { Territory } from '../../types/Territory';
import { white } from '../../colors/white';
import { drawShapeWithPoints } from '../../canvas/drawShapeWithPoints';

export const drawCurrentlyHoveredTerritory = 
    (t: Territory) => 
    drawShapeWithPoints
        (white)
        (white)
        (lightenColor(0.75)(t.empire.color))
        (t.boundary)
