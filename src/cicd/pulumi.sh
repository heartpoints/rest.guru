#!/usr/bin/env bash

source "src/cicd/brew.sh"
source "src/cicd/docker.sh"
source "src/cicd/credentials.sh"

hp_pulumi_access_token() {
     hp_credential "pulumi/access.token"
}

hp_pulumiWithCICDCred() { local args="$@"
    export PULUMI_ACCESS_TOKEN=${PULUMI_ACCESS_TOKEN:-"$(hp_pulumi_access_token)"}
    hp_pulumi "$@"
}

hp_pulumi_via_docker() { export PULUMI_ACCESS_TOKEN; local args="$@"
    hp_docker run -it \
        -e PULUMI_ACCESS_TOKEN \
        -w /app \
        -v $(pwd)/pulumi:/app \
        --entrypoint bash \
        pulumi/pulumi:v1.2.0 \
        -c "yarn install && pulumi \"$@\""
}

hp_pulumi() { local args="$@"
    if command_does_not_exist pulumi; then
        if hp_isMac; then
            (cd pulumi && runCommandSilently yarn install)
            brew_package_run pulumi --cwd pulumi "$@"
        else
            hp_pulumi_via_docker "$@"
        fi
    else
        (cd pulumi && yarn install)
        pulumi --cwd pulumi "$@"
    fi
}

hp_env_name() {
    echo ${CI_ENVIRONMENT_SLUG:-$(string_toLower $(git_currentBranchName))}
}

hp_pulumi_build_and_deploy() { local envName=${1:-$(hp_env_name)}
  export PULUMI_ACCESS_TOKEN
  export shaToBuild="$(git_currentShaOrTempShaIfDirty)"
  export imageRepository="$(hp_gcr)"
  export taggedImageName="$(hp_taggedImageName ${imageRepository} ${shaToBuild})"
  export registryHostAndPort="$(hp_gcr_io)"
  export registryUsername="_json_key"
  export registryPassword="$gcpCicdServiceAccountCredentialsJson"

  pulumi login
  gcloud_cicdAccountLogin
  cd pulumi
    yarn install
    pulumi stack init $envName --non-interactive || pulumi stack select $envName
    export HP_NAMESPACE_NAME=$envName; pulumi up --non-interactive --yes
  cd -
}

hp_pulumi_destroy() { local envName=${1:-$(hp_env_name)}
  export PULUMI_ACCESS_TOKEN

  pulumi login
  cd pulumi
    pulumi stack select $envName
    gcloud_cicdAccountLogin
    pulumi destroy --yes
    pulumi stack rm --yes
  cd -
}
