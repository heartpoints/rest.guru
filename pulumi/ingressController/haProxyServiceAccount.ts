import { haProxyNamespace } from "./haProxyNamespace"
import * as k8s from "@pulumi/kubernetes"

export const haProxyServiceAccount = new k8s.core.v1.ServiceAccount("haproxy-ingress-service-account", {
  metadata: {
    name: "haproxy-ingress-service-account",
    namespace: haProxyNamespace.metadata.name
  },
})
