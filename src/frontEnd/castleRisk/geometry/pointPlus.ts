import { Point } from '../types/Point';
import { Plus } from '../../../utils/math/PlusOfT';

export const pointPlus: Plus<Point> = 
    ({ x, y }) => 
    ({ x: x2, y: y2 }) => 
    ({ x: x + x2, y: y + y2 });
