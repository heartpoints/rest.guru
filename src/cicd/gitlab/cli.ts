import { newBranchForIssue } from "./newBranchForIssue"
import { boardIssuesInOrder } from "./boardIssuesInOrder"

export const cli = async () => {
    const command = process.argv[2]
    const issueNumber = Number.parseInt(process.argv[3])
    const boardIssuesInOrderName = "boardIssuesInOrder"
    const newBranchForIssueName = "newBranchForIssue"
    if (command == boardIssuesInOrderName) {
        console.log(await boardIssuesInOrder())
    }
    else if (command == newBranchForIssueName) {
        await newBranchForIssue(issueNumber)
    }
    else {
        const validCommandNames = [boardIssuesInOrderName, newBranchForIssueName]
        console.log(`Command: ${command} Does Not Exist! Valid Commands: ${validCommandNames}`)
    }
}
