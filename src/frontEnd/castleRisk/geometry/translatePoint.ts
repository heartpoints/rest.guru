import { Point } from '../types/Point';
import { translatePointX } from './translatePointX';
import { translatePointY } from './translatePointY';

export const translatePoint = 
    ({ x:xDiff, y:yDiff }: Point) => 
    (point: Point) => 
    translatePointX(xDiff)(translatePointY(yDiff)(point))
