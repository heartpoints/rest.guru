import * as React from 'react';
import { Page } from '../page/Page';
import { developers } from '../developers/developers';
import { SimpleUserProfile } from '../user/SimpleUserProfile';

export const ViewUser = ({url, organizations, navTo, userCredentials}) => {
    const selectedUserID = (url.path).substring(6);
    const possibleUserEmail = developers.find(d => d == selectedUserID);

    const profileProps = { possibleUserEmail, organizations, navTo, userCredentials}
    return <Page>
        <SimpleUserProfile {...profileProps} />
    </Page>
}