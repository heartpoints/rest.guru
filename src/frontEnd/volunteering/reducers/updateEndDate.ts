import { Moment } from 'moment';

export const updateEndDate = (state, date:Moment) => ({
    ...state,
    endDate: date
});