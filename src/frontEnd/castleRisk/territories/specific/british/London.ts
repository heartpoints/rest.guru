export const London = [
    {
        "x": 258,
        "y": 270
    },
    {
        "x": 289,
        "y": 252
    },
    {
        "x": 305,
        "y": 254
    },
    {
        "x": 315,
        "y": 249
    },
    {
        "x": 324,
        "y": 238
    },
    {
        "x": 339,
        "y": 221
    },
    {
        "x": 351,
        "y": 218
    },
    {
        "x": 360,
        "y": 228
    },
    {
        "x": 365,
        "y": 224
    },
    {
        "x": 381,
        "y": 223
    },
    {
        "x": 391,
        "y": 226
    },
    {
        "x": 391,
        "y": 235
    },
    {
        "x": 372,
        "y": 253
    },
    {
        "x": 378,
        "y": 258
    },
    {
        "x": 383,
        "y": 255
    },
    {
        "x": 387,
        "y": 258
    },
    {
        "x": 376,
        "y": 265
    },
    {
        "x": 316,
        "y": 269
    },
    {
        "x": 313,
        "y": 264
    },
    {
        "x": 303,
        "y": 265
    },
    {
        "x": 298,
        "y": 269
    }
];
