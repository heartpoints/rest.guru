import * as React from 'react';
import { OnCanvasMouseEventHandler } from "./OnCanvasMouseEventHandler";
import { Point } from '../types/Point';

export const gameCanvas = 
    (canvasRef: React.MutableRefObject<null>) => 
    ({x,y}:Point) =>
    (onMouseUp: OnCanvasMouseEventHandler) => 
    (onMouseMove: OnCanvasMouseEventHandler) => 
    <canvas
        ref={canvasRef} 
        width={x}
        height={y}
        {...{onMouseUp, onMouseMove}}
    >
        Your browser does not support the canvas element.
    </canvas>
