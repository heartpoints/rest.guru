import { staticContentAbsolutePaths } from "../config/staticContentAbsolutePaths";
import express from "express";
import { ExpressMiddleware } from "./ExpressMiddleware";

export const staticContentMiddlewares:ExpressMiddleware[] = 
    staticContentAbsolutePaths.map(absPath => express.static(absPath)) //params: request, response; no return type
    //request: requests reader - we read from it (url, cookie, posted data, http headers)
    //resonse: response writer - response.write(...)
    