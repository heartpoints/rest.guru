import { NumPlayers } from './NumPlayers';
import { isInteger } from '../../../utils/math/isInteger';
export const isValidNumPlayers = (num: number): num is NumPlayers => isInteger(num) && [2, 3, 4, 5, 6].includes(num);
