import { NumPlayers } from './NumPlayers';

export const numArmies = (numPlayers: NumPlayers) => ({
    2: 50,
    3: 40,
    4: 35,
    5: 30,
    6: 25
})[numPlayers];
