import { centroidOfPolygon } from '../geometry/centroidOfPolygon';
import { drawSingleArmyPiece } from './drawSingleArmyPiece';
import { RGBAColor } from '../types/RGBAColor';
import { Territory } from '../types/Territory';
export const drawPiecesInTerritory = (color: RGBAColor) => (territory: Territory) => drawSingleArmyPiece(color)(centroidOfPolygon(territory.boundary));
