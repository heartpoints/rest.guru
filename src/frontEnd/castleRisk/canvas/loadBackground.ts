import { memoize } from "../../../utils/memoize";

export const loadBackground = 
    memoize(
        async (imagePath: string): Promise<HTMLImageElement> => {
            const background = new Image();
            background.src = imagePath;
            return new Promise((resolve, reject) => {
                background.onload = () => {
                    resolve(background);
                };
            });
        }
    );
