import { Point } from '../types/Point';

export const distanceBetweenPoints = 
    ({ x: x1, y: y1 }: Point) => 
    ({ x: x2, y: y2 }: Point) => 
    Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
