import { Mapper } from "../axioms/Mapper";
import { Maybe } from "./MaybeType";
export type MaybeFlatmapper<T, S> = Mapper<T, Maybe<S>>;
