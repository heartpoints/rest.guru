import { Moment } from 'moment';

export interface VolOpportunity {
    jobID: string,
    jobTitle: string,
    jobDescription: string,
    startDate: Moment,
    endDate: Moment,
    heartpointsReward: number,
    coordinates: {lat:number, lng: number},
    address: {streetAddress: string, suite: string, city: string, state: string, zip: string}
}

export interface Organization {
    href: string,
    imageThumbnailURL: string,
    title: string,
    mission: string,
    homepage: string,
    creatorEmail: string,
    creatorName: string,
    creatorProfilePicture?: string,
    volOpportunities: Array<VolOpportunity>
}