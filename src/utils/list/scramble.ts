import { Scrambles } from "./Scrambles";
import { randomIntegerBetweenZeroAnd } from "./randomIntegerBetweenZeroAnd";
import { everythingButIndex } from "./everythingButIndex";

export const scramble: Scrambles = a => {
    return a.length == 0
        ? []
        : (() => {
            const randomIndex = randomIntegerBetweenZeroAnd(a.length);
            const scrambledElement = a[randomIndex];
            const stillNeedsScrambling = everythingButIndex(randomIndex)(a);
            return [scrambledElement, ...scramble(stillNeedsScrambling)];
        })();
};
