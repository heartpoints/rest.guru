export const diffsWithNum = 
    (num: number) => 
    (nums: number[]) => 
    nums.map(n => num - n)
