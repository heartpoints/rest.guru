import { udpateStateAndRerender } from "../nav/udpateStateAndRerender";
import { heartpointsMutableState } from "./heartpointsMutableState";

export const makeStatefulCallback = 
    (stateUpdatingCallback) => 
    (...args) => 
    udpateStateAndRerender
        (stateUpdatingCallback(heartpointsMutableState.read(), ...args))