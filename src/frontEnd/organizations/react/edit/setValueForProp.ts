import { udpateStateAndRerender } from '../../../nav/udpateStateAndRerender';
import { heartpointsMutableState } from '../../../state/heartpointsMutableState';
import { mapFirst } from '../../../../utils/arrays/mapFirst';
import { Organizations } from '../../data/Organizations';

export const setValueForProp = 
    (orgPath: string) => 
    (propName: string) => 
    <T>(value: T) => {
        const previousState = heartpointsMutableState.read();
        const newState = {
            ...previousState,
            organizations: [
                ...mapFirst(previousState.organizations as Organizations)(o => o.href == orgPath)(o => ({
                    ...o,
                    [propName]: value
                }))
            ]
        };
        udpateStateAndRerender(newState);
    };
