import { CastleRiskState } from "../types/CastleRiskState";
import { UpdateStateOf } from "../../state/UpdateStateOf";

export type UpdateStateForCastleRisk = UpdateStateOf<CastleRiskState>;
