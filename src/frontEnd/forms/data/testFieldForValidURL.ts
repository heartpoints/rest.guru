export const testFieldForValidURL = (field:string) => {
    const urlRegex = new RegExp("^((https?|ftp)://)?(www\\.)?(((([a-zA-Z0-9.-]+\\.){1,}[a-zA-Z]{2,4}|localhost))|((\\d{1,3}\\.){3}(\\d{1,3})))(:(\\d+))?(/([a-zA-Z0-9-._~!$&'()*+,;=:@/]|%[0-9A-F]{2})*)?(\\?([a-zA-Z0-9-._~!$&'()*+,;=:/?@]|%[0-9A-F]{2})*)?(#([a-zA-Z0-9._-]|%[0-9A-F]{2})*)?$");
    const isValidURL = !field || urlRegex.test(field);

    return isValidURL
        ? undefined
        : "Please enter a valid URL"
}