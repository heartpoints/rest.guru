Castle Risk
===========

This is a fun implementation of the classic castle risk game.

# Next Game Rules to implement:

- highlight border of empire (but not territories)
- place castle
- move to next player
- after last player places castle, move to phase "Choose Territories"
- now, highlight only those territories that are unoccupied
- once all territories are occupied by one piece, move to phase "Choose Hidden Armies"
- each player takes turn choosing their hidden army location from any territory.
- hidden army checkable by clicking icon next to player name
- move to phase "Place Remaining Reserves"
- only highlight territories the current player already occupies
- allow player to place up to five pieces at a time until all players are done
- shuffle order to see who goes first
- all players are dealt 3 initial cards
- on each subsequent start of turn, deal up to 3, then deal 1 more
- show territories that are attackable by current player
- show amount of spoils each player would collect based on current standing
- allow player to stop attacking, they can then place their spoils
- if player clicks attackable area, he then selects from where he will attack (if more than 1, else default)
- players are presented with options for number of dice, defaulted to max
- on button push, dice are rolled, explanation of outcome is given, pieces are removed
- button to "take it to the death" auto rolls until one player loses all pieces
- upon death of player, ask player how many to move to new square, minimum is num dice rolled on last attack
- show num empires, castles for each player in player box
- show statistics of battle win/loss likelihood
- allow attacker to play general (+1 to highest die, discard if highest die loses)
- allow defender to play marshall (+1 to highest die, discard if highest die loses)
- allow player to play reinforcements at beginning of turn (show counter with number for next reinforcements, can place on any currently occupied territory)
- allow player to bring out secret reinforcements at any time (if they occupy the square, only show error if they attempt so people cant guess)
- allow player to attack "adjacent by sea"
- allow diplomat card (prevent another player from being attacked by, or attacking, current player, until current player's next turn)
- allow admiral card - player can attack any water-adjacent territory, but must commit to life or death a predetermined number of troops. If win, keep card, else discard.
- implement castle max die restriction
- implement castle defeat (all losing player pieces removed, winner gets any cards and hidden armies, winner gets castle token but it is no longer on the board, winner must fill territories left empty by loser, if runs out of pieces, other players take turns like during territory claim phase)
- implement winning the game when only one castle remains on the board