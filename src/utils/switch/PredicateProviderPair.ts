import { Provider } from "../axioms/Provider";
import { Pair } from "../arrays/Pair";

export type PredicateProviderPair<V> = Pair<Provider<boolean>, Provider<V>>;
