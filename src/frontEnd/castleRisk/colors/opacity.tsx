import { RGBAColor } from '../types/RGBAColor';
export const opacity = (percentOpaque: number) => ([r, g, b]: RGBAColor) => [r, g, b, percentOpaque] as RGBAColor;
