import { Phase } from "../types/Phase";
import { Switch } from "../../../utils/switch/Switch";
import { NotFound } from '../../nav/NotFound';
import { AddPlayers } from "../addPlayers/AddPlayers";
import { PlaceCastles } from '../placeCastles/PlaceCastles';
import { AdminBorders } from '../admin/AdminBorders';

export const componentForPhase = phase => Switch
    .when(phase)
    .case(Phase.AddPlayers, AddPlayers)
    .case(Phase.PlaceCastles, PlaceCastles)
    .case(Phase.AdminBorders, AdminBorders)
    .result
    .valueOrDefault(NotFound);
