import { Consumer } from "../../utils/axioms/Consumer";
import { Provider } from "../../utils/axioms/Provider";
export type MutableStateType<S> = {
    read: Provider<S>;
    write: Consumer<S>;
};
