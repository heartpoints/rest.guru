import { blue } from './blue';
import { green } from './green';
import { averageColor } from './averageColor';
export const blueGreen = averageColor([blue, green]);
