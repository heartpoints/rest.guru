import { multiplex } from '../../../utils/composition/multiplex';
import { range } from '../../../utils/list/range';
import { numericPlus } from '../../../utils/math/numericPlus';
import { drawRectangle } from '../canvas/drawRectangle';
import { drawText } from '../canvas/drawText';
import { contrastColor } from '../colors/contrastColor';
import { lightenColor } from '../colors/lightenColor';
import { translatePoint } from '../geometry/translatePoint';
import { playerBoxSize } from '../players/playerBoxSize';
import { Player } from '../types/Player';
import { Point } from '../types/Point';
import { armyPieceDiameter } from './armyPieceDiameter';
import { armyPieceRadius } from './armyPieceRadius';
import { draw10ArmyPiece } from './draw10ArmyPiece';
import { drawSingleArmyPiece } from './drawSingleArmyPiece';
import { reservePiecesForPlayer } from './reservePiecesForPlayer';

export const drawPlayerReserveArmies = 
    (player: Player) =>
    (origin: Point) => 
    {
        const { color } = player;
        const reservePieces = reservePiecesForPlayer(player)
        const num10Pieces = Math.floor(reservePieces.length / 10)
        const numSinglePieces = reservePieces.length % 10
        const margin = { x: 10, y: 10}
        const drawPlayer1Piece = drawSingleArmyPiece(color)
        const drawPlayer10Piece = draw10ArmyPiece(color)
        const titleHeight = 30
        const piecesOffset = i => ({ x: i * armyPieceDiameter + armyPieceRadius, y: armyPieceRadius + titleHeight})
        const xLocationByIndex = i => translatePoint(piecesOffset(i))(origin)
        const singleDrawers = range(num10Pieces).map(xLocationByIndex).map(drawPlayer10Piece)
        const tenDrawers = range(numSinglePieces).map(numericPlus(num10Pieces)).map(xLocationByIndex).map(drawPlayer1Piece)
        const playerBGColor = lightenColor(0.5)(color)
        const bgRect = drawRectangle(origin)(playerBoxSize)(playerBGColor)(playerBGColor)
        const textColor = contrastColor(playerBGColor)
        const drawPlayerName = drawText(false)(textColor)(translatePoint(margin)(origin))(player.name);
        return multiplex([
            bgRect,
            drawPlayerName,
            ...singleDrawers, 
            ...tenDrawers
        ])
    };