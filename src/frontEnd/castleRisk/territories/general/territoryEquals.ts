import { Territory } from '../../types/Territory';
export const territoryEquals = (t1: Territory) => (t2: Territory) => t1.name == t2.name;
