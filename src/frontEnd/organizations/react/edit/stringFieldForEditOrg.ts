import { getValueForMatchingOrgPath } from './getValueForMatchingOrgPath'
import { setValueForProp } from './setValueForProp'
import { stringFieldForCreateOrEditOrg } from '../general/stringFieldForCreateOrEditOrg'

export const stringFieldForEditOrg = 
    (orgPath: string) => 
    stringFieldForCreateOrEditOrg
        (getValueForMatchingOrgPath(orgPath))
        (setValueForProp(orgPath))