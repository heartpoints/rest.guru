import { darkenColor } from '../../colors/darkenColor';
import { Territory } from '../../types/Territory';
import { drawTerritoryWithoutDots } from './drawTerritoryWithoutDots';
export const drawDarkTerritory = (t: Territory) => drawTerritoryWithoutDots(darkenColor(0.6)(t.empire.color))(t.boundary);
