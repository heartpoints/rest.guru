import { VolOpportunity, Organization } from '../../organizations/data/organization';
import { AllState } from '../../state/AllState';
export type AddNewVolOpportunityWithMagicStateAlreadyPassed = (parentOrg: Organization, newVolopportunity: VolOpportunity) => AllState;
