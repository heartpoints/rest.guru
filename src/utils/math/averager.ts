import { Divide } from './DivideOfT';
import { Sum } from './SumOfT';

export const averager = 
    <T>(sum: Sum<T>) => 
    (divide: Divide<number, T>) => 
    (averagables: T[]) => 
    divide(averagables.length)(sum(averagables))
