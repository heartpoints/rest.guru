import { territoryEquals } from './territoryEquals';
import { Territories } from '../../types/Territories';
import { Territory } from '../../types/Territory';

export const territoryIn = 
    (territories: Territories) => 
    (territory: Territory) => 
    territories.some(territoryEquals(territory));
