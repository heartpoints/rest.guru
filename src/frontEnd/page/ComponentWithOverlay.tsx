import * as React from "react";
import { Overlay } from "./Overlay";
import { isMobile } from "../site/isMobile";

export interface IComponentWithOverlayProps {
    children: React.ReactNode,
    bgColor: string,
    showOverlay: boolean
}

export const contentToBeHighlightedStyle:React.CSSProperties = {
    "display": "inline-block",
    "position": "relative",
    "left": "0px",
    "top": "0px",
}

export const ComponentWithOverlay = ({children, bgColor, showOverlay}:IComponentWithOverlayProps) => {
    const zIndexThatSitsAboveEverythingElse = 1202;
    const highlightedComponentZIndex = () => isMobile() && !showOverlay ? "auto" : zIndexThatSitsAboveEverythingElse;
    const overlayZIndex = zIndexThatSitsAboveEverythingElse - 1;

    return <React.Fragment>
        <div style={{zIndex: highlightedComponentZIndex(), backgroundColor: bgColor, ...contentToBeHighlightedStyle}}>
            {children}
        </div>
        {showOverlay && <Overlay z={overlayZIndex} /> }
    </React.Fragment>
}