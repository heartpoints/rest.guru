export const textChangeHandler = 
    handlerFromProps => 
    (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        return handlerFromProps(event.target.value);
    }