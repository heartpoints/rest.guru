import * as React from 'react';
import { CastleRiskPhaseProps } from '../types/AddPlayerProps';
import { adminHeader } from './adminHeader';
import { SingleRowOfColumns } from '../../page/SingleRowOfColumns';
import { gameCanvas } from '../canvas/gameCanvas';
import { updateAdminCanvas } from './updateAdminCanvas';
import { rightPanel } from './rightPanel';
import { onMouseUp } from './onMouseUp';
import { onMouseMove } from './onMouseMove';
import { canvasWidth } from '../config/canvasWidth';
import { canvasHeight } from '../config/canvasHeight';
import { loadBackground } from '../canvas/loadBackground';
import { castleRiskBoardBackgroundPath } from './castleRiskBoardBackgroundPath';
import { useAsyncEffect } from '../../react/useAsyncEffect';

export const AdminBorders = 
    (state:CastleRiskPhaseProps) => {
        const {updateState, admin} = state
        const canvasRef = React.useRef(null);
        const rightSection = <div>
            {adminHeader}
            {rightPanel(admin)(updateState)}
        </div>

        useAsyncEffect(async () => {
            const background = await loadBackground(castleRiskBoardBackgroundPath)
            updateAdminCanvas(canvasRef)(state)(background)
        })

        return <div>
            {SingleRowOfColumns(
                gameCanvas
                    (canvasRef)
                    ({x: canvasWidth, y: canvasHeight})
                    (onMouseUp(admin)(updateState))
                    (onMouseMove(updateState)),
                rightSection
            )}
        </div>
    }
    