import * as React from 'react';
import { IconButton, Tooltip } from '@material-ui/core';
import FilterListIcon from '@material-ui/icons/FilterList';

export const FilterByZipButton = ({buttonClickHandler}) => <Tooltip title="Filter By ZIP" placeholder="right">
<IconButton onClick={buttonClickHandler}>
    <FilterListIcon />
</IconButton>
</Tooltip>