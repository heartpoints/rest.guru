import { RGBAColor } from '../types/RGBAColor';
export const plainStringToRGBAColor = (rgbaString: string) => rgbaString.split(",").map(parseFloat) as RGBAColor;
