import { pieceColors } from "../colors/pieceColors";
import { austrianEmpire } from "../territories/specific/austrian/austrianEmpire";
import { britishEmpire } from "../territories/specific/british/britishEmpire";
import { frenchEmpire } from "../territories/specific/french/frenchEmpire";
import { Players } from "../types/Players";
import { initialPlayer } from "./initialPlayer";
import { tommy } from "./tommy";

export const preexistingPlayers: Players = [
    tommy,
    initialPlayer("Tdizzlewizzle", pieceColors[2], [frenchEmpire]),
    initialPlayer("Mike", pieceColors[3], [austrianEmpire]),
    initialPlayer("Peepycakes", pieceColors[4], [britishEmpire]),
];
