import { colorsAreEqual } from '../colors/colorsAreEqual'
import { RGBAColor } from '../types/RGBAColor'
import { Players } from '../types/Players'

export const noPlayerHasColor = 
    (players: Players) => 
    (c: RGBAColor) => 
    !players.some(p => colorsAreEqual(c)(p.color))
