import { haProxyNamespace } from "./haProxyNamespace"
import * as k8s from "@pulumi/kubernetes"
import { defaultBackendPort } from "./defaultBackendPort"

const ingressDefaultBackendServiceName = "ingress-default-backend"
export const ingressDefaultBackendService = new k8s.core.v1.Service(ingressDefaultBackendServiceName, {
  metadata: {
    labels: {
      run: ingressDefaultBackendServiceName
    },
    name: ingressDefaultBackendServiceName,
    namespace: haProxyNamespace.metadata.name,
  },
  spec: {
    selector: {
      run: ingressDefaultBackendServiceName
    },
    ports: [
      {
        name: "port-1",
        port: defaultBackendPort,
        protocol: "TCP",
        targetPort: defaultBackendPort,
      }
    ]
  }
})
