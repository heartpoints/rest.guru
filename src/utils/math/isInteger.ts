import { Integer } from './Integer';
export const isInteger = (num: number): num is Integer => Math.floor(num) == num;
