export const highlightedPlayerBoxStyle: React.CSSProperties = {
    borderStyle: "dashed",
    borderColor: "white",
    borderWidth: "3px"
};
