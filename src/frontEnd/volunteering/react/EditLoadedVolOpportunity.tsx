import React, { useState, Fragment } from 'react';
import { Page } from '../../page/Page';
import { PageTitle } from '../../page/PageTitle';
import { Space } from '../../page/Space';
import { HPTextField } from '../../forms/HPTextField';
import { HPButton } from '../../forms/HPButton';
import { testFieldForMinimumCharacters } from '../../forms/data/testFieldForMinimumCharacters';
import { allFieldsValid } from '../../forms/data/allFieldsValid';
import { Tooltip, InputAdornment } from '@material-ui/core';
import BlockIcon from '@material-ui/icons/Block';
import { commonDateControl } from './commonDateControl';
import { VolunteeringLocationSelect } from '../../maps/VolunteeringLocationSelect';
import { testIntegerInputForNumber } from '../../forms/data/testIntegerInputForNumber';
import { testCoordinatesNotDefault } from '../../forms/data/testCoordinatesNotDefault';
import { isMobile } from '../../site/isMobile';
import { HPRichText } from '../../forms/HPRichText';
import { ShiftBox, TopOrLeft, BottomOrRight } from '../../page/ShiftBox';

export const EditLoadedVolOpportunity = (props) => {
    const { organizations, 
            title, 
            href, 
            jobID,
            jobTitle, 
            jobDescription, 
            startDate: originalStartDate, 
            endDate: originalEndDate,
            coordinates,
            address,
            heartpointsReward,
            updateVolOpportunity} = props

    const updateOppCoordinates = (e) => 
        updateCoordinates(e);

    const updateOppAddress = (a) => 
        updateAddress(a);

    const { dateRangeError, endDate, startDate, uiComponent: dateRangeComponent } = commonDateControl(useState, originalStartDate, originalEndDate)    
    const [ jobTitleFieldValue, setTitle ] = useState(jobTitle)
    const [ jobDescriptionFieldValue, setDescription ] = useState(jobDescription)
    const [ heartpointsRewardString, setHeartpointsRewardString] = useState(`${heartpointsReward}`);
    const [ coordinatesFieldValue, updateCoordinates ] = useState(coordinates);
    const [ addressFieldValue, updateAddress ] = useState(address);

    const jobTitleError = testFieldForMinimumCharacters(jobTitleFieldValue);
    const jobDescriptionError = testFieldForMinimumCharacters(jobDescriptionFieldValue, 20);
    const rewardError = testIntegerInputForNumber(heartpointsRewardString);
    const coordinatesError = testCoordinatesNotDefault(coordinates);

    const shouldEnableButton = allFieldsValid([jobTitleError, jobDescriptionError, dateRangeError, rewardError, coordinatesError]);

    const currentOrganization = organizations.filter(o => o.title == title)[0];
    const updateButtonClickHandler = () => {
        const hpReward = parseInt(heartpointsRewardString, 10);
        updateVolOpportunity(currentOrganization, { jobTitle: jobTitleFieldValue, jobDescription: jobDescriptionFieldValue, jobID, startDate, endDate, coordinates: coordinatesFieldValue, address: addressFieldValue, heartpointsReward: hpReward })
    }

    const nonEditableTextFieldProps = {
        endAdornment: <InputAdornment position="end">
            <Tooltip title="Non-editable"><BlockIcon /></Tooltip>
        </InputAdornment>
    }
        
    const content = <Fragment>
            <ShiftBox>
                <TopOrLeft>
                    <HPTextField 
                    label="Organization"
                        InputProps={nonEditableTextFieldProps}
                        disabled={true} 
                        value={title} 
                    />
                    <Space />
                    {dateRangeComponent}
                    <Space />
                    <HPTextField label="Job Title" errorMessage={jobTitleError} value={jobTitleFieldValue} onChange={e => setTitle(e.target.value)} /><br />
                    <HPRichText errorMessage={jobDescriptionError} changeHandler={value => setDescription(value)} markup={jobDescriptionFieldValue} />
                    <HPTextField label="HeartPoints Rewarded" errorMessage={rewardError} value={heartpointsRewardString} onChange={e => setHeartpointsRewardString(e.target.value)} /><br />
                </TopOrLeft>
                <BottomOrRight>
                    <VolunteeringLocationSelect {...{coordinates, updateOppCoordinates, updateOppAddress, coordinatesError, startingAddress: addressFieldValue}}/>
                </BottomOrRight>
            </ShiftBox>
            {isMobile() && <Space />}
            <HPButton shouldBeInline shouldUseThemeSpacing={!isMobile()} disabled={!shouldEnableButton} label='Done Editing' onClick={updateButtonClickHandler} />
        </Fragment>

    return <Page>
        <PageTitle>{jobTitleFieldValue} (editing)</PageTitle>
        <Space />
        {content}
    </Page>
}
