import { numericSum } from './numericSum';
import { divideBy } from './numericDivide';
import { averager } from './averager';

export const averageNumber = averager(numericSum)(divideBy);
