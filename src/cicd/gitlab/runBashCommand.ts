import * as childProcess from "child_process"

export const runBashCommand = (command: string): Promise<void> => {
    return new Promise((resolve, reject) => {
        childProcess.exec(command, (err, stdout, stderr) => {
            console.log(stdout);
            console.log(stderr);
            err ? reject() : resolve();
        });
    });
};
