import { summer } from './summer'
import { numericMultiply } from './numericMultiply'
import { multiplicativeIdentity } from './multiplicativeIdentity'

export const numericProduct = summer(numericMultiply)(multiplicativeIdentity)
