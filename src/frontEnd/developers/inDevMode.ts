import { isLocalhost } from "./isLocalhost";
import { isDeveloper } from "./isDeveloper";
import { userCredentialsCookieString } from '../authentication/userCredentialsCookieString';

export const inDevMode = () => 
    isLocalhost() || isDeveloper(userCredentialsCookieString())
