import * as React from 'react';

export const Columns = (...children: JSX.Element[]) => <React.Fragment>
    {children.map((child, i) => <td key={i} style={{ verticalAlign: "top", textAlign: "left" }}>{child}</td>)}
</React.Fragment>;
