import { Territory } from "../../types/Territory"
import { Shape } from "../../types/Shape"
import { Empire } from "../../types/Empire"
import { Provider } from "../../../../utils/axioms/Provider"
import { territories } from "./territories"

export const territory =
    (empireProvider: Provider<Empire>) => 
    (name: string) => 
    (boundary: Shape): Territory => 
    ({
        get adjacentTerritories() { return territories().filter(t => t.boundary.some(({x,y}) => boundary.some(({x: x2, y: y2}) => x == x2 && y == y2))) },
        boundary,
        castles: [],
        get empire() { return empireProvider() }, //TODO: Not all territories are in an empire, some are Independent
        name,
        pieces: [],
        adjacentBodiesOfWater: []
    })
