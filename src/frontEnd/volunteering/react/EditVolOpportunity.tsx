import React from 'react';
import { findOrgOpportunityPairByJobId } from '../findOrgOpportunityPairByJobId';
import { combineOrgOppPairToSinglePropsObject } from '../combineOrgOppPairToSinglePropsObject';
import { NotFound } from '../../nav/NotFound';
import { EditLoadedVolOpportunity } from './EditLoadedVolOpportunity';

export const EditVolOpportunity = ({url, organizations, navTo, updateVolOpportunity}) => {
    const selectedJobID = (url.path).substring(14, (url.path).lastIndexOf("/"));
    const possiblyMatchingOrgVolPair = findOrgOpportunityPairByJobId(organizations, selectedJobID)
    const combinedPropsForMatch = possiblyMatchingOrgVolPair.map(combineOrgOppPairToSinglePropsObject)

    return combinedPropsForMatch.mapOrDefault(
        combinedProps => <EditLoadedVolOpportunity  { ...{...combinedProps}} {...{navTo}} {...{organizations}} {...{updateVolOpportunity}} />,
        <NotFound />
    )
}