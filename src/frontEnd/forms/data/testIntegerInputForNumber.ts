export const testIntegerInputForNumber = (value:string) => {
    return isNaN(value as any) || value.trim().length <= 0
        ? "Please Enter a Number Value"
        : undefined
}