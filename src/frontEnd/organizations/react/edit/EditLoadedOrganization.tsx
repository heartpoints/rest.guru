import * as React from 'react';
import { OrgAddEdit } from '../general/OrgAddEdit';
import { PageTitle } from '../../../page/PageTitle';
import { Space } from '../../../page/Space';
import { urlWithGuaranteedProtocol } from '../../../forms/data/urlWithGuaranteedProtocol';


export const EditLoadedOrganization = ({ orgFields }) => {

    const beforeUpdateHandler = () => {
        const homepage = urlWithGuaranteedProtocol(orgFields.homepage.value);
        orgFields.homepage.setValue(homepage);
        history.back();
    }

    const orgAddEditProps = {
        ...orgFields,
        buttonLabel: "Done Editing",
        clickHandler: beforeUpdateHandler
    }

    return <React.Fragment>
        <PageTitle>{orgFields.title.value} <small style={{color: "lightgray"}}>(editing)</small></PageTitle>
        <OrgAddEdit {...orgAddEditProps} />
        <Space />
    </React.Fragment>
}