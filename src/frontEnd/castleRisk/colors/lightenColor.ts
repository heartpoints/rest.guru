import { RGBAColor } from "../types/RGBAColor"
import { diffsWithNum } from "../../../utils/math/diffsWithNum"
import { scale } from "../../../utils/math/scale"
import { addNums } from "../../../utils/math/addNums"

export const lightenColor = 
    (percentage: number) => 
    (color: RGBAColor) => {
        const [r, g, b, a] = color
        const rgb = [r, g, b]
        const amountToPureWhite = diffsWithNum(255)(rgb)
        const amountsToIncrease = scale(percentage)(amountToPureWhite)
        return [...addNums(amountsToIncrease)(rgb), a] as RGBAColor
    }
