import React, { useState, Fragment, CSSProperties } from 'react';
import { InputAdornment, IconButton, Typography, ClickAwayListener } from "@material-ui/core";
import { HPTextField } from "./HPTextField";
import ArrowDropDownCircleOutlinedIcon from '@material-ui/icons/ArrowDropDownCircleOutlined'
import { Popper } from '@material-ui/core';
import { isMobile } from '../site/isMobile';

type HasData = {data: any[]}
type HasSearchBarValue = {searchBarValue: string}
type HasOnSearchBarValueChange = {onSearchBarValueChange:(value:string) => void}
type HasPopperTitle = {popperTitle: string}
type DropdownTextAreaProps = HasData & HasSearchBarValue & HasOnSearchBarValueChange & HasPopperTitle

export const popperStyle:CSSProperties = {
    width: isMobile() ? "75vw" : "310px",
    backgroundColor: "#ffffff",
    borderRadius: "4px",
    boxShadow: "5px 10px 18px #888888",
    marginRight: "20px"
}

export const resultStyle:CSSProperties = {
    display: "block",
    cursor: "pointer",
    padding: "8px 0px"
}

export const popperHeaderStyle:CSSProperties = {
    display: "block",
    backgroundColor: "#3f51b5",
    padding: "20px",
    color: "white",
    borderTopRightRadius: "4px",
    borderTopLeftRadius: "4px"
}

export const DropdownTextArea = ({data, searchBarValue, onSearchBarValueChange, popperTitle}:DropdownTextAreaProps) => {

    const [anchorEl, setAnchorEl] = useState(null);

    const optionClickHandler = (event, text) => {
        onSearchBarValueChange(text);
        handleClick(event);
    }
    const handleClick = event =>
        setAnchorEl(anchorEl ? null : event.currentTarget);

    const open = anchorEl ? true : false;

    const renderResult = (i, index) => {
        const borderBottom = index != data.length - 1 ? "1px solid #bdbdbd" : "none"

        return <div onClick={(e) => optionClickHandler(e, i)} style={{...resultStyle, borderBottom}}>
            <Typography variant="button">{i}</Typography>
        </div>
    }

    return <Fragment>
        <HPTextField placeholder={`Ex: "${data[0]}"`} value={searchBarValue} onChange={e => onSearchBarValueChange(e.target.value)} label="Search Terms" 
            InputProps={{
                endAdornment: (
                    <InputAdornment style={{color: "#616161", cursor: "pointer"}} position="end" onClick={e => handleClick(e)}>
                        <IconButton><ArrowDropDownCircleOutlinedIcon /></IconButton>
                    </InputAdornment>
                )
            }} />
            <Popper open={open} anchorEl={anchorEl} style={{marginTop: "30px", zIndex: 5000}} placement="bottom-end">
                <ClickAwayListener onClickAway={e => handleClick(e)}>
                    <div style={{...popperStyle}}>
                        <div style={popperHeaderStyle}><Typography variant="h6">{popperTitle}</Typography></div>
                        <div style={{padding: "0px 8px"}}>
                            {data.map((i, index) => renderResult(i, index))}
                        </div>
                    </div>
                </ClickAwayListener>
            </Popper>
    </Fragment>
}