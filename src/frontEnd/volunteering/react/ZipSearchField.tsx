import * as React from 'react';
import { InputAdornment, IconButton } from '@material-ui/core';
import ClearIconRounded from '@material-ui/icons/ClearOutlined';
import MyLocationOutlinedIcon from '@material-ui/icons/MyLocationOutlined';
import { HPTextField } from '../../forms/HPTextField';
import { isMobile } from '../../site/isMobile';
import { testFieldForValidZip } from '../../forms/data/testFieldForValidZip';

type HasZipGeneratedByLocation = {zipGeneratedFromLocation:boolean}
type HasZipFilter = {zipFilter:string}
type HasUpdatePermissionMessageOpen = {updatePermissionMessageOpen:(boolean) => void}
type HasOnZipTextChange = {onZipTextChange:(string) => void}
type HasClearUserLocationZipHandler = {clearUserLocationZipHandler:() => void}
type ZipSearchFieldProps = HasZipGeneratedByLocation & HasZipFilter & HasUpdatePermissionMessageOpen & HasOnZipTextChange & HasClearUserLocationZipHandler

export const Adornment = ({children}) => isMobile() ? children : <IconButton>{children}</IconButton>

export const ZipSearchField = ({zipGeneratedFromLocation, zipFilter, updatePermissionMessageOpen, onZipTextChange, clearUserLocationZipHandler}:ZipSearchFieldProps) => {

    const adornmentStyle:React.CSSProperties = isMobile()
        ? {marginRight: "12px", color: "#616161"}
        : {cursor: "pointer"}

    const zipFilterErrorMessage = testFieldForValidZip(zipFilter, "Invalid Zipcode. Filter Not Applied.");
        
    const zipInput = <HPTextField value={zipFilter} onChange={event => onZipTextChange(event.target.value)} label="Zip" helperText={zipFilterErrorMessage}
        InputProps={{
            endAdornment: (<InputAdornment style={adornmentStyle} position="end" onClick={() => updatePermissionMessageOpen(true)}>
                <Adornment><MyLocationOutlinedIcon /></Adornment>
            </InputAdornment>)}} />

    const zipLocked = <HPTextField disabled value="Your Location" 
        InputProps={{
            endAdornment: (<InputAdornment style={adornmentStyle} position="end" onClick={clearUserLocationZipHandler}>
                <Adornment><ClearIconRounded /></Adornment>
            </InputAdornment>)}} />

    return zipGeneratedFromLocation ? zipLocked : zipInput
}