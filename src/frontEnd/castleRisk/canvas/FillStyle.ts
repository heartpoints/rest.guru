export type FillStyle = string | CanvasGradient | CanvasPattern;
