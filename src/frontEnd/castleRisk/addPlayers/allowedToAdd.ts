import { Players } from '../types/Players'
import { Player } from '../types/Player'
import { playerNameRequirementsMet } from './playerNameRequirementsMet';
import { playerNameNotDuplicated } from './playerNameNotDuplicated';

export const allowedToAdd = 
    (players: Players) => 
    (possibleNewPlayer: Player) => 
    playerNameRequirementsMet(possibleNewPlayer)
    && playerNameNotDuplicated(players)(possibleNewPlayer)
