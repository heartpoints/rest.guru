import { randomSampling } from '../../../utils/list/randomSampling';
import { zip } from '../../../utils/list/zip';
import { empires } from '../territories/general/empires';
import { Players } from '../types/Players';
export const assignRandomEmpires = (players: Players): Players => zip(players, randomSampling(players.length)(empires()))
    .value
    .map(([player, empire]) => ({
        ...player,
        castles: [{ empire, owner: player, territory: undefined }],
        empires: []
    }));
