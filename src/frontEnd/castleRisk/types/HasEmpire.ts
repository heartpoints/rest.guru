import { Empire } from "./Empire";
export type HasEmpire = {
    empire: Empire;
};
