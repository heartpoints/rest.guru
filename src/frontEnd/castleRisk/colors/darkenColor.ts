import { RGBAColor } from "../types/RGBAColor"

export const darkenColor = 
    (percentage: number) =>
    (color: RGBAColor) => {
        const [r, g, b, a] = color
        const adjust = 1 - percentage
        return [
            ...[r, g, b].map(c => c * adjust),
            a
        ] as RGBAColor
    }
