#!/usr/bin/env bash

source "src/cicd/file.sh"
source "src/cicd/cli.sh"
source "src/cicd/git.sh"
source "src/cicd/logging.sh"
source "src/cicd/brew.sh"

hp_pointToAndRunMinikubeDockerDaemon() {
    hp_minikube_start
    pointToMinikubeDockerDaemon    
}

pointToMinikubeDockerDaemon() {
    local dockerEnvCode="$(hp_minikube docker-env)"
    eval $dockerEnvCode
}

hp_dockerMK() { local args="$@"
    hp_pointToAndRunMinikubeDockerDaemon
    hp_docker "$@"
}

minikubeRegistryHostAndPort() { 
    echo "locahost:5000"
}

docerkIOHostAndPort() {
    echo "docker.io"
}

dockerDotIOImageName() { local shaToBuild=$1
    echo "$(hp_taggedImageName $(docerkIOHostAndPort)/heartpointsorg ${shaToBuild})"
}

hp_minikubeTaggedImageName() { local shaToBuild=$1
    echo "$(hp_taggedImageName $(minikubeRegistryHostAndPort) ${shaToBuild})"
}

hp_minikubeBuild_help() { echo "<taggedImageName> using minikube's docker daemon, build image and tag with minikube metadata"; }
hp_minikubeBuild() { local taggedImageName=$1; local shaToReportInHttpHeaders=$2
    requiredParameter "taggedImageName" "${taggedImageName}"
    requiredParameter "shaToReportInHttpHeaders" "${shaToReportInHttpHeaders}"
    hp_pointToAndRunMinikubeDockerDaemon
    hp_minikube_start
    hp_buildAndTagImage "${taggedImageName}" "${shaToReportInHttpHeaders}"
    hp_dockerTestImage "${taggedImageName}"
}

hp_minikubeDestroyEnvironment_help() { echo "if minikube dev environment is running, destroys it"; }
hp_minikubeDestroyEnvironment() {
    hp_minikube delete
}

hp_urlOfMinikubeWebsite() {
    echo "https://$(hp_minikube ip)"
}

hp_minikubeOpenWebsite_help() { echo "assuming site is running in minikube locally, open web browser to home page"; }
hp_minikubeOpenWebsite() {
    open "$(hp_urlOfMinikubeWebsite)"
}

minikubeInstallLogPath() {
    hp_log_path "mikikube-installation.log"
}

hp_minikube_update() {
    if hp_minikube update-check; then
        hp_brew cask update minikube
    fi
}

hp_minikube() { local args="$@:-"
    virtualbox_install_globally
    hp_brew_cask_run minikube "${@:-}"
}

minikube_addonNotEnabled() { local addonName=$1
    ! hp_minikube addons list | grep "${addonName}: enabled" > /dev/null
}

hp_minikubeAddonEnable() { local addonToEnable=$1
    if minikube_addonNotEnabled "${addonToEnable}"; then
        hp_minikube addons enable "${addonToEnable}"
    fi
}

hp_minikubeEnableIngress() {
    hp_minikubeAddonEnable ingress
}

hp_minikubeEnableDockerRegistry() {
    hp_minikubeAddonEnable registry
}

hp_minikubeDashboard_help() { echo "open minikube dashboard in web browser"; }
hp_minikubeDashboard() {
    hp_minikube dashboard 
}

minikube_vm_memory_mb() {
    echo "8192"
}

hp_minikube_start() {
    if ! hp_minikube_isRunning; then
        #TODO: The --mount stuff is to try to allow pods read/write access for dev env / filewatcher purposes
        #Currently, from within minikube vm cannot write to those locations, there is some permission error.
        # hp_minikube start --memory "$(minikube_vm_memory_mb)" --mount-string="$(pwd):/heartpointsWorkspace" --mount
        hp_minikube start --memory "$(minikube_vm_memory_mb)"
    fi
    hp_minikubeEnableIngress
    hp_minikubeEnableDockerRegistry
}

hp_minikube_stop() {
    if hp_minikube_isRunning; then
        hp_minikube stop
    fi
}

hp_minikube_isRunning() {
    hp_minikube status | runCommandSilently grep "host: Running"
}

hp_kubectl() { local args="$@"
    set -e
    if hp_isMac; then
        brew_package_run "kubernetes-cli" "$@"
    else
        kubectl "$@"
    fi
}