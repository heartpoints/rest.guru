import React, { Fragment } from 'react';
import { Space } from '../../page/Space';
import { Typography } from '@material-ui/core';
export const noOrgsContent = <Fragment>
    <h1>
        [friendly oops icon here]
        </h1>
    <Typography variant="body1">To add a volunteering opportunity, you'll need to be an administrator for an organization.
        Each organization can only have one administrator.</Typography>
    <Space />
    <Typography variant="body1">If your organization exists already
        (<a href="/organizations/search">you can check here</a>), please find it and request that
        the administrator add the opportunity for you. The ability for multiple users to administrate
        a single organization is forthcoming, thanks for your patience!</Typography>

    <Space />
    <Typography variant="body1">If you'd like to set up your organization in heartpoints and become its administrator,
        please visit the <a href="/organizations/new">create new organization</a> page. Thanks!</Typography>
</Fragment>;
