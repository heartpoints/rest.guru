import { TypePredicate } from "../predicates/TypePredicate";
import { Some } from "../maybe/Some";
import { None } from "../maybe/None";
import { Maybe } from "../maybe/MaybeType";

export const firstTypeMatch = <T, S extends T>(array: T[], predicate: TypePredicate<T, S>): Maybe<S> => {
    for (const item of array) {
        if (predicate(item))
            return Some(item);
    }
    return None;
};
