import { maybe } from '../../../../utils/maybe/maybe';
import { heartpointsMutableState } from '../../../state/heartpointsMutableState';

export const getValueForMatchingOrgPath = 
    (orgPath: string) => 
    (fieldName: string) => 
        maybe(
            heartpointsMutableState
                .read()
                .organizations
                .find(o => o.href == orgPath)
        )
        .mapOrDefault(o => o[fieldName], undefined);
