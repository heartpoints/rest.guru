import { OnCanvasMouseEventHandler } from '../canvas/OnCanvasMouseEventHandler';
import { UpdateStateForCastleRisk } from '../general/UpdateStateForCastleRisk';
import { AdminState } from '../types/AdminState';
import { pointFromMouseEvent } from '../canvas/pointFromMouseEvent';
import { overlappingPointsWithinAdminShapes } from './overlappingPointsWithinAdminShapes';

export const onMouseUp = 
    (admin:AdminState) =>
    (updateState: UpdateStateForCastleRisk): OnCanvasMouseEventHandler => 
    (e) => {
        const mousePoint = pointFromMouseEvent(e)
        const overlappingPoints = overlappingPointsWithinAdminShapes(mousePoint)
        const newPoint = overlappingPoints.length > 0 ? overlappingPoints[0] : pointFromMouseEvent(e)
        return updateState({
            admin: {
                ...admin,
                currentlyDrawingShape: [...admin.currentlyDrawingShape, newPoint],
            }
        })
    }
    
