import { List } from "../../../../utils/list/List";
import { empiresWithIndependents } from "./empires"
import { Territories } from "../../types/Territories";
import { Provider } from "../../../../utils/axioms/Provider";

export const territories:Provider<Territories> = () => [
    ...List(empiresWithIndependents()).flatMap(e => List(e.territories)).asArray,
]