import * as React from 'react';

export const Rows = (...children: JSX.Element[]) => <table style={{width: "100%"}}>
    <tbody>
        {children.map((child, i) => <tr key={i}>{child}</tr>)}
    </tbody>
</table>;
