import { statefulCallbacks } from "./statefulCallbacks";

export const sitePropsFromState = 
    (state) => 
    ({
        ...state,
        ...statefulCallbacks
    })