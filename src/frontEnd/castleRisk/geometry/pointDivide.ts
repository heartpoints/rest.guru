import { Point } from '../types/Point';
import { Divide } from '../../../utils/math/DivideOfT';

export const pointDivide: Divide<Point, number> = 
    ({ x, y }) => 
    (divisor) => 
    ({
        x: x / divisor,
        y: y / divisor
    });
