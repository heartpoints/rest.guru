import { Player } from '../types/Player';
export const initialPlayer = (name, color, empires): Player => ({
    name,
    color,
    castles: [],
    territories: [],
    empires,
    pieces: [],
});
