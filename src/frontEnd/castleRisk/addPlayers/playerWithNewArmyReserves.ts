import { range } from '../../../utils/list/range';
import { Player } from '../types/Player';
import { Integer } from '../../../utils/math/Integer';
import { newArmyReservePiece } from '../pieces/newArmyReservePiece';
export const playerWithNewArmyReserves = (numPieces: Integer) => (player: Player): Player => ({
    ...player,
    pieces: range(numPieces).map(() => newArmyReservePiece(player))
});
