import { Divide } from "./DivideOfT";

export const divideBy:Divide<number, number> = 
    a => 
    b => 
    b / a