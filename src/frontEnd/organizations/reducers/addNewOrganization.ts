import { navTo } from "../../nav/navTo";
import { initialOrganization } from "../data/initialOrganization";
import { urlWithGuaranteedProtocol } from '../../forms/data/urlWithGuaranteedProtocol';

export const addNewOrganization = (state, creatorEmail) => {
    const { organizations, possibleNewOrganization } = state;
    const href = `/organizations/${organizations.length + 1}`;

    const homepage = urlWithGuaranteedProtocol(possibleNewOrganization.homepage);

    const newOrganization = {
        ...possibleNewOrganization,
        href,
        creatorEmail,
        homepage,
        volOpportunities: []
    };
    const stateWithNewOrg = {
        ...state,
        organizations: [...state.organizations, newOrganization],
        possibleNewOrganization: initialOrganization(),
    };

    return navTo(stateWithNewOrg, href);
};
