import { multiplex } from '../../../../utils/composition/multiplex';
import { Point } from '../../types/Point';
import { territories } from '../general/territories';
import { currentlyHoveredTerritory } from './currentlyHoveredTerritory';
import { drawNormalTerritory } from './drawNormalTerritory';
import { drawTerritoryShapesWhenHovering } from './drawTerritoryShapesWhenHovering';

export const drawTerritoryShapes = 
    (mousePoint: Point) => 
    currentlyHoveredTerritory(mousePoint).mapOrDo(
        drawTerritoryShapesWhenHovering, 
        () => multiplex(territories().map(drawNormalTerritory))
    )
