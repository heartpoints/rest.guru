import { multiplex } from '../../../../utils/composition/multiplex';
import { Territories } from '../../types/Territories';
import { Territory } from '../../types/Territory';
import { drawNormalTerritory } from './drawNormalTerritory';
import { notInTerritories } from './notInTerritories';
import { territories } from './territories';

export const drawNonNearbyTerritories = 
    (adjacentTerritories: Territories) => 
    (currentTerritory: Territory) => 
    {
        const territoriesOfInterest = [...adjacentTerritories, currentTerritory];
        const otherTerritories = territories().filter(notInTerritories(territoriesOfInterest));
        return multiplex(otherTerritories.map(drawNormalTerritory))
    };
