import { makeStatefulCallback } from "./makeStatefulCallback";
import { mapProperties } from "../../utils/list/mapProperties";
import { stateUpdatingCallbacks } from "./stateUpdatingCallbacks";

export const statefulCallbacks = 
    mapProperties(
        stateUpdatingCallbacks, 
        makeStatefulCallback
    );
