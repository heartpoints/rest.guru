import { AddNewVolOpportunityWithMagicStateAlreadyPassed } from "./AddNewVolOpportunityWithMagicStateAlreadyPassed";
export type WithAddNewVolOpportunityWithMagicStateAlreadyPassed = {
    addNewVolOpportunity: AddNewVolOpportunityWithMagicStateAlreadyPassed;
};
