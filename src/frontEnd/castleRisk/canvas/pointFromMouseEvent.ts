import { CanvasMouseEvent } from './CanvasMouseEvent';
import { Point } from '../types/Point';

export const pointFromMouseEvent = 
    ({ nativeEvent: { offsetX, offsetY } }: CanvasMouseEvent): Point => 
    ({ x: offsetX, y: offsetY });
