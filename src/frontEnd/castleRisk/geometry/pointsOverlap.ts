import { Point } from '../types/Point';
import { pointRadius } from '../config/pointRadius';
import { distanceBetweenPoints } from './distanceBetweenPoints';
export const pointsOverlap = (p1: Point) => (p2: Point) => distanceBetweenPoints(p1)(p2) <= pointRadius;
