import { Shape } from '../types/Shape';
import { centroidX } from './centroidX';
import { centroidY } from './centroidY';

export const centroidOfPolygon = 
    (points: Shape) => 
    ({
        x: centroidX(points),
        y: centroidY(points),
    })
