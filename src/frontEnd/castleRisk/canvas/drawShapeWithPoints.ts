import { drawPoint } from './drawPoint';
import { drawPolygon } from './drawPolygon';
import { RGBAColor } from '../types/RGBAColor';
import { Shape } from '../types/Shape';
import { multiplex } from '../../../utils/composition/multiplex';

export const drawShapeWithPoints = 
    (strokeColor: RGBAColor) => 
    (pointColor: RGBAColor) => 
    (fillColor: RGBAColor) => 
    (shape: Shape) => 
    multiplex([
        drawPolygon(false)(strokeColor)(fillColor)(shape),
        ...shape.map(drawPoint(pointColor))
    ])
