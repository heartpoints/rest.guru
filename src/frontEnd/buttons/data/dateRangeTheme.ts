export const dateRangeTheme = {
    DateRange: {
        background: "#ffffff"
    },
    Calendar: {
        background: "transparent",
        color: "#95a5a6",
        boxShadow: "0 0 1px #eee",
        padding: "0px"
    },
    MonthAndYear: {
        background: "red",
        color: "#fff",
        padding: "20px 10px",
        height: "auto"
    },
    MonthButton: {
        background: "#fff"
    },
    Weekday: {
        background: "#3AA6DF",
        color: "#fff",
        padding: "10px",
        height: "auto",
        fontWeight: "normal"
    },
    DaySelected: {
        background: "#55B1E3"
    },
    DayActive: {
        background: "#55B1E3",
        boxShadow: "none"
    },
    DayInRange: {
        background: "#eee",
        color: "#55B1E3"
    }
}