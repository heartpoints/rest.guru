import { Territory } from '../../types/Territory';
import { Empire } from '../../types/Empire';
import { Territories } from '../../types/Territories';
import { not } from '../../../../utils/list/not';
import { territoryEquals } from "./territoryEquals";

export const otherEmpireTerritories = 
    (empire: Empire) => 
    (territory: Territory): Territories => 
    empire.territories.filter(not(territoryEquals(territory)))
