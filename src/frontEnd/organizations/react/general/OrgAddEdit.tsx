import * as React from 'react';
import { textChangeHandler } from "../../../forms/textChangeHandler";
import { ImagePicker } from '../../../forms/ImagePicker';
import { Space } from '../../../page/Space';
import { HPTextField } from '../../../forms/HPTextField';
import { HPButton, OnClick } from '../../../forms/HPButton';
import { Field } from '../../../state/fields/types/Field';
import { testFieldForMinimumCharacters } from '../../../forms/data/testFieldForMinimumCharacters';
import { testFieldForValidURL } from '../../../forms/data/testFieldForValidURL';
import { allFieldsValid } from '../../../forms/data/allFieldsValid';
import { HPRichText } from '../../../forms/HPRichText';

export type OrgAddEditProps = { 
    title: Field<string>, 
    homepage: Field<string>,
    mission: Field<string>,
    imageThumbnailURL: Field<string>,
    buttonLabel: string,
    clickHandler: OnClick
}

//todo: why does editing the rich html ui cause the text to update even
//if we do not update the state?
export const OrgAddEdit = (props:OrgAddEditProps) => {
    const { title, homepage, mission, imageThumbnailURL, buttonLabel, clickHandler } = props
    const titleError = testFieldForMinimumCharacters(title.value)
    const missionError = testFieldForMinimumCharacters(mission.value, 20)
    const homepageError = testFieldForValidURL(homepage.value);
    const shouldEnableButton = allFieldsValid([titleError, missionError, homepageError]);

    return <React.Fragment>
        <Space />
        <ImagePicker field={imageThumbnailURL} /><br />
        <HPTextField {...title} errorMessage={titleError} onChange={textChangeHandler(title.setValue)} /><br />
        <HPTextField {...homepage} errorMessage={homepageError} onChange={textChangeHandler(homepage.setValue)} /><br />
        <HPRichText errorMessage={missionError} changeHandler={value => textChangeHandler(mission.setValue(value))} markup={mission.value} />
        <HPButton label={buttonLabel} onClick={clickHandler} disabled={!shouldEnableButton} />
    </React.Fragment>
}