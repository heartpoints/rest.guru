import { galecia } from "./galecia";
import { hungary } from "./hungary";
import { Empire } from "../../../types/Empire";
import { yellow } from "../../../colors/yellow";
import { territory } from "../../general/territory";
import { Vienna } from "./Vienna";
import { Bohemia } from "./Bohemia";
import { Trieste } from "./Trieste";
import { darkenColor } from "../../../colors/darkenColor";

export const austrianEmpire:Empire = {
    color: darkenColor(0.2)(yellow),
    name: "Austrian",
    get territories() {
        const austrianTerritory = territory(() => austrianEmpire)
        return [
            austrianTerritory("Galecia")(galecia),
            austrianTerritory("Hungary")(hungary),
            austrianTerritory("Vienna")(Vienna),
            austrianTerritory("Bohemia")(Bohemia),
            austrianTerritory("Trieste")(Trieste),
        ]
    }
}