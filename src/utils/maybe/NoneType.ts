import { Maybe } from "./MaybeType";

export type NoneType = Maybe<never>;
