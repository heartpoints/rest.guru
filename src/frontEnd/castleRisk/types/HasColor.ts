import { RGBAColor } from "./RGBAColor";
export type HasColor = {
    color: RGBAColor;
};
