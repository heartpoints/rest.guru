import React from 'react';
import { Page } from '../../../page/Page';
import { PageTitle } from '../../../page/PageTitle';
import { Space } from '../../../page/Space';
import { SearchBar as OrgSearchBar } from './SearchBar';

export const OrganizationSearchPage = (props) => {
    return <Page>
        <PageTitle>Organization Search...</PageTitle>
        <Space />
        <OrgSearchBar {...props} />
    </Page>
}