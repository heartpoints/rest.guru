import * as k8s from "@pulumi/kubernetes";
import * as pulumi from "@pulumi/pulumi";
import { haProxyNamespace } from "./haProxyNamespace";

const config = new pulumi.Config();

export const heartpointsWildcardTlsSecret = new k8s.core.v1.Secret("heartpoints-wildcard-tls-secrets", {
  metadata: {
    name: "heartpoints-wildcard-tls-secrets",
    namespace: haProxyNamespace.metadata.name,
  },
  data: {
    "tls.crt": config.getSecret("tlsCertBase64Encoded"),
    "tls.key": config.getSecret("tlsPrivKeyBase64Encoded"),
  }
})