import { HasCastleRiskState } from "../types/HasCastleRiskState";
import { initialCastleRiskStateValue } from "./initialCastleRiskStateValue";
import { usePreloaded } from "../config/usePreloaded";
import { initialStateForCastlePlacementTesting } from "./initialStateForCastlePlacementTesting";

export const initialCastleRiskState = 
    ():HasCastleRiskState => ({
        castleRisk: usePreloaded 
            ? initialStateForCastlePlacementTesting
            : initialCastleRiskStateValue
    })