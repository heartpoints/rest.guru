import { lightenColor } from '../../colors/lightenColor';
import { Territory } from '../../types/Territory';
import { drawShapeWithPoints } from '../../canvas/drawShapeWithPoints';
import { gray } from '../../colors/gray';

export const drawAdjacentTerritory = 
    ({ empire: { color }, boundary }: Territory) => 
    (context: CanvasRenderingContext2D) => 
    drawShapeWithPoints
        (gray)
        (gray)
        (lightenColor(0.5)(color))
        (boundary)
        (context);
