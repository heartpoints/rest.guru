import { Dictionary } from "../../restguru/Dictionary";
import { Mapper } from "../axioms/Mapper";

export const mapProperties = 
    <T, S>(obj:Dictionary<T>, mapper:Mapper<T, S>):Dictionary<S> =>
    Object.entries(obj).reduce(
        (obj, [key,value]) => ({...obj, [key]: mapper(value) }),
        {}
    )