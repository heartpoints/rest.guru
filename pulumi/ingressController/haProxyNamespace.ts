import * as k8s from "@pulumi/kubernetes"

const namespaceName = "haproxy-controller";

export const haProxyNamespace = new k8s.core.v1.Namespace(namespaceName, {
  metadata: {
    name: namespaceName,
  },
})
