import { RGBAColor } from "../types/RGBAColor";
export const rgbaToCSSString = (color: RGBAColor) => `rgba(${color.join(",")})`;
