import { Point } from "../types/Point";
export const pointToArrayPoint = ({ x, y }: Point): [number, number] => [x, y];
