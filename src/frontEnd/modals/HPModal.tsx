import * as React from 'react';
import Modal from '@material-ui/core/Modal';
import ClearIconRounded from '@material-ui/icons/ClearOutlined';
import { Typography } from '@material-ui/core';
import { isMobile } from '../site/isMobile';

export const modalStyle:React.CSSProperties = {
    "backgroundColor": "white",
    "borderRadius": "10px",
    "textAlign": "center",
    "padding": "10px",
    "position": "absolute",
    "top": "50%",
    "left": "50%",
    "transform": "translate(-50%, -50%)"
}

export interface IHPModalProps{
    title: string,
    subtitle: React.ReactNode,
    imageURL: string,
    children: React.ReactNode,
    onXClicked(): void,
}

export const HPModal = (props:IHPModalProps) => {
    const { modalWidth, titleVariant } = isMobile() 
        ? {modalWidth: "90vw", titleVariant: "h5" as "h5" }
        : {modalWidth: "50%", titleVariant: "h2" as "h2" } ;

    return(
        <Modal
            open={true}
            disableAutoFocus={true}>
            <div style={{width: modalWidth, ...modalStyle}}>
                <ClearIconRounded onClick={props.onXClicked} style={{float: "right"}}/>
                <Typography variant={titleVariant} style={{clear: "both", margin: "0px", color: "red"}}>
                    {props.title}
                </Typography>
                {props.subtitle}
                <img 
                    style={{width: "35%", margin: "15px 0"}} 
                    src={props.imageURL} />
                {props.children}
            </div>
        </Modal>
    )
}