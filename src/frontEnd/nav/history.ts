import { createBrowserHistory } from "history";
import { memoize } from "../../utils/memoize";

export const history = memoize(createBrowserHistory)