import { first } from "../list/first";
import { MaybeFlatmapper } from "./MaybeFlatmapper";
import { Maybe } from "./MaybeType";

export const firstSuccessfulMapResult = <T, S extends T>(ts: T[], f: MaybeFlatmapper<T, S>): Maybe<S> => {
    return first(ts, t => f(t).hasValue()).flatMap(t => f(t));
};
