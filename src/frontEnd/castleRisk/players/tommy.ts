import { initialPlayer } from "./initialPlayer";
import { pieceColors } from "../colors/pieceColors";
import { russianEmpire } from "../territories/specific/russian/russianEmpire";
export const tommy = initialPlayer("Tommy", pieceColors[0], [russianEmpire]);
