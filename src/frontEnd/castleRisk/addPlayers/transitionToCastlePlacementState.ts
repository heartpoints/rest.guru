import { compose } from '../../../utils/composition/compose';
import { scramble } from '../../../utils/list/scramble';
import { Phase } from '../types/Phase';
import { Players } from '../types/Players';
import { assignRandomEmpires } from './assignRandomEmpires';
import { playersWithNewArmyReserves } from './playersWithNewArmyReserves';

export const transitionToCastlePlacementState = 
    (playersWithoutEmpires: Players) => 
    {
        const updatePlayers = compose(scramble, assignRandomEmpires, playersWithNewArmyReserves);
        const players = updatePlayers(playersWithoutEmpires);
        return {
            phase: Phase.PlaceCastles,
            players,
            currentPlayer: players[0],
            newPlayer: undefined,
        };
    };
