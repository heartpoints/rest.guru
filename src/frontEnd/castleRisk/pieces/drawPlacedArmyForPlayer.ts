import { multiplex } from '../../../utils/composition/multiplex';
import { Player } from '../types/Player';
import { drawPiecesInTerritory } from './drawPiecesInTerritory';
export const drawPlacedArmyForPlayer = (player: Player) => multiplex(player.territories.map(drawPiecesInTerritory(player.color)));
