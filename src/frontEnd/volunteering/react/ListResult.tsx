import * as React from 'react';
import { EntityPreview } from '../../page/EntityPreview';
import { CombinedOrgAndVolOpportunity } from '../types/CombinedOrgAndVolOpportunity';
import { returnOneDateIfSame } from '../../date/formatDate';
import { ellipsis } from '../../../utils/strings/ellipsis';

type HasEntity = {entity:CombinedOrgAndVolOpportunity}
type HasDistance = {distance:number}
type HasNavTo = { navTo: (url:string) => void }
type ListResultProps = HasEntity & HasDistance & HasNavTo

export const ListResult = ({entity, distance, navTo}:ListResultProps) => {
    const {jobTitle, imageThumbnailURL, address, startDate, endDate, jobDescription, jobID, coordinates} = entity;
    const {streetAddress, suite, city, state, zip} = address;

    return <EntityPreview 
        title={jobTitle} 
        image={imageThumbnailURL}
        details={`<p>${streetAddress} ${suite && suite}<br />
            ${city}, ${state}, ${zip}</p>
            <p>${returnOneDateIfSame(startDate, endDate, "day")}</p>
            <p>${ellipsis(jobDescription, 200)}</p>
            <p>${distance} miles away</p>`}  
        clickHandler={() => navTo(`/volunteering/${jobID}`)}  /> 
}