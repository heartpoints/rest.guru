import { isInForEqualityDefinition } from './isInForEqualityDefinition';
import { doubleEqual } from './doubleEqual';

export const isIn = isInForEqualityDefinition(doubleEqual);
