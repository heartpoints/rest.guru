import { CastleRiskState } from "./CastleRiskState";
import { HasUpdateStateForCastleRisk } from "./CastleRiskStateUpdater";

export type CastleRiskPhaseProps = HasUpdateStateForCastleRisk & CastleRiskState;
