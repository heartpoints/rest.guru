import { HasName } from "./HasName";
import { HasTerritories } from "./HasTerritories";
import { HasColor } from "./HasColor";

export type Empire = HasName & HasColor & HasTerritories;
