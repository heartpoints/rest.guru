import { testFieldForMinimumCharacters } from "./testFieldForMinimumCharacters";
import { testIntegerInputForNumber } from "./testIntegerInputForNumber";
import { testFieldForMaximumCharacters } from './testFieldForMaximumCharacters'

export const testFieldForValidZip = (zip:string, message = "Please enter a valid Zip Code") => {
    const isUnderMinimum = testFieldForMinimumCharacters(zip, 5);
    const isNotNumber = testIntegerInputForNumber(zip);
    const isOverMaximum = testFieldForMaximumCharacters(zip, 5);

    return isUnderMinimum || isNotNumber || isOverMaximum
        ? message
        : undefined
}