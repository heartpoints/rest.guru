export const toggleSideNavOrgSection = (state) => ({
    ...state,
    isSideNavOrgSectionExpanded: !state.isSideNavOrgSectionExpanded
});