import { Predicate } from '../predicates/Predicate';
import { isInForEqualityDefinition } from './isInForEqualityDefinition';
import { EqualityComparer } from './EqualityComparer';

//todo: can rewrite in terms of not?
export const notIn = 
    <T>(equals: EqualityComparer<T>) => 
    (items: T[]): Predicate<T> => 
    t => 
    !isInForEqualityDefinition(equals)(items)(t);
