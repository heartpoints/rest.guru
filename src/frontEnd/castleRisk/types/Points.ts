import { Point } from "./Point";

export type Points = Array<Point>;
