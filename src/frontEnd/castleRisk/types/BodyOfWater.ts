import { HasName } from "./HasName";
import { Location } from "./Location";

export type BodyOfWater = HasName & Location;
