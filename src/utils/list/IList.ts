import { Predicate } from "../predicates/Predicate"
import { Maybe } from "../maybe/MaybeType"
import { Mapper } from "../axioms/Mapper"
import { Reducer } from "../axioms/Reducer"
import { FlatMapper } from "./FlatMapper";

export interface IList<T> extends Iterable<T> {
    map<S>(f: Mapper<T, S>): IList<S>
    flatMap<S>(f: FlatMapper<T, S>): IList<S>
    where(predicate: Predicate<T>): IList<T>
    push<S>(s: S): IList<T | S>
    head: Maybe<T>
    tail: Maybe<IList<T>>
    reduce<S>(reducer: Reducer<S, T>, startingPoint: S): S
    first(predicate: Predicate<T>): Maybe<T>
    isEmpty: boolean
    notEmpty: boolean
    toString(): string
    asArray: Array<T>
    any(predicate: Predicate<T>): boolean
    append<S>(otherList:IList<S>):IList<T | S>
    join(delimiter):string
}