import { ProjectsBundle } from 'gitlab';
import { envVarGetOrThrow } from '../../utils/envVar/envVarGetOrThrow';
export const services = new ProjectsBundle({
    host: 'https://gitlab.com',
    token: envVarGetOrThrow("GITLAB_ENV_TOKEN")
});
