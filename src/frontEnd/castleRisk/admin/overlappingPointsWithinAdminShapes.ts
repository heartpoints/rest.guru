import { List } from '../../../utils/list/List';
import { pointsOverlap } from '../geometry/pointsOverlap';
import { Point } from '../types/Point';
import { territories } from '../territories/general/territories';

export const overlappingPointsWithinAdminShapes = 
    (mousePoint: Point) => {
        const allPoints = List(territories().map(t => t.boundary)).flatMap(List);
        return allPoints.where(pointsOverlap(mousePoint)).asArray;
    };
