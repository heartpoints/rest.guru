import * as React from 'react';
import { IconButton, Tooltip } from '@material-ui/core';
import AddCircle from '@material-ui/icons/AddCircle';

export const AddOpportunityButton = ({navTo}) => <Tooltip title="Add New Opportunity" placeholder="right">
<IconButton style={{color: "#02d177"}} onClick={() => navTo("/volunteering/new")}>
    <AddCircle />
</IconButton>
</Tooltip>