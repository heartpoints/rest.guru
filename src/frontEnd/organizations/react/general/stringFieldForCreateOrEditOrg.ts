import { field } from '../../../state/fields/field'
import { Mapper } from '../../../../utils/axioms/Mapper'
import { Consumer } from '../../../../utils/axioms/Consumer'

export const stringFieldForCreateOrEditOrg =
    <T>
    (fieldGetter: Mapper<string, T>) => 
    (fieldSetter: Mapper<string, Consumer<T>>) => 
    (fieldNameAndLabel: string) => 
    field
        (fieldGetter(fieldNameAndLabel))
        (fieldSetter(fieldNameAndLabel))
        (fieldNameAndLabel)
