export type UpdateStateOf<T> = (newState: Partial<T>) => any;
