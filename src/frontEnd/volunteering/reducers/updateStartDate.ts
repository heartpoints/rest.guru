import { Moment } from 'moment';

export const updateStartDate = (state, date:Moment) => ({
    ...state,
    startDate: date
});