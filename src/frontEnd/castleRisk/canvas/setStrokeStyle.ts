import { RGBAColor } from "../types/RGBAColor";
import { rgbaToCSSString } from "../colors/rgbaToCSSString";
export const setStrokeStyle = (color: RGBAColor) => (context: CanvasRenderingContext2D) => context.strokeStyle = rgbaToCSSString(color);
