import { playerBoxWidth } from '../players/playerBoxWidth';
import { boardWidth } from '../general/boardWidth';
export const placeCastlesCanvasWidth = boardWidth + playerBoxWidth;
