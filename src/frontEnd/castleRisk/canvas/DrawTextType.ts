import { Point } from "../types/Point";
export type DrawText = (textLocation: Point) => (text: string) => void;
