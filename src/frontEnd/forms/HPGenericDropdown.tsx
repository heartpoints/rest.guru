import React, { useState } from 'react';
import { FormControl, InputLabel, Select, MenuItem, FormHelperText} from '@material-ui/core';

export const HPGenericDropdown = ({data, handleChange, label, errorMessage, startingValue=""}) => {
    const [ selectedItem, updateSelectedItem ] = useState(startingValue);

    const menuItemSelected = (e) => {
        updateSelectedItem(e.target.value);
        handleChange(e.target.value);
    }

    return <FormControl style={{marginLeft: "8px"}} variant="standard" margin="normal" error={errorMessage != undefined}>
        <InputLabel>{label}</InputLabel>
        <Select
            style={{minWidth: "120px"}}
            value={selectedItem}
            onChange={menuItemSelected} >
            <MenuItem disabled value=""><em>Select</em></MenuItem>
            {data.map(item => <MenuItem value={item}>{item}</MenuItem>)}
        </Select>
        {errorMessage && <FormHelperText>{errorMessage}</FormHelperText>}
    </FormControl>
}
