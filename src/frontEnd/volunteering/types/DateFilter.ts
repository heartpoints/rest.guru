import { Moment } from 'moment';

export type DateFilter = {dateFilterStart:Moment, dateFilterEnd:Moment}