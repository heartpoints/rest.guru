import * as k8s from "@pulumi/kubernetes"
import { appName } from "./appName";
import { namespace } from "./heartpointsNamespace";
import { heartpointsNamespaceName } from "../envVars/heartpointsNamespaceName";
import { servicePort } from "../servicePort";
import { heartpointsDomain } from "./heartpointsDomain"
import { heartpointsService } from "./heartpointsService"

const protocolRules = {
    paths: [
        {
            backend: {
                serviceName: heartpointsService.metadata.name,
                servicePort,
            },
        }
    ]
}

const domain = heartpointsNamespaceName == 'master' ? heartpointsDomain : `${heartpointsNamespaceName}.${heartpointsDomain}`

export const heartpointsIngress = new k8s.networking.v1beta1.Ingress(appName,
    {
        metadata: {
            name: `${appName}-ingress`,
            namespace: namespace.metadata.name,
            annotations: {
                "kubernetes.io/ingress.class": "haproxy",
            },
        },
        spec: {
            rules: [
                {
                    host: domain,
                    http: protocolRules,
                },
                {
                    host: `www.${domain}`,
                    http: protocolRules,
                },
            ],
        },
    }
)
