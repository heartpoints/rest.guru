export const hungary = [
    {
        "x": 754,
        "y": 333
    },
    {
        "x": 727,
        "y": 326
    },
    {
        "x": 726,
        "y": 316
    },
    {
        "x": 698,
        "y": 322
    },
    {
        "x": 679,
        "y": 332
    },
    {
        "x": 666,
        "y": 324
    },
    {
        "x": 656,
        "y": 328
    },
    {
        "x": 661,
        "y": 334
    },
    {
        "x": 654,
        "y": 348
    },
    {
        "x": 673,
        "y": 365
    },
    {
        "x": 695,
        "y": 373
    },
    {
        "x": 722,
        "y": 361
    },
    {
        "x": 734,
        "y": 364
    },
    {
        "x": 746,
        "y": 352
    }
]