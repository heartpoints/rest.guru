import { Integer } from '../../../utils/math/Integer'
import { regularAngles } from './regularAngles';

export const spikeyShape = 
    (scalingFactor: number) => 
    (radius: number) => 
    (numSides: Integer) => 
    regularAngles(numSides*2)
        .map((angle,i) => ({
            x: Math.cos(angle) * (i % 2 == 0 ? radius : radius / scalingFactor),
            y: Math.sin(angle) * (i % 2 == 0 ? radius : radius / scalingFactor),
        }))
