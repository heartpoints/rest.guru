import { finland } from "./finland";
import { norway } from "./norway";
import { denmark } from "./denmark";
import { Empire } from "../../../types/Empire";
import { green } from "../../../colors/green";
import { territory } from "../../general/territory";
import { Sweden } from "./Sweden";
import { Switzerland } from "./Switzerland";
import { Venice } from "./Venice";
import { Rome } from "./Rome";
import { Naples } from "./Naples";
import { Macedonia } from "./Macedonia";
import { Madrid } from "./Madrid";
import { Portugal } from "./Portugal";
import { darkenColor } from "../../../colors/darkenColor";

//todo: make new type instead of empire
export const independentTerritories:Empire = {
    color: darkenColor(0.2)(green),
    name: "Independent Territories",
    get territories() {
        const independentTerritory = territory(() => independentTerritories)
        return [
            independentTerritory("Finland")(finland),
            independentTerritory("Norway")(norway),
            independentTerritory("Denmark")(denmark),
            independentTerritory("Sweden")(Sweden),
            independentTerritory("Switzerland")(Switzerland),
            independentTerritory("Venice")(Venice),
            independentTerritory("Rome")(Rome),
            independentTerritory("Naples")(Naples),
            independentTerritory("Macedonia")(Macedonia),
            independentTerritory("Madrid")(Madrid),
            independentTerritory("Portugal")(Portugal),
        ]
    }
} 