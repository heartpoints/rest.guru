import * as React from 'react';
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import './css/volCalendar.scss';
import { CombinedOrgAndVolOpportunity } from '../volunteering/types/CombinedOrgAndVolOpportunity';
import { isMobile } from '../site/isMobile';
import { Moment } from 'moment';

type CalendarEvent = { title:string, start:Date, end:Date, id:string };
type Header = { left:string, center:string, right:string };

type WithNavTo = { navTo: (url:string) => void }
type WithFilteredOpportunities = { filteredOpportunities:CombinedOrgAndVolOpportunity[]}
type HasStartDate = {startDate:Moment}
type HasEndDate = {endDate:Moment}
type VolunteeringCalendarProps = WithFilteredOpportunities & HasStartDate & HasEndDate & WithNavTo

export const VolunteeringCalendar = ({filteredOpportunities, startDate, endDate, navTo}:VolunteeringCalendarProps) => {

    const header:Header = isMobile()
        ? {left: 'prev', center: "title", right: 'next'}
        : {left: 'title', center: '', right: 'today prev,next'}
        
    const filteredEvents:CalendarEvent[] = 
        filteredOpportunities.map(o => ({
            title: o.jobTitle,
            start: o.startDate.toDate(), 
            end: o.endDate.toDate(), 
            id: o.jobID}
        ));
   
    return <FullCalendar
        height="auto"
        defaultView="dayGridMonth"
        plugins={[dayGridPlugin]}
        eventClick={info => navTo(`/volunteering/${info.event.id}`)}
        showNonCurrentDates={false}
        header={header}
        events={filteredEvents}
        //dayRender={handleDayRender}
    />
}