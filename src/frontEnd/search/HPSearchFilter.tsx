import * as React from 'react';
import { ClearFilterButton } from '../buttons/ClearFilterButton';

export const filterContainerStyle = {
    "display": "inline-block",
    "backgroundColor": "#e8e8e8",
    "borderTopLeftRadius": "3px",
    "borderTopRightRadius": "3px",
    "padding": "10px",
    "borderBottom": "1px solid #9e9e9e"
}

type withFilterName = { filterName:string }
type WithChildren = { children: React.ReactNode }
type WithOnFilterClear = { onFilterClear: () => void }
type HPSearchFilterProps = WithChildren  & WithOnFilterClear & withFilterName;
export const HPSearchFilter = ({ children, onFilterClear, filterName }:HPSearchFilterProps) => {
    return <div style={filterContainerStyle}>
            <ClearFilterButton filterName={filterName} buttonClickHandler={onFilterClear} />
            {children}
        </div>
}
