import { GetResponse } from 'gitlab/dist/types/core/infrastructure';
import { services } from './services';
import { projectId } from './projectId';
export const gitlabIssue = async (issueNumber: number) => {
    const issue: GetResponse = await services.Issues.show(projectId, issueNumber);
    return issue;
};
