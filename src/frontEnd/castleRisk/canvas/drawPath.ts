import { Shape } from "../types/Shape";
export const drawPath = (shape: Shape) => (context: CanvasRenderingContext2D) => {
    const [firstPoint, ...remainingPoints] = shape;
    const { x, y } = firstPoint;
    context.beginPath();
    context.moveTo(x, y);
    remainingPoints.forEach(({ x, y }) => context.lineTo(x, y));
    context.lineTo(x, y);
    context.closePath();
    context.stroke();
    context.fill();
};
