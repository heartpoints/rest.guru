FROM ubuntu:16.04

# curl
RUN apt-get update && apt-get -y install curl

# kubectl
RUN apt-get update && apt-get install -y apt-transport-https && \
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
    echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | tee -a /etc/apt/sources.list.d/kubernetes.list && \
    apt-get update && \
    apt-get install -y kubectl

# python 2.7 (prereq for gcloud)
RUN apt-get -y install python2.7-minimal

# gcloud
RUN apt-get update && apt-get install -y lsb-core
RUN export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)" && \
    echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
    apt-get update && apt-get install -y google-cloud-sdk

# nvm and node
COPY .nvmrc .
RUN curl https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash \
    && export NVM_DIR="$HOME/.nvm" \
    && [ -s "$NVM_DIR/nvm.sh" ] && set +e; . "$NVM_DIR/nvm.sh"; set -e \
    && nvm install && rm .nvmrc && mv $(which node) /usr/bin

# docker
RUN apt-get update && apt-get install -y apt-transport-https ca-certificates gnupg-agent software-properties-common && \
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - && \
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" && \
    apt-get update && apt-get install -y docker-ce

# yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    apt-get -y update && apt-get -y install yarn

# pulumi
RUN curl -fsSL https://get.pulumi.com | sh && \
    mv $HOME/.pulumi/bin/* /usr/bin

COPY hp .
COPY src/cicd src/cicd
COPY pulumi/Pulumi.yaml pulumi/
