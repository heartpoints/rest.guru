import * as React from 'react';
import { WithUrl } from '../../../../utils/url/WithUrl';
import { Page } from '../../../page/Page';
import { defaultOrgLogoSrc } from '../../data/defaultOrgLogoSrc';
import { findOrgByHref } from '../../data/findOrgByHref';
import { orgHrefFromEditUrl } from '../../data/orgHrefFromEditUrl';
import { WithOrganizations } from '../../data/WithOrganizations';
import { EditLoadedOrganization } from './EditLoadedOrganization';
import { MissingOrganization } from '../general/MissingOrganization';
import { stringFieldMatchingLabelAndPlaceholder } from './stringFieldMatchingLabelAndPlaceholder';
import { stringFieldForEditOrg } from './stringFieldForEditOrg';

export type EditOrganizationProps<S, T> = WithUrl & WithOrganizations
export const EditOrganization = <S, T>(props:EditOrganizationProps<S, T>) => {
    const { url: { path }, organizations } = props;
    const orgHref = orgHrefFromEditUrl(path)
    const title = stringFieldMatchingLabelAndPlaceholder(orgHref)("title")
    const homepage = stringFieldMatchingLabelAndPlaceholder(orgHref)("homepage")
    const mission = stringFieldMatchingLabelAndPlaceholder(orgHref)("mission")
    const imageThumbnailURL = stringFieldForEditOrg(orgHref)("imageThumbnailURL")(defaultOrgLogoSrc)

    const orgFields = {
        title,
        homepage,
        mission,
        imageThumbnailURL,
    }

    const content = findOrgByHref(organizations, orgHref)
        .map(() => <EditLoadedOrganization {...{orgFields}} />)
        .valueOrDefault(<MissingOrganization />)
    
    return <Page>
        {content}
    </Page>
}