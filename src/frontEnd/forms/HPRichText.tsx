import * as React from 'react';
import ReactQuill from 'react-quill';
import { isMobile } from '../site/isMobile';
import { Typography } from '@material-ui/core';
import { Space } from '../page/Space';

type HasChangeHandler = {changeHandler: (value) => void}
type HasMarkup = {markup:string}
type HasErrorMessage = {errorMessage:string | undefined}
type HPRichTextProps = HasChangeHandler & HasMarkup & HasErrorMessage

export const HPRichText = ({changeHandler, markup, errorMessage}:HPRichTextProps) => {
    const {marginLeft, marginBottom, width} = isMobile()
        ? {marginLeft: "0px", marginBottom: "70px", width: "100%"}
        : {marginLeft: "10px", marginBottom: "40px", width: "400px"}

    const borderBottom = errorMessage ? "3px solid #f44336" : 0

    return <React.Fragment>
        <div style={{display: "inline-block", borderBottom, marginLeft}}>
            <ReactQuill 
                style={{width, height: "300px", marginBottom, fontFamily: "Roboto"}} 
                theme="snow" 
                onChange={changeHandler} 
                value={markup || ''} />
        </div>
        <Space />
        {errorMessage && <Typography variant="caption" style={{color: "#f44336", marginLeft: "20px"}}>{errorMessage}</Typography>}
        <Space />
    </React.Fragment>
}