import * as k8s from "@pulumi/kubernetes"

export const haProxyClusterRole = new k8s.rbac.v1.ClusterRole("haproxy-ingress-cluster-role", {
  metadata: { name: "haproxy-ingress-cluster-role" },
  rules: [
    {
      apiGroups: [""],
      resources: [
        "configmaps",
        "endpoints",
        "nodes",
        "pods",
        "services",
        "namespaces",
        "events",
        "serviceaccounts"
      ],
      verbs: [
        "get",
        "list",
        "watch"
      ]
    },
    {
      apiGroups: [
        "extensions"
      ],
      resources: [
        "ingresses",
        "ingresses/status"
      ],
      verbs: [
        "get",
        "list",
        "watch"
      ]
    },
    {
      apiGroups: [""],
      resources: [
        "secrets"
      ],
      verbs: [
        "get",
        "list",
        "watch",
        "create",
        "patch",
        "update"
      ]
    },
    {
      apiGroups: [
        "extensions"
      ],
      resources: [
        "ingresses"
      ],
      verbs: [
        "get",
        "list",
        "watch"
      ]
    }
  ]
})
