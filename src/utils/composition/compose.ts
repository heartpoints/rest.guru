import { GroupOperator } from "../abstractAlgebra/GroupOperator";
import { identity } from "../axioms/identity";

type GroupOperators<T> = Array<GroupOperator<T>>
export const compose = 
    <T>(...operators:GroupOperators<T>):GroupOperator<T> =>
    operators.reduce(
        (innerOperator, currentOperator) => (t:T) => currentOperator(innerOperator(t)),
        identity
    )

console.log("HELLO")
const composedOp = compose<number>(n => n * 2, n => n - 3, n => n * 4)
console.log({composedOp})
console.log(composedOp(7))