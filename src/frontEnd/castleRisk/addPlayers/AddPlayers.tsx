import * as React from 'react';
import { CastleRiskPhaseProps } from '../types/AddPlayerProps';
import { pieceColors } from '../colors/pieceColors';
import { initialPlayer } from '../players/initialPlayer';
import { SingleRowOfColumns } from '../../page/SingleRowOfColumns';
import { noPlayerHasColor } from './noPlayerHasColor';
import { boardStyle } from './boardStyle';
import { playerSetupForm } from './playerSetupForm';

export const AddPlayers = 
    ({ players, updateState, newPlayer }:CastleRiskPhaseProps) => 
    {
        const remainingColors = pieceColors.filter(noPlayerHasColor(players))
        const initialColor = remainingColors[0]
        const player = newPlayer || initialPlayer("", initialColor, [])
        const allowNewPlayers = remainingColors.length > 0
        return SingleRowOfColumns(
            <img src="images/castleRiskLogo.png" style={boardStyle} />,
            playerSetupForm(allowNewPlayers)(players)(player)(remainingColors)(updateState)
        )
    }
    
