import { envVarGetOrThrow } from "../envVarGetOrThrow";

export const heartpointsNamespaceName = envVarGetOrThrow("HP_NAMESPACE_NAME")()
