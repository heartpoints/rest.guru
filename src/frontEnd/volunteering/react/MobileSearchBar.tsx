import React, { useState } from 'react';
import { Page } from '../../page/Page'
import { PageTitle } from '../../page/PageTitle';
import { IconButton, Tooltip } from '@material-ui/core'
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import { findVolunteeringOpportunities } from "../findVolunteeringOpportunities";
import { DateFilter } from '../types/DateFilter';
import { getZipFromCoords } from '../../maps/data/getZipFromCoords';
import { LocationPermissonDialog } from '../../modals/LocationPermissionDialog';
import { DateRangeFilter } from '../../date/DateRangeFilter';
import { HPNotFoundDisplay } from './HPNotFoundDisplay';
import { DropdownTextArea } from '../../forms/DropdownTextArea';
import { sampleDropdownData } from '../data/sampleDropdownData';
import haversine from "haversine";
import { ListResult } from './ListResult';
import { Coord } from '../../maps/types/Coord';
import { ZipSearchField } from './ZipSearchField';

export const MobileSearchBar = ({ userZipCoordinates, updateUserZipCoordinates, organizations, navTo, volSearchBarValue, onVolSearchBarValueChange, zipFilter, updateZipFilter, startDate, updateStartDate, endDate, updateEndDate }) => {
    const [ shouldShowTooltip, toggleShowTooltip ] = useState(false);
    const [ persmissionMessageOpen, updatePermissionMessageOpen ] = useState(false);
    const [ zipGeneratedFromLocation, toggleZipGeneratedFromLocation ] = useState(false);

    const showPosition = async (position) => {
        onZipTextChange(await getZipFromCoords(position.coords.latitude, position.coords.longitude));
        await toggleZipGeneratedFromLocation(true);
    }

    const userLocation = async () => 
        navigator.geolocation.getCurrentPosition(await showPosition, () => console.log("Error"));

    const permissionGrantedHandler = () => {
        userLocation();
        updatePermissionMessageOpen(false);
    }

    const validateStartDate = (date) => {
        updateStartDate(date);
        endDate.isBefore(date) && updateEndDate(date);
    }

    const validateEndDate = (date) => {
        updateEndDate(date);
        startDate.isAfter(date) && updateStartDate(date);
    }

    const clearUserLocationZipHandler = () => {
        onZipTextChange('');
        toggleZipGeneratedFromLocation(false)
    }

    const dateFilter:DateFilter = {dateFilterStart: startDate, dateFilterEnd: endDate};

    const onZipTextChange = async (value:string) => {
        updateZipFilter(value)
        if(value.length == 5) {

            const response = await fetch(`/latLonForZip?zip=${value}`)
            const zipcodeLatLon = (await response.json()) as Coord

            updateUserZipCoordinates(zipcodeLatLon);
        }
    }

    const shownOpportunities = findVolunteeringOpportunities(volSearchBarValue, dateFilter, organizations, userZipCoordinates, zipFilter);

    const listResults = () => {
        const results = shownOpportunities.map(o => {
            const oppCoordinates:Coord = {latitude: o.coordinates.lat, longitude: o.coordinates.lng}
            const distance:number = Math.round(haversine(oppCoordinates, userZipCoordinates, {unit: 'mile'}) * 100) / 100;

            return <ListResult entity={o} distance={distance} navTo={navTo} /> }
        );

        return <React.Fragment>
            {results.length > 0 ? results : <HPNotFoundDisplay message="No results match your search. Please refine your search criteria and try again!" />}
        </React.Fragment>
    }

    return <Page>
        <PageTitle>
            Help Out
            <Tooltip
                title={<div>Find volunteering <br /> opportunities near you</div>}
                placement="right-end"
                PopperProps={{style:{marginLeft: "120px"}}}
                open={shouldShowTooltip}>
                <IconButton onClick={() => toggleShowTooltip(!shouldShowTooltip)}>
                    <InfoOutlinedIcon />
                </IconButton>
            </Tooltip>
        </PageTitle>
        <DropdownTextArea data={sampleDropdownData} searchBarValue={volSearchBarValue} onSearchBarValueChange={onVolSearchBarValueChange} popperTitle="Popular Searches" />
        <ZipSearchField {...{zipGeneratedFromLocation, zipFilter, updatePermissionMessageOpen, onZipTextChange, clearUserLocationZipHandler}} />
        <DateRangeFilter
            startDate={startDate}
            onStartDateChange={date => validateStartDate(date)}
            endDate={endDate}
            onEndDateChange={date => validateEndDate(date)} />
        {listResults()}
        <LocationPermissonDialog isOpen={persmissionMessageOpen} permissionGrantedHandler={permissionGrantedHandler} toggleIsOpen={updatePermissionMessageOpen} />
    </Page>
}