import { centroidForCoord } from './centroidForCoord';
import { centroidOfAdjacentPointsForCoordMapper } from "./centroidOfAdjacentPointsForCoordMapper";
import { CoordToNumber } from './CoordToNumber';

export const centroidPoints = 
    (coordToNumber: CoordToNumber) => 
    centroidForCoord(centroidOfAdjacentPointsForCoordMapper(coordToNumber));
