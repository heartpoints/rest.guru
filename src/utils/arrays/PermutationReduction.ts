import { LikePairs } from './LikePairs';
export type PermutationReduction<T> = {
    permutations: LikePairs<T>;
    toBePermuted: T | undefined;
};
