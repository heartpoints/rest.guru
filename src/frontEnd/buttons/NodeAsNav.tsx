import * as React from 'react';
import { Tooltip } from '@material-ui/core'

export const NodeAsNav = ({children, clickHandler, toottipTitle = ''}) => {
    const clickableItemStyle = {"cursor": "pointer"};

    return <Tooltip title={toottipTitle} placement='right-end'>
    <a style={clickableItemStyle} onClick={clickHandler}>
        {children}
    </a>
    </Tooltip>
}