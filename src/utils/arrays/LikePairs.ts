import { LikePair } from './LikePair';
export type LikePairs<T> = Array<LikePair<T>>;
