import Cookies from 'js-cookie';
import { inDevMode } from '../developers/inDevMode';
import { navTo } from '../nav/navTo';
import { userCredentialsCookieKey } from './userCredentialsCookieKey';

export const logoutUserCredentials = (state) => {
    Cookies.remove(userCredentialsCookieKey);
    
    const postLogoutState = {
        ...state,
        userCredentials: {email: ""},
        inDevMode: inDevMode()
    }

    return navTo(postLogoutState, "/")
}