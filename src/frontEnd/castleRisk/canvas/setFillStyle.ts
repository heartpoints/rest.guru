import { RGBAColor } from "../types/RGBAColor";
import { rgbaToCSSString } from "../colors/rgbaToCSSString";
export const setFillStyle = (color: RGBAColor) => (context: CanvasRenderingContext2D) => context.fillStyle = rgbaToCSSString(color);
