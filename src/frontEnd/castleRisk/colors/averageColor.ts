import { averager } from '../../../utils/math/averager';
import { sumColors } from './sumColors';
import { colorDivideBy } from './colorDivideBy';
export const averageColor = averager(sumColors)(colorDivideBy);
