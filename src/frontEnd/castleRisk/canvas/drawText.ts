import { multiplex } from "../../../utils/composition/multiplex";
import { Point } from "../types/Point";
import { RGBAColor } from "../types/RGBAColor";
import { defaultFontFace } from "./defaultFontFace";
import { defaultFontPixels } from "./defaultFontPixels";
import { defaultFontSize } from "./defaultFontSize";
import { setShadow } from "./setShadow";
import { setStrokeStyle } from "./setStrokeStyle";
import { strokeText } from "./strokeText";
import { setFont } from "./setFont";

export const drawText =
    (useShadow: boolean) => 
    (color: RGBAColor) => 
    ({ x, y }: Point) => 
    (text: string) => 
    multiplex([
        setShadow(useShadow),
        setStrokeStyle(color),
        setFont(`${defaultFontSize} ${defaultFontFace}`),
        strokeText(text)(x)(y + defaultFontPixels / 2),
        setShadow(false),
    ])
