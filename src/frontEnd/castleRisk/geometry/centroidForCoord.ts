import { Shape } from '../types/Shape';
import { numericSum } from '../../../utils/math/numericSum';
import { cyclicPermutations } from '../../../utils/arrays/cyclicPermutations';
import { CentroidOfAdjacentPoints } from "./CentroidOfAdjacentPoints";
import { centroidCooefficient } from './centroidCooefficient';

export const centroidForCoord = 
    (centroidOfAdjacentPoints:CentroidOfAdjacentPoints) => 
    (points: Shape) => 
    centroidCooefficient(points) *
    numericSum(cyclicPermutations(points).map(centroidOfAdjacentPoints))
