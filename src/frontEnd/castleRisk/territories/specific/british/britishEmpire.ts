import { Empire } from "../../../types/Empire";
import { darkenColor } from "../../../colors/darkenColor";
import { desaturateColor } from "../../../colors/desaturateColor";
import { territory } from "../../general/territory";
import { London } from "./London";
import { Wales } from "./Wales";
import { Yorkshire } from "./Yorkshire";
import { Ireland } from "./Ireland";
import { Scotland } from "./Scotland";

export const britishEmpire:Empire = {
    color: desaturateColor(0.7)(darkenColor(0.2)([200,0,255,255])),
    name: "British",
    get territories() {
        const britishTerritory = territory(() => britishEmpire)
        return [
            britishTerritory("London")(London),
            britishTerritory("Wales")(Wales),
            britishTerritory("Yorkshire")(Yorkshire),
            britishTerritory("Ireland")(Ireland),
            britishTerritory("Scotland")(Scotland),
        ]
    }
}