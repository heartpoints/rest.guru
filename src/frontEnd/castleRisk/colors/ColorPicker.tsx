import * as React from 'react';
import { HPDropdown } from '../../forms/HPDropdown';
import { contrastColor } from './contrastColor';
import { colorStyle } from './colorStyle';
import { ColorPickerProps } from './ColorPickerProps';
import { plainStringToRGBAColor } from './plainStringToRGBAColor';
import { ColorMenuItem } from './ColorMenuItem';

export const ColorPicker = ({ colors, onColorChange, value }: ColorPickerProps) => {
    const labelStyle = { color: contrastColor(value) };
    const fieldStyle = colorStyle(value);
    const onItemSelected = (rgbaString) => onColorChange(plainStringToRGBAColor(rgbaString))
    return <HPDropdown label="color" {...{ labelStyle, fieldStyle, value, onItemSelected }}>
        {colors.map(ColorMenuItem)}
    </HPDropdown>;
};
