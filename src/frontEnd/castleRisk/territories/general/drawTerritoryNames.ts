import { white } from "../../colors/white";
import { drawTerritoryName } from "./drawTerritoryName";
import { territories } from './territories';
import { multiplex } from "../../../../utils/composition/multiplex";

export const drawTerritoryNames = 
    multiplex(territories().map(drawTerritoryName(white)))