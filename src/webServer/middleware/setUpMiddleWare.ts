import { middlewares } from "./middlewares"
import { Express } from "express"

export const setUpMiddleWare = 
    (expressApp:Express) =>
    middlewares.map(middleware => expressApp.use(middleware))