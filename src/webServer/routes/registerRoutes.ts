import { allRouters } from "./allRouters";
import { multiplex } from "../../utils/composition/multiplex";

export const registerRoutes = multiplex(allRouters)
