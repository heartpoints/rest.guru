import * as docker from "@pulumi/docker";
import { appName } from "../heartpoints.org/appName";
import { imageName } from "../envVars/imageName";
import { commitSha } from "../envVars/commitSha";
import { registryHostAndPort } from "../envVars/registryHostAndPort";
import { registryUsername } from "../envVars/registryUsername";
import { registryPassword } from "../envVars/registryPassword";

export const dockerImage = new docker.Image(appName, {
    build: {
        context: "../",
        args: {
            commitSha: commitSha()
        }
    },
    imageName: imageName(),
    registry: {
        server: registryHostAndPort(),
        username: registryUsername(),
        password: registryPassword()
    }
});
