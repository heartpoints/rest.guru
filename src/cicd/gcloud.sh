#!/usr/bin/env bash

source "src/cicd/reflect.sh"
source "src/cicd/brew.sh"

gcloud_install() {
    if command_does_not_exist "gcloud"; then
        hp_brew_cask_install google-cloud-sdk
    fi
}

hp_gcloud() { local args=$@
    if hp_isMac && ! which gcloud &> /dev/null; then
        hp_brew_cask_run_unmatching_executable google-cloud-sdk gcloud $args
    else
        gcloud $args
    fi
}

hp_gcr_io() {
    echo "gcr.io"
}

hp_gcr() {
    echo "$(hp_gcr_io)/heartpoints-org"
}