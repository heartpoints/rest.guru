import { addShadowToContext } from "./addShadowToContext";
import { removeShadowFromContext } from "./removeShadowFromContext";

export const setShadow = 
    (useShadow: boolean) => 
    (context: CanvasRenderingContext2D) => 
    (useShadow ? addShadowToContext : removeShadowFromContext)(context);
