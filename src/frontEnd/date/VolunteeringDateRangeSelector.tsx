import * as React from 'react';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { Typography } from '@material-ui/core';
import MomentUtils from '@date-io/moment';
import { Space } from '../page/Space';
import { HPDateTimePicker } from './HPDateTimePicker';
import { Moment } from 'moment';
import { isMobile } from '../site/isMobile';

export const materialUiStyle = {
    "backgroundColor": "#e8e8e8",
    "borderTopLeftRadius": "3px",
    "borderTopRightRadius": "3px",
    "padding": "10px",
}

type HasOnStartDateChange = {onStartDateChange: (date:Moment) => void}
type HasOnEndDateChange = {onEndDateChange: (date:Moment) => void}
type HasStartDate = {startDate:Moment}
type HasEndDate = {endDate:Moment}
type HasErrorText = {errorText: string | undefined}
type VolunteeringDateRangeSelectorProps = HasOnStartDateChange & HasOnEndDateChange & HasStartDate & HasEndDate & HasErrorText

export const VolunteeringDateRangeSelector = (props:VolunteeringDateRangeSelectorProps) => {
    const {onStartDateChange, onEndDateChange, startDate, endDate, errorText } = props;

    const { borderBottomColor, borderBottomWidth } = errorText 
        ? { borderBottomColor: "#f44336", borderBottomWidth: "2px"}
        : { borderBottomColor: "#9e9e9e", borderBottomWidth: "1px"};

    const containerMargin = () => isMobile() ? "0px" : "10px"

    const renderEndDatePickers = () => 
        <HPDateTimePicker dateVal={endDate} dateTimeChangeHandler={onEndDateChange} dateLabel="Select End Date" timeLabel="Select End Time" />

    return <div style={{display: "inline-block", marginLeft: containerMargin()}}>
        <div style={{borderBottom: `${borderBottomWidth} solid ${borderBottomColor}`, ...materialUiStyle}}>
            <MuiPickersUtilsProvider utils={MomentUtils}>
                <HPDateTimePicker dateVal={startDate} dateTimeChangeHandler={onStartDateChange} dateLabel="Select Start Date" timeLabel="Select Start Time"/>
                <Space />
                {renderEndDatePickers()}
            </MuiPickersUtilsProvider>
        </div>
        <Space />
        {errorText && <Typography variant="caption" style={{color: "#f44336"}}>{errorText}</Typography>}
    </div>
}