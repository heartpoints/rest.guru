import { RGBAColor } from '../types/RGBAColor';
import { darkenColor } from './darkenColor';
import { lightenColor } from './lightenColor';
import { colorSaturation } from './colorSaturation';
import { inverseColor } from './inverseColor';

export const contrastColor = 
    (color: RGBAColor): RGBAColor => {
        const sat = colorSaturation(color)
        const distanceFromMediumGray = sat - 127
        const lightenOrDarken = distanceFromMediumGray > 0 ? lightenColor : darkenColor
        return lightenOrDarken(distanceFromMediumGray / 128)(inverseColor(color))
    }
