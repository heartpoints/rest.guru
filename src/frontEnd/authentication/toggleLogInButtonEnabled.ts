import { inDevMode } from '../developers/inDevMode';

export const toggleLogInButtonEnabled = (state, logInButtonEnabled:boolean) =>({
    ...state,
    logInButtonEnabled,
    inDevMode: inDevMode()
});