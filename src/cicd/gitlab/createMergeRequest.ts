import { services } from './services';
import { projectId } from './projectId';
export const createMergeRequest = async (newBranchName: string) => {
    return await services.MergeRequests.create(projectId, newBranchName, "master", newBranchName)
        .catch(e => console.log(e));
};
