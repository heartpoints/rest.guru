import { staticContentMiddlewares } from "./staticContentMiddlewares";
import { expressMiddlewareFromImmutableMiddleware } from "./expressMiddlewareFromImmutableMiddleware";
import { immutableMiddlewares } from "./immutableMiddlewares";
import compression from "compression"

export const middlewares = [
    compression(),
    ...immutableMiddlewares.map(expressMiddlewareFromImmutableMiddleware),
    ...staticContentMiddlewares, 
];
