import { Pieces } from "./Pieces";
export type HasPieces = {
    pieces: Pieces;
};
