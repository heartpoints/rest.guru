import * as React from "react";
import { Typography, Container, Card } from "@material-ui/core";
import { Space } from "../page/Space";
import { MiscSnackBar } from "../modals/MiscSnackBar";
import { isMobile } from "../site/isMobile";

const containerStyle = {
    minHeight: "90vh",
    alignItems: "center"
}

export const HomePage = ({shouldShowSnackbar, snackbarText, onDisplayHomeSnackbar}) => { 
    const snackbarProps = {
        snackbarText,
        onDisplayHomeSnackbar
    }

    const {subtitleSize, logoImgSrc, display, marginTop} = isMobile()
        ? {subtitleSize: "subtitle1" as "subtitle1", logoImgSrc: "images/logo_mobile.png", display: "block", marginTop: "60px"}
        : {subtitleSize: "h4" as "h4", logoImgSrc: "images/logo.png", display: "flex", marginTop: "0px"}

    return <Container style={{...containerStyle, display, marginTop}}>
        <Card style={{textAlign: "center"}}>
            <img width="100%" src={logoImgSrc} />
            <Typography variant={subtitleSize}>help us build something amazing!</Typography>
            <Space />
            <Typography variant="h6">
                <a href="mailto:info@heartpoints.org">info@heartpoints.org</a> 
            </Typography>
            <Space />
        </Card>
        {shouldShowSnackbar && <MiscSnackBar {...snackbarProps} />}
    </Container>
}