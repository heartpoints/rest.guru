import { Url } from "./theInternet";

export type RGSONValue = string | number | boolean | null | RGSONDictionary | RGSONArray;

export interface RGSONDictionary {
    [x: string]: Url;
}

const isArray = <T>(possibleArray:any): possibleArray is T[] =>
    possibleArray.length != undefined 
    && possibleArray.map != undefined

export const IsRJSONArray = (v:RGSONValue): v is RGSONArray => isArray(v)
export const IsRJSONDictionary = (v:RGSONValue): v is RGSONDictionary => !isArray(v)

export type RGSONArray = Array<Url>