import { heartpointsNamespaceName } from "./envVars/heartpointsNamespaceName"
import { namespace } from "./heartpoints.org/heartpointsNamespace"
import { heartpointsDeployment } from "./heartpoints.org/heartpointsDeployment"
import { heartpointsIngress } from "./heartpoints.org/heartpointsIngress"
import { heartpointsService } from "./heartpoints.org/heartpointsService"

const ingressResources = heartpointsNamespaceName == "master" ? {
  haProxyClusterRoleBinding: require("./ingressController/haProxyClusterRoleBinding"),
  haProxyConfigMap: require("./ingressController/haProxyConfigMap"),
  ingressDefaultBackend: require("./ingressController/ingressDefaultBackend"),
  ingressDefaultBackendService: require("./ingressController/ingressDefaultBackendService"),
  haProxyIngressDeployment: require("./ingressController/haProxyIngressDeployment"),
  haProxyService: require("./ingressController/haProxyService"),
  heartpointsWildcardTlsSecret: require("./ingressController/heartpointsWildcardTlsSecret"),
} : {}

export const resources = {
  namespace,
  heartpointsDeployment,
  heartpointsIngress,
  heartpointsService,
  ...ingressResources,
}
