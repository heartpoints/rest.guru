
import { appName } from "./appName";
import { appLabels } from "./appLabels";
import { servicePort } from "../servicePort";
import * as k8s from "@pulumi/kubernetes";
import { namespace } from "./heartpointsNamespace";

export const heartpointsService = new k8s.core.v1.Service(appName, {
    metadata: {
        name: appName,
        namespace: namespace.metadata.name,
    },
    spec: {
        ports: [{
            port: servicePort,
            targetPort: 5001,
        }],
        selector: appLabels,
    },
})
