import { Plus } from './PlusOfT'

export const summer = 
    <T>(plus: Plus<T>) => 
    (identity: T) => 
    (values: T[]) => 
    values.reduce(
        (acc, cur) => plus(acc)(cur), 
        identity
    )
