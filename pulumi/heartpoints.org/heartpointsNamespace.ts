import * as k8s from "@pulumi/kubernetes"
import { heartpointsNamespaceName } from "../envVars/heartpointsNamespaceName";

export const namespace = new k8s.core.v1.Namespace(heartpointsNamespaceName,
    {
        metadata: {
            name: heartpointsNamespaceName,
        },
    }
)
