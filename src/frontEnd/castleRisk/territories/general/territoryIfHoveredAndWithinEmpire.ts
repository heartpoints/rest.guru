import { currentlyHoveredTerritory } from './currentlyHoveredTerritory';
import { Point } from '../../types/Point';
import { territoryIn } from './territoryIn';
import { Empire } from '../../types/Empire';

export const territoryIfHoveredAndWithinEmpire = 
    (mousePoint: Point) =>
    ({ territories }: Empire) => 
    currentlyHoveredTerritory(mousePoint)
    .if(territoryIn(territories));
