Firebase Authentication
==================================

# Firebase Auth

[Firebase Auth](https://firebase.google.com/docs/auth) is the means by which Heartpoints organizes our user accounts, and the method by which users log in to our application. It allows users to sign in with their existing accounts on a variety of platforms. Currently, we have Facebook and Google authentication set up.

## Managing Firebase Auth

All Firebase-specific data and settings can be managed by visiting [firebase](https://firebase.google.com/)'s homepage, selecting `Get Started`, and selecting the `heartpoints-org` project. 

### NOTE: You will have to granted access by a Heartpoints administrator to have access to our Firebase account

One you've selected the `heartpoints-org` project, the Firebase dashboard will load. There you can access any of our activated Firebase components. In this doc, we will focus on `Authentication`.

Clicking the `Authentication` tab on the page's left navigation will bring you to the Authentication dashboard, where there are 4 menu items.

#### Users
- The `Users` tab will display all users who have registerd with Heartpoints through Firebase, which available platform(s) they signed in with, and other important information. For testing purposes, you can easily delete your own account but `**please be careful not to delete someone else's**`

### Sign-in Method
The `Sign-in Method` tab is where you can manage which applications Hearpoints' firebase includes in our authentication process. 

  - `Sign-in providers` allows you to manage the credentials that that application uses to authenticate their individual users on HP's behalf. (Covered in more detail in the next section)
  - `Authorized domains` specifies the trusted domains for which Firebase will process authentication requests


## Managing Credentials

[Google]() does not require any additional credentials
[Facebook](https://developers.facebook.com/apps/1010813445640879/fb-login/settings/)

[Learn how to add credentials for additional applications](https://firebase.google.com/docs/auth/web/github-auth)