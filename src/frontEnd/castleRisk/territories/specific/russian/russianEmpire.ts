import { stPetersberg } from "./stPetersburg"
import { poland } from "./poland"
import { livonia } from "./livonia"
import { moscow } from "./moscow"
import { smolensk } from "./smolensk"
import { ukraine } from "./ukraine"
import { Empire } from "../../../types/Empire";
import { red } from "../../../colors/red";
import { territory } from "../../general/territory";

export const russianEmpire:Empire = {
    color: red,
    name: "Russian",
    get territories() { 
        const russianTerritory = territory(() => russianEmpire)
        return [
            russianTerritory("St. Petersburg")(stPetersberg),
            russianTerritory("Poland")(poland),
            russianTerritory("Livonia")(livonia),
            russianTerritory("Moscow")(moscow),
            russianTerritory("Smolensk")(smolensk),
            russianTerritory("Ukraine")(ukraine),
        ]
    }
}