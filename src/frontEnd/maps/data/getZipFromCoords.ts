import Geocode from 'react-geocode';

export const getZipFromCoords = (lat, lng) => {
    Geocode.setApiKey("AIzaSyCCCW385gZ4e-6rSTXNVtcoklltFRat8ck");

    return Geocode.fromLatLng(lat, lng).then(
        response => {
            const results = response.results[0];
            
            const zipComponent = results.address_components.find(c => c.types.indexOf("postal_code") > -1);
            const zip = zipComponent.short_name;

            return zip;
        },
        error => {
            return "Error. Please Try Again";
        }
    );
}