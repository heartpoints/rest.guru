import * as React from 'react';
import { YesOrNoDialog } from './YesOrNoDialog';
import { DialogContent, DialogContentText } from '@material-ui/core';

type HasIsOpen = {isOpen:boolean}
type HasPermissionGrantedHandler = {permissionGrantedHandler:() => void}
type HasToggleIsOpen = {toggleIsOpen:(bool:boolean) => void}
type LocationPermissonDialogProps = HasIsOpen & HasPermissionGrantedHandler & HasToggleIsOpen

export const LocationPermissonDialog = ({isOpen, permissionGrantedHandler, toggleIsOpen}:LocationPermissonDialogProps) => {
    return <YesOrNoDialog 
            isOpen={isOpen} 
            titleText="Heartpoints.org"
            onYesClicked={() => permissionGrantedHandler()} 
            onNoClicked={() => toggleIsOpen(false)}>
            <DialogContent>
                <DialogContentText>
                    May we have access to your device's location to find 
                    nearby volunteering opportunities ?
                </DialogContentText>
            </DialogContent>
        </YesOrNoDialog>
}