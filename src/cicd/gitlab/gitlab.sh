#!/usr/bin/env bash

source "src/cicd/yarn.sh"
source "src/cicd/git.sh"

hp_dirtyHeadError() {
    echo ""
        echo "ERROR: Uncommitted Changes!"
        echo "Current Branch has Unstaged Changes"
        echo "Please commit all current changes to the remote before proceeding"
        echo ""
        errorAndExit "Run the ./hp g [commit message] command then try again\n\n"
        echo ""
}

hp_boardIssuesInOrder_help() { echo "[alias: bi] lists all issues in the current sprint"; }
hp_boardIssuesInOrder() {
    hp_lab boardIssuesInOrder
}

gitlabTokenPath() {
    echo ~/heartpoints-gitlab-token.key
}

verifyUserHasGitlabToken() {
    if file_does_not_exist "$(gitlabTokenPath)"; then
        outputGitlabTokenHelp
        errorAndExit "Gitlab token not found at $(gitlabTokenPath)"
    else
        gitlabTokenPath
    fi
}

outputGitlabTokenHelp() {
    echo "Please obtain a gitlab token at https://gitlab.com/profile/personal_access_tokens"
    echo "Copy it to clipboard and then run 'hp createGitlabToken'"
}

hp_createGitlabToken() {
    local clipboardContent="$(pbpaste)"
    if string_is_empty "${clipboardContent}"; then
        outputGitlabTokenHelp
        errorAndExit "No token found in clipboard"
    fi
    echo "${clipboardContent}" > $(gitlabTokenPath)
    echo "Token has been copied from clipboard to $(gitlabTokenPath)"
}

gitlabToken() {
    cat $(gitlabTokenPath)
}

hp_newBranchForIssue_help() { echo "[alias: nb] creates a new branch from a given issue number"; }
hp_newBranchForIssue() { local args=$@ export allowDockerBuildForUncommittedChanges
    if gitHeadIsDirty && strings_are_equal "${allowDockerBuildForUncommittedChanges:-false}" false; then
        hp_dirtyHeadError
    else 
        hp_lab newBranchForIssue "$@"
    fi
}

hp_lab() { local args="$@"
    verifyUserHasGitlabToken
    GITLAB_ENV_TOKEN="$(gitlabToken)" hp_yarn ts-node src/cicd/gitlab/runCli.ts "$@"
}

hp_bi(){
    hp_boardIssuesInOrder "$@"
}

hp_nb(){ local args=$@
    hp_newBranchForIssue $@
}