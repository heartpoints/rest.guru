import { HasName } from "./HasName";
import { HasPieces } from "./HasPieces";
import { HasCastles } from "./HasCastles";
import { HasTerritories } from "./HasTerritories";
import { HasEmpires } from "./HasEmpires";
import { HasColor } from "./HasColor";

export type Player = HasColor & HasName & HasCastles & HasTerritories & HasEmpires & HasPieces