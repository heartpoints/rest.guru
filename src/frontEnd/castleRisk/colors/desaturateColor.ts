import { RGBAColor } from "../types/RGBAColor";
import { diffsWithNum } from "../../../utils/math/diffsWithNum";
import { scale } from "../../../utils/math/scale";
import { addNums } from "../../../utils/math/addNums";
import { averageNumber } from "../../../utils/math/averageNumber";

export const desaturateColor = (saturationPercent: number) => ([r, g, b, a]: RGBAColor) => {
    const rgb = [r, g, b];
    const grayLevel = averageNumber(rgb);
    const diffs = diffsWithNum(grayLevel)(rgb);
    const scaledDiffs = scale(saturationPercent)(diffs);
    return [...addNums(rgb)(scaledDiffs), a] as RGBAColor;
};
