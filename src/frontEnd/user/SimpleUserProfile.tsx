import * as React from 'react';
import { Avatar, Typography } from '@material-ui/core';
import { Space } from '../page/Space';
import { EntityPreview } from '../page/EntityPreview';

export const avatarStyle:React.CSSProperties = {
    "display": "inline-block",
    "marginRight": "20px",
    "width": "60px",
    "height": "60px"
}

export const SimpleUserProfile = ({possibleUserEmail, organizations, navTo, userCredentials}) => {

    const associatedOrganizations = organizations.filter(o => o.creatorEmail == possibleUserEmail);
    const profilePicture = userCredentials.email != ""
        ? userCredentials.photoURL
        : "/images/fillerAvatar.png"

    const userHasOrgs = associatedOrganizations.length > 0;

    const renderAssociatedOrganizations = () => userHasOrgs
        ? associatedOrganizations.map(o => <EntityPreview image={o.imageThumbnailURL} title={o.title} details={o.mission} clickHandler={() => navTo(o.href)} />)
        : <Typography variant="h6">No Organizations Yet!</Typography>

    return <div style={{textAlign: "center"}}>
        <Avatar src={profilePicture} style={avatarStyle} />
        <Typography variant="h3"><a href={`mailto:${possibleUserEmail}`}>{possibleUserEmail}</a></Typography>
        <Space />
        <Space />
        {userHasOrgs && <Typography variant="h5" align="left">Member of the Following Organizations :</Typography>}
        {renderAssociatedOrganizations()}
    </div>
}