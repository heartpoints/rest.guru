import { OrgOpportunityPair } from './types/OrgOpportunityPair';
import { CombinedOrgAndVolOpportunity } from './types/CombinedOrgAndVolOpportunity';

export const combineOrgOppPairToSinglePropsObject = 
    ([org, opp]: OrgOpportunityPair):CombinedOrgAndVolOpportunity => 
    ({ ...org, ...opp })
