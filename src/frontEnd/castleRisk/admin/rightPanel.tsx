import * as React from 'react';
import { UpdateStateForCastleRisk } from '../general/UpdateStateForCastleRisk';
import { instructions } from './instructions';
import { AdminState } from '../types/AdminState';

export const rightPanel = 
    (admin: AdminState) => 
    (updateState: UpdateStateForCastleRisk) => 
    <React.Fragment>
        {instructions}
        <textarea 
            style={{width: "100%", height: "30vh"}}
            onChange={e => updateState({ admin: { ...admin, currentlyDrawingShape: JSON.parse(e.target.value) } })}
            value={JSON.stringify(admin.currentlyDrawingShape, null, 3)}
        />
    </React.Fragment>
