import { Consumer } from '../../../utils/axioms/Consumer';
import { RGBAColor } from '../types/RGBAColor';
import { Colors } from './Colors';
export type ColorPickerProps = {
    colors: Colors;
    onColorChange: Consumer<RGBAColor>;
    value: RGBAColor;
};
